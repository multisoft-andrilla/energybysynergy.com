// Firebase
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'

// * Config
const firebaseConfig = {
  apiKey: 'AIzaSyCbuqFvStPhm4BnZKLmoJxXVFlDOD86XIs',
  authDomain: 'andrilla-database.firebaseapp.com',
  projectId: 'andrilla-database',
  storageBucket: 'andrilla-database.appspot.com',
  messagingSenderId: '39716572820',
  appId: '1:39716572820:web:50089ff31b8b6b2379ae81'
}

export default firebaseConfig

export const firebaseApp = initializeApp(firebaseConfig)
export const firestore = getFirestore(firebaseApp)

// * Site IDs
export const ownerID = 'wNGBBqpKnDaYiPjIdi1vW58SyvD3'
export const siteID = 'zn66TUV0QsYJfZl7s4Cy'

// * Form IDs
export const contactFormID = 'r0Kv11jwvOzQCX8NlfxP'