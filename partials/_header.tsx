// * Types
import type { FunctionComponent } from 'react'

// * Hooks
import { useState } from 'react'

// * Components
import Link from 'next/link'
import Logo from '@/svgs/logo/synergy-solar-group.svg'

// * Partials
import Nav, { SideNav } from './nav/_nav'

// * Functions
import { addClass, removeClass } from '@/utils/class-management'

const Header: FunctionComponent = () => {

  const [ isOpen, setIsOpen ] = useState(false),
        toggleNavOpen = () => setIsOpen((previousValue) => {

          const body = document.querySelector('body')

          if (body) {
            if (previousValue === true) removeClass(body, ['h-[100dvh]', 'overflow-y-hidden'])
            if (previousValue === false) addClass(body, ['h-[100dvh]', 'overflow-y-hidden'])
          }

          return !previousValue
        }),
        closeNav = () => setIsOpen(() => {

          const body = document.querySelector('body')
          if (body) removeClass(body, ['h-[100dvh]', 'overflow-y-hidden'])

          return false
        })

  return (
    <header
      className='
        fixed inset-0 bottom-auto
        px-2.5 py-3 sm:px-4
        flex justify-between items-center gap-6
        z-30
        bg-white bg-opacity-80 dark:bg-gray-900 dark:bg-opacity-70
        backdrop-filter backdrop-blur-xl
      '
    >

      <Link href='/' className='z-10' onClick={closeNav}>
        <span className='sr-only'>Synergy Energy Solutions (Go Home)</span>
        <Logo className='h-12 sm:h-16 dark:fill-white' />
      </Link>

      <Nav />

      <SideNav isOpen={isOpen} setIsOpen={setIsOpen} toggleNavOpen={toggleNavOpen} />

    </header>
  )
}

export default Header