// * Types
import type { FunctionComponent } from 'react'

// * Components
import A from 'next/link'
import Link from '@/components/link'
import Logo from '@/svgs/logo/synergy-solar-group.svg'
import InstagramLogo from '@/svgs/social-media/instagram-logo.svg'
import FacebookLogo from '@/svgs/social-media/facebook-logo.svg'
import LinkedInLogo from '@/svgs/social-media/linkedin-logo.svg'
import GoogleLogo from '@/svgs/social-media/google-logo.svg'
import VisaLogo from '@/svgs/graphics/visa.svg'
import MastercardLogo from '@/svgs/graphics/mastercard.svg'
import DiscoverLogo from '@/svgs/graphics/discover.svg'
import AmericanExpressLogo from '@/svgs/graphics/american-express.svg'

// * Utils
import { year } from '@/utils/date'

const Footer: FunctionComponent = () => {
  return (
    <footer className='bg-gray-900 px-6 py-12'>

      <div className='
        max-w-7xl mx-auto
        grid grid-flow-row gap-10
        p-6 sm:p-12
        rounded-4xl sm:rounded-5xl
        bg-gray-100 dark:bg-gray-700 dark:contrast-more:bg-black
        text-gray-600 dark:text-gray-300 contrast-more:text-black dark:contrast-more:text-white
        font-display
      '>

        <div className='grid grid-flow-row gap-8 sm:grid-cols-2 lg:grid-cols-4 lg:place-items-center'>

          <div className='flex flex-col justify-between gap-8 lg:w-full'>

            <A href='/'>
              <span className='sr-only'>Synergy Energy Solutions (Go Home)</span>
              <Logo className='h-12 sm:h-16 dark:fill-white' />
            </A>

            <div className='grid grid-flow-row gap-2'>

              <span>Check us out</span>

              <ul className='grid grid-flow-row grid-cols-4 gap-3 w-fit'>

                {
                  [
                    {
                      title: 'Instagram',
                      href: 'https://instagram.com/energybysynergyofficial',
                      Icon: InstagramLogo
                    },
                    {
                      title: 'Facebook',
                      href: 'https://facebook.com/synergyenergysolutions',
                      Icon: FacebookLogo
                    },
                    {
                      title: 'LinkedIn',
                      href: 'https://linkedin.com/synergyenergysolutions',
                      Icon: LinkedInLogo
                    },
                    {
                      title: 'Google',
                      href: 'https://google.com/?query=synergy%20energy%20solutions',
                      Icon: GoogleLogo
                    }
                  ].map(({ title, href, Icon }) => {
                    return (
                      <li key={title}>
                        <Link href={href} title={title} className='group/link'>

                          <span className='sr-only'>{title}</span>
                          
                          <Icon className='
                            w-8 h-8
                            fill-primary-500
                            opacity-0 group-hover/link:opacity-100
                            transition-opacity duration-[0s] delay-500 group-hover/link:delay-[0s]
                          '/>

                          <div className='
                            w-8 h-8
                            absolute overflow-hidden
                            -translate-y-full
                          '>

                            <div className='
                              absolute top-1/2 left-1/2
                              w-16 h-16
                              -translate-y-1/2 -translate-x-1/2 -rotate-225
                            '>

                              <div className='
                                overflow-hidden
                                w-16 h-16 group-hover/link:w-0
                                transition-[width] duration-500 ease-[cubic-bezier(0,.1,.1,.6)]
                              '>

                                <div className='w-16 h-16 absolute rotate-225'>

                                  <Icon className='
                                    w-8 h-8
                                    top-1/2 left-1/2
                                    -translate-y-1/2 -translate-x-1/2
                                    fill-gray-600 dark:fill-gray-300
                                  '/>
                                  
                                </div>

                              </div>
                              
                            </div>
                            
                          </div>

                        </Link>
                      </li>
                    )
                  })
                }

              </ul>

            </div>

          </div>

          <div className='grid grid-flow-row gap-4'>

            <span className='font-bold'>Contact Us</span>

            <ul className='text-sm grid grid-flow-row gap-2'>

              <li>
                <Link href='mailto:support@energybysynergy.com' type='ltr'>support@energybysynergy.com</Link>
              </li>

              <li>
                <Link href='tel:18445277377' type='ltr'>(844) 527-7377</Link>
              </li>

              <li>
                <address className='whitespace-nowrap'>
                  3950 RCA Blvd. Suite 5003
                  <br />
                  Palm Beach Gardens, FL 33410
                </address>
              </li>

              <li>
                <Link href='/contact-us' type='ltr'>Contact / FAQ</Link>
              </li>

            </ul>

          </div>

          <div className='grid grid-flow-row gap-4'>

            <span className='font-bold'>Navigation</span>

            <ul className='text-sm grid grid-flow-row gap-2 text-gray-500 dark:text-gray-400'>

              <li>
                <Link href='/about-us' type='ltr'>About Us</Link>
              </li>

              <li>
                <Link href='/energy-efficiency-services' type='ltr'>Our Services</Link>
              </li>

              <li>
                <Link href='/tune-filter' type='ltr'>Tune® Filter</Link>
              </li>

              <li>
                <Link href='/red-e-protocol' type='ltr'>RED-E Protocol</Link>
              </li>

              <li>
                <Link href='/careers' type='ltr'>Careers</Link>
              </li>

            </ul>

          </div>

          <div className='grid grid-flow-row gap-4'>

            <span className='font-bold'>Legal</span>

            <ul className='text-sm grid grid-flow-row gap-2 text-gray-500 dark:text-gray-400'>

              <li>
                <Link href='/terms' type='ltr'>Terms and Conditions</Link>
              </li>

              <li>
                <Link href='/privacy' type='ltr'>Privacy Policy</Link>
              </li>

              <li>
                <Link href='/refund-policy' type='ltr'>Refund Policy</Link>
              </li>

              <li>
                <Link href='/shipping-policy' type='ltr'>Shipping Policy</Link>
              </li>

              <li>
                <Link href='/cancellation-policy' type='ltr'>Cancellation Policy</Link>
              </li>

            </ul>

          </div>
          
        </div>

        <div className='flex flex-col-reverse md:flex-row md:justify-between md:items-end gap-8'>

          <span className='block text-xs text-gray-400'>© {year} Synergy Energy Solutions – All&nbsp;rights&nbsp;reserved | Design&nbsp;by&nbsp;<Link href='https://andrilla.net' rel='prefetch'>Andrilla&nbsp;LLC</Link></span>

          <div className='flex items-center gap-2'>

            <VisaLogo className='h-4 dark:brightness-150' />

            <MastercardLogo className='w-8' />

            <DiscoverLogo className='h-4 fill-[#201d1c] dark:fill-gray-200' />

            <AmericanExpressLogo className='h-8' />

          </div>

        </div>

      </div>

    </footer>
  )
}

export default Footer