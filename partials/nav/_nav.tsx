// * Types
import type { FunctionComponent } from 'react'
import type { SideNav as SideNavProps } from '@/typings/partials'

// * Hooks
import { useState } from 'react'

// * Components
import Button from '@/components/button'
import NavItem from './nav-item'
import { Transition } from '@headlessui/react'
import DropDown from '@/components/drop-down'

// * Functions
import { addClass, removeClass } from '@/utils/class-management'

const Nav: FunctionComponent = () => {
  return (
    <nav
      aria-label='Main Menu'
      className='
        hidden md:flex justify-between items-center gap-6
        flex-1
      '
    >

      <ul className='contents'>

        <NavItem href='/about-us' label='About Us' />

        <DropDown
          type='hover'
          trigger={<NavItem href='/energy-efficiency-solutions' label='Our Services' as='div' />}
          items={[
            {
              label: 'Tune® Energy',
              href: '/tune-filter'
            },
            {
              label: 'Residential Solar',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Commercial Solar (coming soon)',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Solar Water Heating',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Solar Pool Heating',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Solar and Tune® Energy Installation',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Battery Storage',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'EV Chargers',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Automation',
              href: '/energy-efficiency-solutions'
            },
            {
              label: 'Remote Monitoring (coming soon)',
              href: '/energy-efficiency-solutions'
            },
            'divider',
            {
              label: 'See All Services',
              href: '/energy-efficiency-solutions',
              addClass: 'text-base'
            }
          ]}
          addClass='mr-auto'
        />

        <NavItem href='/tune-filter' label='Tune® Filter' addClass='hidden lg:block' />

        <NavItem href='/red-e-protocol' label='RED-E Protocol' addClass='hidden xl:block' />

        <NavItem href='/careers' label='Careers' addClass='hidden xl:block' />

        <NavItem href='/contact-us' label='Contact Us' addClass='hidden lg:block' />
        
        <li>

          <Button href='#contact' addClass='md:px-4 lg:px-8 md:py-3'>Get a Quote</Button>

        </li>

      </ul>
      
    </nav>
  )
}

export default Nav

export const SideNav: FunctionComponent<SideNavProps> = ({ isOpen, setIsOpen, toggleNavOpen }) => {
  return (
    <nav
      aria-label='Main Menu Extended'
      className='contents'
    >

      <button
        onClick={toggleNavOpen}
        aria-controls='Main Menu Extended'
        className='
          bg-primary-400 hover:bg-primary-50 contrast-more:bg-white contrast-more:hover:bg-white
          p-2
          flex justify-center items-center
          rounded-xl
          group/nav
          transition-[background_color] duration-300
          contrast-more:border-[0.5px] contrast-more:border-black
          z-10
        '
      >

        <span className='sr-only'>Main Menu</span>
        
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 64 32.4' className='fill-primary-50 group-hover/nav:fill-primary-400 contrast-more:fill-primary-400 w-7 h-7 transition-[fill] duration-300'>

          <path className={`
            ${isOpen
              ? '-rotate-45 translate-y-3 translate-x-2.5 scale-y-95 scale-x-90'
              : 'group-hover/nav:translate-x-[1px] group-hover/nav:scale-x-[0.85]'
            }
            origin-center transition-transform duration-300
          `} d='M2.4,4.8h59.2c1.3,0,2.4-1,2.4-2.4S62.9,0,61.6,0H2.4C1.1,0,0,1.1,0,2.4S1.1,4.8,2.4,4.8z'/>

          <g className={`${isOpen ? 'rotate-45 translate-x-[21px] -translate-y-[16px]' : ''}`}>

            <path className={`
              ${isOpen
                ? 'scale-y-95 scale-x-90'
                : 'group-hover/nav:translate-x-[1.5px] group-hover/nav:scale-x-[0.67]'
              }
              origin-center transition-transform duration-300
            `} d='M2.4,18.6h59.2c1.3,0,2.4-1,2.4-2.4c0-1.4-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1.1-2.4,2.4C0,17.5,1.1,18.6,2.4,18.6z'/>

            <path className={`
              ${isOpen
                ? '-translate-y-[0.8125rem] scale-y-95 scale-x-90'
                : 'group-hover/nav:translate-x-0.5 group-hover/nav:scale-x-50'
              }
              origin-center transition-transform duration-300
            `} d='M2.4,32.4h59.2c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1-2.4,2.4C0,31.3,1.1,32.4,2.4,32.4z'/>

          </g>

        </svg>

      </button>

      <Transition
        show={isOpen}
        appear
        enter='motion-safe:transition-transform motion-reduce:transition-opacity duration-500 ease-in-out'
        enterFrom='motion-safe:translate-x-full motion-reduce:opacity-0'
        enterTo='motion-safe:translate-x-0 motion-reduce:opacity-100'
        leave='motion-safe:transition-transform motion-reduce:transition-opacity duration-500 ease-in-out'
        leaveFrom='motion-safe:translate-x-0 motion-reduce:opacity-100'
        leaveTo='motion-safe:translate-x-full motion-reduce:opacity-0'
        className={`
          w-full h-[100dvh]
          absolute inset-0
          px-6
          shadow-3xl
          bg-gray-300 bg-opacity-90 dark:bg-gray-700 dark:bg-opacity-90
          transparency-reduce:bg-opacity-100
          backdrop-blur-lg transparency-reduce:backdrop-filter-none
          overflow-y-scroll
        `}
      >

        <ul className={`
          before:content-[""]
          before:fixed before:inset-0 before:bottom-auto before:z-10
          before:h-20 sm:before:h-24
          before:backdrop-blur
          mt-32 sm:mt-36
          flex flex-col justify-center items-start gap-4
          h-[calc(100%-8rem)] sm:h-[calc(100%-9rem)]
          text-3xl
        `}>

          <NavItem onClick={toggleNavOpen} href='/about-us' label='About Us' />

          <DropDown
            onClick={toggleNavOpen}
            trigger={<NavItem as='div' label='Our Services' />}
            items={[
              {
                label: 'Tune® Energy',
                href: '/tune-filter'
              },
              {
                label: 'Residential Solar',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Commercial Solar (coming soon)',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Solar Water Heating',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Solar Pool Heating',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Solar and Tune® Energy Installation',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Battery Storage',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'EV Chargers',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Automation',
                href: '/energy-efficiency-solutions'
              },
              {
                label: 'Remote Monitoring (coming soon)',
                href: '/energy-efficiency-solutions'
              },
              'divider',
              {
                label: 'See All Services',
                href: '/energy-efficiency-solutions',
                addClass: 'text-base'
              }
            ]}
          />

          <NavItem onClick={toggleNavOpen} href='/tune-filter' label='Tune® Filter' />

          <NavItem onClick={toggleNavOpen} href='/red-e-protocol' label='RED-E Protocol' />

          <NavItem onClick={toggleNavOpen} href='/careers' label='Careers' />

          <hr className='w-full border border-gray-600 dark:border-gray-400 my-6' />

          <NavItem onClick={toggleNavOpen} href='/contact-us' label='Contact Us' />
          
          <li className='text-base'>

            <Button onClick={toggleNavOpen} href='#contact' padding='lg'>Get a Quote</Button>

          </li>

          <hr className='border-none my-6' />

          <NavItem onClick={toggleNavOpen} href='https://synergyenergysolutions.net' label='Rep Portal' addClass='text-base absolute bottom-4' />

        </ul>

      </Transition>

    </nav>
  )
}