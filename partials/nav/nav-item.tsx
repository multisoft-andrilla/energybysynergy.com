// * Types
import type { FunctionComponent } from 'react'
import type { NavItem as NavItemProps } from '@/typings/partials'

// * Hooks
import { useRouter } from 'next/router'

// * Components
import Link, { lineLtrClasses } from '@/components/link'

const NavItem: FunctionComponent<NavItemProps> = ({ href, label, className, addClass, onClick, as = 'li' }) => {

  const router = useRouter(),
        { asPath } = router,
        isActive = asPath === href,
        Item = as

  return (
    <Item className={`
      ${className || `
        whitespace-nowrap font-display font-semibold
        ${isActive ? 'text-primary-700 dark:text-primary-400' : 'text-gray-900 dark:text-gray-50'}
      `}
      ${addClass || ''}
    `}>

      {
        href
        ?
        <Link onClick={onClick} type={isActive ? 'static' : 'fill-ltr'} href={href} addClass='block w-min'>{label}</Link>
        :
        <span className={`
          ${lineLtrClasses}
          block w-min
        `}>{label}</span>
      }

    </Item>
  )
}

export default NavItem