// * Types
import type { FunctionComponent } from 'react'
import type { ContactForm as ContactFormProps } from '@/typings/partials'

// * Components
import { Form, Input } from '@/components/forms'
import Button from '@/components/button'
import { toast } from 'react-hot-toast'

// * Utils
import { isDefined, toCamelCase } from '@/utils/functions'
import { submitForm } from '@/firebase/submit-form'
import { contactFormID } from '@/lib/firebase'

const ContactForm: FunctionComponent<ContactFormProps> = ({ andrillaContent, ownerID, button, className, addClass, addId }) => {

  const onSubmit = (e: any) => {

    e.preventDefault()

    const form = e.target,
          inputs = form.querySelectorAll('input'),
          formFields = [...inputs]

    let values: {name: string, id: string, value: string}[] = []

    formFields.map((input: HTMLInputElement, index) => {

      const { value } = input
      let { name, id } = input

      if (name.includes('[name]') || !isDefined(value)) return

      name = name.replace('[id]', '')

      if (!isDefined(id)) id = toCamelCase(name)

      if (id.includes('-')) id = id.split('-')[0]
      
      values.push({
        name,
        id,
        value
      })

      if (formFields.length === index + 1) {
        submitForm(andrillaContent, ownerID, values, contactFormID)
        toast.success('Thank you for your submission. We will be in touch with you soon.')
        form.reset()
      }

    })

  }

  return (
    <div itemScope itemType='http://schema.org/ContactPage' className={`${className || 'bg-white dark:bg-gray-900 rounded-4xl shadow-3xl p-4 max-w-xl mx-auto'} ${addClass || ''}`}>

      <Form
        itemProp='potentialAction'
        itemScope
        itemType='http://schema.org/CommunicateAction'
        className='grid grid-flow-row gap-4 sm:grid-cols-2'
        privacyMessage={{
          addClass: 'sm:col-span-2'
        }}
        onSubmit={onSubmit}
      >

        <Input addId={addId} name='First Name' label='*' placeholder='*' />

        <Input addId={addId} name='Last Name' label='*' placeholder='*' />

        <Input addId={addId} addClass='sm:col-span-2' type='email' name='Email' label='*' placeholder='Email Address' />

        <Input addId={addId} type='tel' name='Phone' label='*' placeholder='Phone Number' />

        <Input addId={addId} name='ZIP Code' label='*' placeholder='*' />

        <Button padding='lg' rounded='xl' addClass='sm:col-span-2 font-extrabold text-lg' type='submit'>{button?.label || 'Get Started'}</Button>

      </Form>

    </div>
  )
}

export default ContactForm