const plugin = require('tailwindcss/plugin')

const mainSans = [
  'var(--typeface-montserrat-alt1)',
  '"SF Pro Text"',
  'SFProText-Regular',
  '-apple-system',
  '"Helvetica Neue"',
  'BlinkMacSystemFont',
  'Arial',
  'Helvetica',
  'sans-serif'
]

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './partials/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './templates/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    textShadow: {
      sm: '0 0 0.125rem hsl(0 0% 0% / 0.25)',
      DEFAULT: '0 0 0.25rem hsl(0 0% 0% / 0.25)',
      md: '0 0 0.5rem hsl(0 0% 0% / 0.25)',
      lg: '0 0 0.75rem hsl(0 0% 0% / 0.25)',
      xl: '0 0 1rem hsl(0 0% 0% / 0.25)',
      '2xl': '0 0 2rem hsl(0 0% 0% / 0.25)',
      '3xl': '0 0 3rem hsl(0 0% 0% / 0.25)',
      none: 'none'
    },
    borderGradient: {
      'to-t': 'linear-gradient(to top, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-tr': 'linear-gradient(to top right, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-r': 'linear-gradient(to right, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-br': 'linear-gradient(to bottom right, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-b': 'linear-gradient(to bottom, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-bl': 'linear-gradient(to bottom left, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-l': 'linear-gradient(to left, var(--tw-gradient-from), var(--tw-gradient-to))',
      'to-tl': 'linear-gradient(to top left, var(--tw-gradient-from), var(--tw-gradient-to))'
    },
    extend: {
      fontSize: {
        '2xs': '0.625rem'
      },
      colors: {
        'primary': {
          50: 'hsl(var(--brand-primary-50) / <alpha-value>)',
          100: 'hsl(var(--brand-primary-100) / <alpha-value>)',
          200: 'hsl(var(--brand-primary-200) / <alpha-value>)',
          300: 'hsl(var(--brand-primary-300) / <alpha-value>)',
          400: 'hsl(var(--brand-primary-400) / <alpha-value>)',
          500: 'hsl(var(--brand-primary-500) / <alpha-value>)',
          600: 'hsl(var(--brand-primary-600) / <alpha-value>)',
          700: 'hsl(var(--brand-primary-700) / <alpha-value>)',
          800: 'hsl(var(--brand-primary-800) / <alpha-value>)',
          900: 'hsl(var(--brand-primary-900) / <alpha-value>)'
        },
        'tune': {
          50: 'hsl(var(--brand-tune-50) / <alpha-value>)',
          100: 'hsl(var(--brand-tune-100) / <alpha-value>)',
          200: 'hsl(var(--brand-tune-200) / <alpha-value>)',
          300: 'hsl(var(--brand-tune-300) / <alpha-value>)',
          400: 'hsl(var(--brand-tune-400) / <alpha-value>)',
          500: 'hsl(var(--brand-tune-500) / <alpha-value>)',
          600: 'hsl(var(--brand-tune-600) / <alpha-value>)',
          700: 'hsl(var(--brand-tune-700) / <alpha-value>)',
          800: 'hsl(var(--brand-tune-800) / <alpha-value>)',
          900: 'hsl(var(--brand-tune-900) / <alpha-value>)'
        }
      },
      fontFamily: {
        sans: [ ...mainSans ],
        serif: [
          'Georgia',
          '"Times New Roman"',
          'serif'
        ],
        mono: [
          '"SF Mono"',
          'SFMono-Regular',
          'Menlo',
          'monospace'
        ],
        script: [
          '"Snell Roundhand"',
          'cursive'
        ],
        display: [ 'var(--typeface-geom)', ...mainSans ],
        'red-e': [ 'var(--typeface-red-e)', ...mainSans ]
      },
      boxShadow: {
        sm: '0 0.1875rem 0.375rem hsl(0 0% 0% / 0.1)',
        DEFAULT: '0 0.25rem 0.375rem hsl(0 0% 0% / 0.1)',
        md: '0 0.0625rem 0.625rem hsl(0 0% 0% / 0.1)',
        lg: '0 0rem 0.5rem hsl(0 0% 0% / 0.25)',
        xl: '0 0.0625rem 0.625rem hsl(0 0% 0% / 0.25)',
        '2xl': '0 0.5rem 4rem hsl(0 0% 0% / 0.07)',
        '3xl': '0 1.25rem 3.125rem hsl(0 0% 0% / 0.1)',
        inset: 'inset 0 0.25rem 0.5rem hsl(0 0% 0% / 0.1)'
      },
      dropShadow: {
        sm: '0 0.1875rem 0.375rem hsl(0 0% 0% / 0.1)',
        DEFAULT: '0 0.25rem 0.375rem hsl(0 0% 0% / 0.1)',
        md: '0 0.0625rem 0.625rem hsl(0 0% 0% / 0.1)',
        lg: '0 0rem 0.5rem hsl(0 0% 0% / 0.25)',
        xl: '0 0.0625rem 0.625rem hsl(0 0% 0% / 0.25)',
        '2xl': '0 0.5rem 4rem hsl(0 0% 0% / 0.07)',
        '3xl': '0 1.25rem 1.875rem hsl(0 0% 0% / 0.16)'
      },
      borderRadius: {
        '4xl': '2.25rem',
        '5xl': '3.5rem'
      },
      transitionTimingFunction: {
        DEFAULT: 'ease'
      },
      rotate: {
        225: '225deg'
      }
    },
  },
  plugins: [
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          'border-gradient': (value) => ({
            borderImageSource: value,
            borderImageSlice: '1 2 3 4'
          })
        },
        { values: theme('borderGradient') }
      )
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value
          })
        },
        { values: theme('textShadow') }
      )
      matchUtilities(
        {
          'counter': (value) => ({
            counterIncrement: value
          })
        }
      )
      matchUtilities(
        {
          'counter-reset': (value) => ({
            counterReset: value
          })
        }
      )
    }),
    plugin(function ({ addVariant }) {
      addVariant('desktop', '@media (pointer: fine)')
      addVariant('mobile', '@media (pointer: coarse)')
      addVariant('transparency-reduce', '@media (prefers-contrast: more)')
    }),
    plugin(function ({ addComponents }) {
      addComponents({
        '.error': {
          backgroundColor: 'rgb(254 202 202) !important',
          borderColor: 'rgb(239 68 68) !important',
          boxShadow: '0 0 0.2rem 0.1rem rgb(239 68 68)'
        }
      })
    })
  ],
}