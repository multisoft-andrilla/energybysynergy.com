// * Types
import type { FunctionComponent } from 'react'
import type { IFrame as IFrameProps } from '../typings/components'

/**
 * # iFrame Component
 * 
 * - An `<iframe/>` with preset classes, backup, and required title for SEO.
 * 
 * ## Props
 * 
 * - `src` - The URL of the website to be displayed.
 * - `title` - The name of the embed.
 * - `allow` - A string or array of features that the iFrame is allowed to access.
 * - `referrerPolicy` [referrerPolicy='no-referrer-when-downgrade'] - A string of the referrer policy.
 * - `sandbox` - A string or array of sanbox features to be used by the iFrame.
 * - `loading` - How the iFrame should load.
 * - `className` - Replaces all default classes to apply to the embed.
 * - `addClass` - Additional classes to apply to the embed.
 * 
 * ## Examples
 * 
 * @example
 *   <IFrame src='https://apple.com' title='Apple' />
 * 
 * @example
 *   <IFrame src='https://player.vimeo.com/video/557741207' title='My Video' allow={['fullscreen', 'autoplay', 'picture-in-picture']} />
 */
const IFrame: FunctionComponent<IFrameProps> = ({
  src,
  id,
  title,
  allow,
  referrerPolicy = 'no-referrer-when-downgrade',
  sandbox,
  loading,
  className,
  addClass
}) => {

  const allAllowProperties = 'accelerometer, autoplay, camera, encrypted-media, fullscreen, gyroscope, magnetometer, microphone, payment, picture-in-picture, publickey-credentials-get, usb',
        allSanboxProperties = 'allow-downloads allow-forms allow-modals allow-orientation-lock allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-presentation allow-same-origin allow-scripts allow-top-navigation allow-top-navigation-by-user-activation allow-top-navigation-to-custom-protocols'

  const allAllowIsAllowed = allow === 'allow-all',
        allSandboxIsAllowed = sandbox === 'allow-all',
        allowIsString = typeof(allow) === 'string',
        sandboxIsString = typeof(sandbox) === 'string'

  return (
    <iframe
      id={id}
      src={src}
      className={`${className || 'w-full h-full min-w-[12rem] min-h-[32rem]'} ${addClass || ''}`}
      title={title}
      allow={allAllowIsAllowed ? allAllowProperties : (allowIsString ? allow : allow?.join('; '))}
      referrerPolicy={referrerPolicy}
      sandbox={allSandboxIsAllowed ? allSanboxProperties : (sandboxIsString ? sandbox : sandbox?.join(' '))}
      loading={loading}
      allowFullScreen={allow?.includes('fullscreen')}
    ></iframe>
  )
}

export default IFrame