import { FunctionComponent } from 'react'
import { Card as CardProps } from '@/typings/components'

const Card: FunctionComponent<CardProps> = ({ heading, body, className, addClass, options }) => {

  const Heading = options?.heading?.as || 'h2'

  return (
    <div className={`
      ${className || `
        bg-gray-50 dark:bg-gray-900
        rounded-3xl
        max-w-2xl
        overflow-y-hidden
        border-[1.5rem] sm:border-[3rem] border-gray-50 dark:border-gray-900
      `}
      ${addClass || ''}
    `}>

      <Heading className={`
        ${options?.heading?.className || 'font-display font-black text-3xl sm:text-4xl'}
        ${options?.heading?.addClass || 'text-gray-700 dark:text-gray-50'}
      `}>{heading}</Heading>
      <br />

      <p className={`
        ${options?.body?.className || 'sm:text-xl'}
        ${options?.body?.addClass || 'text-gray-500 dark:text-gray-300'}
      `}>{body}</p>

    </div>
  )
}

export default Card