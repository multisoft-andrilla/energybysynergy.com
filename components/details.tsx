// * Types
import type { FunctionComponent } from 'react'
import type { Details as DetailsProps } from '@/typings/components'

// * Components
import { Disclosure, Transition } from '@headlessui/react'
import ChevronDown from '@/svgs/icons/chevron-down.svg'
import { Fragment } from 'react'

const Details: FunctionComponent<DetailsProps> = ({ summary, className, addClass, children }) => {
  return (
    <Disclosure
      as='div'
      className={`
        ${className || `
          max-w-2xl
          bg-gray-50 dark:bg-gray-600 rounded-3xl p-2
        `}
        ${addClass || ''}
      `}
    >
      {({ open }) => (
        <>
          <Disclosure.Button className={`
            w-full
            pl-4 py-4 pr-12 rounded-2xl
            text-gray-50 font-bold text-2xl text-left font-display
            bg-gray-600 dark:bg-gray-800 hover:bg-gray-500 dark:hover:bg-gray-700 active:bg-gray-700 dark:active-gray-900
            transition-[background-color] duration-300
          `}>

            {summary}

            <ChevronDown className={`${open ? 'rotate-180' : ''} absolute right-4 top-1/2 -translate-y-1/2 w-6 fill-white transition-transform duration-300`} />

          </Disclosure.Button>

          <Transition
            as={Fragment}
            enter='transition-[transform,opacity] duration-300 ease-out'
            enterFrom='scale-95 opacity-0'
            enterTo='scale-100 opacity-100'
            leave='transition-[transform,opacity] duration-100 ease-out'
            leaveFrom='scale-100 opacity-100'
            leaveTo='scale-95 opacity-0'
          >
            
            <Disclosure.Panel className='text-gray-500 dark:text-gray-200 px-2 pt-4 pb-2'>{children}</Disclosure.Panel>

          </Transition>
        </>
      )}
    </Disclosure>
  )
}

export default Details