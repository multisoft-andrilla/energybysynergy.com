// * Types
import type{ FunctionComponent, Dispatch, SetStateAction } from 'react'
import type { Video as VideoProps } from '@/typings/components'

// * Modules
import Vimeo from '@vimeo/player'

// * Hooks
import { useState } from 'react'
import { useVimeoVideoInfo } from '@/hooks/use-vimeo-video-info'

// * Functions
import { isDefined } from '@/utils/functions'

// * Components
import Img from 'next/image'

// * SVGs
import Play from '@/svgs/icons/play.svg'
import IFrame from './iframe'

/**
 * # Video Component
 * 
 * - A component that renders a video, and takes advantage of *Vimeo* API for additional features like autoplay and generating facades.
 * - Use `useFacade` when the video is visible on page load, for best performance with Vimeo videos.
 * 
 * ## Props
 * 
 * - `src` - The video URL to play.
 * - `useFacade` [useFacade=false] - If true, the component displays a poster image with a play button that, when clicked, plays the video (only works with *Vimeo* videos).
 * - `autoplay` [autoplay=false] - If true, the video will be allowed to autoplay, and—if a *Vimeo* video—autoplay will be triggered.
 * - `title` - Use for SEO and accessibility (if *Vimeo* video, defaults to the title in *Vimeo*).
 * - `className` - Replaces all default classes to apply to the video.
 * - `addClass` - Additional classes to apply to the video.
 * 
 * ## Examples
 * 
 * @example
 *   <Video src='https://player.vimeo.com/video/557741207' />
 * 
 * @example
 *   <Video src='https://player.vimeo.com/video/557741207' useFacade autoplay />
 */
const Video: FunctionComponent<VideoProps> = ({
  src,
  useFacade = false,
  thumbnail,
  autoplay = false,
  loop,
  title,
  className = 'w-full max-w-3xl rounded-lg aspect-video overflow-hidden',
  addClass
}) => {

  const isVimeoVideo = src.includes('vimeo')
  
  const id = `vimeo-${src.replace(/\D/g, '')}`,
        facadeState = useState<boolean>(useFacade && isVimeoVideo),
        [isFacade, setIsFacade] = facadeState,
        videoInfo = useVimeoVideoInfo(src, id, autoplay, facadeState, playVideo, loop)

  return useFacade
  ?
  <>
      
    {
      isFacade
      ?
      <div
        className={`${className} ${addClass || ''}`}
        onPointerUp={videoInfo?.title !== 'Error' ? (e) => playVideo(id, facadeState, loop, e) : undefined}
      >

        {
          (videoInfo?.thumbnail || thumbnail)
          ?
          <>

            <Img
              src={thumbnail?.src || videoInfo.thumbnail!}
              alt={videoInfo.title ? videoInfo.title : (thumbnail?.alt || 'Video Thumbnail')}
              width={thumbnail?.width || 1920}
              height={thumbnail?.height || 1080}
              className={thumbnail?.className || 'absolute inset-0 object-cover rounded-lg bg-black'}
            />
          
            <button
              className='
                absolute left-1/2 top-1/2
                -translate-x-1/2 -translate-y-1/2
                w-[3.75rem] h-9
                rounded-md
                flex justify-center items-center
                bg-black/80 hover:bg-white/80
                fill-white hover:fill-primary-400
                transition-[background-color_fill] duration-200
                cursor-pointer
              '
              onClick={(e) => playVideo(id, facadeState, loop, e)}
              type='button'
            >

              <span className='sr-only'>Play {videoInfo.title ? videoInfo.title : 'Video'}</span>
              <Play className='w-6' />

            </button>
            
          </>
          :
          <video
            src='/videos/preload-animation.webm'
            title={title || 'Your Video is Loading'}
            muted autoPlay loop
            className='
              absolute left-0 right-0 top-1/2
              w-full
              -translate-y-1/2
              rounded-lg
              bg-black
            '
          />
        }

      </div>
      :
      <IFrame
        id={id}
        src={src}
        title={title || (videoInfo?.title || 'Video')}
        allow={['fullscreen', 'picture-in-picture', 'autoplay']}
        loading='eager'
        className={`
          ${className}
          ${addClass || ''}
        `}
      />
    }

  </>
  :
  <IFrame
    id={id}
    src={src}
    title={title || (videoInfo?.title || 'Video')}
    allow={autoplay ? ['fullscreen', 'picture-in-picture', 'autoplay'] : ['fullscreen', 'picture-in-picture']}
    loading={autoplay ? 'eager' : 'lazy'}
    className={`${className} ${addClass}`}
  />
}

const playVideo = (vimeoID: string, facadeState: [boolean, Dispatch<SetStateAction<boolean>>], loop?: boolean, e?: any) => {

  e?.preventDefault()

  const [isFacade, setIsFacade] = facadeState

  isFacade ? setIsFacade(false) : null

  if (isDefined(document.querySelector(`#${vimeoID}`))) {
    const player = new Vimeo(vimeoID)
    player.play()
    loop ? player.setLoop(loop) : null
  } else {
    setTimeout(() => {
      playVideo(vimeoID, facadeState, e)
    }, 1000)
  }
}

export default Video