// * Types
import type { FunctionComponent } from 'react'
import type { Divider as DividerProps } from '@/typings/components'

// * Components
import BlockyWave from '@/svgs/backgrounds/blocky-wave.svg'

const Divider: FunctionComponent<DividerProps> = ({ style, className, addClass }) => {
  return (
    <>

      <hr className='sr-only' />

      <div aria-hidden='true' className={`
        ${className || `absolute inset-0 w-screen overflow-hidden translate-y-1/4`}
        ${addClass || ''}
      `}>

        <div className='w-full h-24 isolate'>

          <div className={`
            fixed left-1/2
            -translate-x-1/2 ${style === 'blocky-180' ? 'rotate-180' : ''}
            flex justify-center items-center
          `}>

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />

            <BlockyWave className='w-64' />
            
          </div>

        </div>
        
      </div>
    
    </>
  )
}

export default Divider