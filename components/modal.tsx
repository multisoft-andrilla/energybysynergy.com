// * Types
import type { FunctionComponent } from 'react'
import type { Modal as ModalProps } from '@/typings/components'

// * Hooks
import React, { useState } from 'react'

// * Components
import { Fragment } from 'react'
import { Transition, Dialog } from '@headlessui/react'
import Xmark from '@/svgs/icons/xmark.svg'

/**
 * # Modal Component
 * 
 * - A slightly opinionated, functional modal component that can display any content inside.
 * 
 * ## Props
 * 
 * - `trigger` - The component that will trigger the modal when clicked (should be render a <button> for ARIA).
 * - `place` [place='bottom'] - The position where the modal will appear (either 'bottom' or 'center').
 * - `className` - Replaces all default classes to apply to the embed.
 * - `addClass` - Additional classes to apply to the embed.
 * - `children` - The content to display inside the modal.
 * 
 * ## Example
 * 
 * @example
 *   <Modal trigger={<Button>Open Modal</Button>}>
 *     <Calendly />
 *   </Modal>
 */
const Modal: FunctionComponent<ModalProps> = ({ trigger, place = 'bottom', className, addClass, children }) => {

  const [isOpen, setIsOpen] = useState(false),
        openModal = () => setIsOpen(true),
        closeModal = () => setIsOpen(false)

  const Trigger = () => React.cloneElement(trigger, { onClick: openModal })

  const enterFrom = place === 'center'
    ? '-translate-y-[calc(50%-12rem)]'
    : 'translate-y-full sm:-translate-y-[calc(50%-12rem)]'

  const mainPosition = place === 'center'
    ? '-translate-y-1/2'
    : 'translate-y-0 sm:-translate-y-1/2'

  const leaveTo = place === 'center'
    ? '-translate-y-[calc(50%+8rem)]'
    : 'translate-y-full sm:-translate-y-[calc(50%+8rem)]'

  return (
    <>
    
      <Trigger />

      <Transition show={isOpen} as={Fragment}>

        <Dialog onClose={closeModal} className='z-50'>

          {/* Backdrop */}
          <Transition.Child
            as={Fragment}
            enter='transition-[opacity_transform] duration-500 ease-out'
            enterFrom='opacity-0 scale-75'
            enterTo='opacity-100 scale-100'
            leave='transition-[opacity_transform] duration-300 ease-in'
            leaveFrom='opacity-100 scale-100'
            leaveTo='opacity-0 scale-75'
          >
            <div className='fixed inset-0 bg-neutral-200/50 dark:bg-neutral-900/50 backdrop-blur-sm cursor-pointer' aria-hidden='true' />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter='transition-[opacity_transform] duration-500 ease-out'
            enterFrom={`opacity-0 sm:scale-50 ${enterFrom}`}
            enterTo={`opacity-100 scale-100 ${mainPosition}`}
            leave='transition-[opacity_transform] duration-300 ease-in'
            leaveFrom={`opacity-100 scale-100 ${mainPosition}`}
            leaveTo={`opacity-0 sm:scale-50 ${leaveTo}`}
          >

            <Dialog.Panel
              className={`
                ${className || `
                  fixed left-1/2
                  ${
                    place === 'center'
                    ? 'top-1/2 rounded-2xl'
                    : 'bottom-0 sm:bottom-auto sm:top-1/2 rounded-t-2xl xs:rounded-b-2xl'
                  }
                  bg-neutral-50 dark:bg-neutral-900
                  w-min
                  -translate-x-1/2
                  overflow-hidden
                  shadow-3xl shadow-primary-400/10
                `}
                ${addClass || ''}
              `}
            >
              {children}
              
            </Dialog.Panel>
            
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter='transition-[opacity_transform] duration-500 ease-out'
            enterFrom='opacity-0 scale-75'
            enterTo='opacity-100 scale-100'
            leave='transition-[opacity_transform] duration-300 ease-in'
            leaveFrom='opacity-100 scale-100'
            leaveTo='opacity-0 scale-75'
          >

            <button
              onClick={closeModal}
              className='
                fixed top-4 right-4
                bg-primary-600 hover:bg-primary-400
                rounded-full
                h-7 w-7 desktop:hover:w-20
                overflow-x-hidden
                transition-[background-color_width] duration-300 ease-in-out
                group/button
              '
            >
              <div className='
                absolute inset-1 left-auto
                flex items-center gap-1
                desktop:group-hover/button:px-1
                transition-[padding] duration-300 ease-in-out
              '>

                <span className='text-white text-xs leading-[1] uppercase font-medium'>
                  Close<span className='sr-only'> Popup</span>
                </span>

                <Xmark className='
                  w-5 h-5
                  fill-white stroke-white stroke-1
                  rotate-90 group-hover/button:rotate-0
                  transition-transform duration-300 ease-in-out
                '/>
                
              </div>
            </button>
            
          </Transition.Child>

        </Dialog>

      </Transition>

    </>
  )
}

export default Modal