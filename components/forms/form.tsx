// * Types
import type { FunctionComponent } from 'react'
import type { Form as FormProps } from '@/typings/components'

// * Components
import Link from '../link'

const Form: FunctionComponent<FormProps> = ({
  itemProp,
  itemScope,
  itemType,
  onSubmit,
  privacyMessage,
  className,
  addClass,
  children
}) => {
  return (
    <form
      itemProp={itemProp}
      itemScope={itemScope}
      itemType={itemType}
      onSubmit={onSubmit}
      className={`${className || ''} ${addClass || ''}`}
    >

      {children}

      <span className={`${privacyMessage?.className || 'text-2xs text-center text-gray-500'} ${privacyMessage?.addClass || ''}`}>Your information is private and never shared. Learn more about how we use your data in our <Link href='/privacy'>Privacy Policy</Link>.</span>

    </form>
  )
}

export default Form