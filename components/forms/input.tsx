// * Types
import type { FunctionComponent } from 'react'
import type { Input as InputProps, CheckboxRadioInput as CheckboxRadioInputProps } from '@/typings/components'

// * Hooks
import { useState, useRef } from 'react'

// * SVGs
import Checkmark from '@/svgs/icons/checkmark.svg'
import CircleFill from '@/svgs/icons/circle-fill.svg'

// * Functions
import { getRandomNumber, toCamelCase } from '@/utils/functions'

// * Styles
const inputClasses = `
  w-full input
  px-4 pt-3 pb-2 rounded-3xl
  bg-white hover:bg-gray-200 focus-within:!bg-white dark:bg-gray-900 dark:hover:bg-gray-700 dark:focus-within:!bg-gray-900
  font-display
  border border-gray-200 contrast-more:border-gray-900 contrast-more:dark:border-gray-50
  outline-none
  transition-[background-color_color] duration-200
  grid grid-flow-row gap-1
  cursor-pointer
`

const labelClasses = `text-xs font-semibold text-gray-700 dark:text-gray-500 text-left`

const Input: FunctionComponent<InputProps> = ({
  type = 'text',
  label,
  placeholder,
  required = true,
  name,
  addId,
  min,
  max,
  rows,
  cols,
  resize = false,
  options,
  autocomplete,
  className,
  addClass
}) => {

  const id = addId ? `${toCamelCase(name)}-${addId}` : `${toCamelCase(name)}`

  if (placeholder === '*') placeholder = name
  if (label === '*') label = name
  if (required && label) label = `${label} *`
  if (required && !label && placeholder) placeholder = `${placeholder} *`

  // + Text Boxes
  if (type === 'text' || type === 'email' || type === 'tel' || type === 'password' || type === 'time') return (
    <label htmlFor={id} className={`${inputClasses} ${className || ''} ${addClass || ''}`}>

      <span className={labelClasses}>{label}</span>

      <input
        type={type}
        placeholder={placeholder}
        required={required}
        id={id}
        name={name}
        autoComplete={autocomplete}
        className='bg-transparent text-black dark:text-white placeholder:text-gray-400'
      />

    </label>
  )

  // + Radio and CheckBoxes
  if (type === 'radio' || type === 'checkbox') return (
    <fieldset className={`grid grid-flow-row p-4 border border-gray-900 rounded h-min ${className}`}>

      <legend className={`px-2 whitespace-normal ${labelClasses}`}>{label}</legend>

      {
        options?.map(option => {
          return typeof option === 'string'
          ?
          <CheckboxRadioInput key={toCamelCase(option)} type={type} option={option} name={name} />
          :
          <CheckboxRadioInput key={toCamelCase(option.name)} type={type} option={option} name={name} />
        })
      }

    </fieldset>
  )

  // + Textarea
  if (type === 'textarea') return (
    <label htmlFor={id} className={`${inputClasses} ${className || ''} ${addClass || ''}`}>

      <span className={labelClasses}>{label}</span>

      <textarea
        placeholder={placeholder}
        required={required}
        id={id}
        name={name}
        className={`bg-transparent text-black dark:text-white placeholder:text-gray-400 ${resize ? (resize === 'x' ? 'resize-x' : resize === 'y' && 'resize-y') : 'resize-none'}`}
        minLength={min}
        maxLength={max}
        rows={rows}
        cols={cols}
      />

    </label>
  )

  // + Default
  return (
    <input
      type={type}
      placeholder={placeholder}
      required={required}
      name={name}
      className={`${inputClasses} ${className}`}
    />
  )
  
}

export default Input

const CheckboxRadioInput: FunctionComponent<CheckboxRadioInputProps> = ({ type, name, option }) => {

  const optionIsString = typeof(option) === 'string'

  const optionID = toCamelCase(optionIsString ? option : option.id),
        checkboxInput = useRef<HTMLInputElement>(null),
        [ isChecked, setIsChecked ] = useState(false)

  const toggleCheck = () => isChecked ? setIsChecked(false) : setIsChecked(true)

  if (isChecked && checkboxInput?.current) checkboxInput.current.checked = true
  if (!isChecked && checkboxInput?.current) checkboxInput.current.checked = false

  return (
    <div className='contents'>
    
      <input
        type={type}
        id={optionID}
        name={name}
        value={optionIsString ? option : option.id}
        className='sr-only'
        ref={checkboxInput}
      />

      <button
        type='button'
        className='
          flex justify-start gap-2
          px-1.5 py-1
          hover:bg-gray-100 active:bg-gray-200
          rounded-lg
          transition-[background-color] duration-200
          h-min
          text-left
        '
        onClick={(e: any) => {
          e.preventDefault()
          toggleCheck()
        }}
      >

        {
          type === 'checkbox'
          ?
          <div className='rounded cursor-pointer border border-gray-300 bg-white w-4 h-4 top-0.5'>

            <div className={`
              absolute top-1/2 left-1/2
              rounded
              w-4 h-4
              border border-gray-300
              bg-primary-400
              -translate-x-1/2 -translate-y-1/2
              transition-transform duration-200
              ${isChecked ? 'scale-100' : 'scale-0'}
            `}>

              <Checkmark className='
                absolute left-1/2 top-1/2
                -translate-x-1/2 -translate-y-1/2
                w-2 h-2
                overflow-visible
                fill-white
                stroke-white stroke-[7]
              '/>

            </div>

          </div>
          :
          <div className='rounded-full cursor-pointer border border-gray-300 bg-white w-4 h-4 overflow-hidden'>

            <div className={`
              absolute top-1/2 left-1/2
              rounded-full
              w-4 h-4
              border border-gray-300
              bg-primary-400
              -translate-x-1/2 -translate-y-1/2
              transition-transform duration-200
              ${isChecked ? 'scale-100' : 'scale-0'}
            `}>

                <CircleFill className='
                  absolute left-1/2 top-1/2
                  -translate-x-1/2 -translate-y-1/2 origin-center
                  w-2 h-2
                  overflow-visible
                  fill-white
                  stroke-white stroke-2
                '/>
                
              </div>

          </div>
        }

        <span className={`${labelClasses} max-w-[calc(100%-1.5rem)]`}>{optionIsString ? option : option.name}</span>

      </button>
    
    </div>
  )
}