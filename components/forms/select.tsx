// * Types
import type { FunctionComponent } from 'react'
import type { Select as SelectProps } from '@/typings/components'

// * Hooks
import { useState } from 'react'

// * Components
import { Listbox, Transition } from '@headlessui/react'
import { Fragment } from 'react'

// * SVGs
import ChevronUpChevronDown from '@/svgs/icons/chevron-up-chevron-down.svg'
import Checkmark from '@/svgs/icons/checkmark.svg'

const Select: FunctionComponent<SelectProps> = ({
  name,
  options,
  label,
  defaultValue,
  onChange,
  required = true,
  addClass
}) => {

  if (!required) options = [
    {
      id: 'none',
      name: 'None'
    },
    ...options
  ]

  const [selectedOption, setSelectedOption] = useState(options[(defaultValue || 0) + (required ? 0 : 1)])

  if (label === '*') label = name

  return (
    <div>

      <Listbox
        name={name}
        value={selectedOption}
        onChange={(newSelection: { id: string, name: string }) => {
          setSelectedOption(newSelection)
          onChange && onChange(newSelection)
        }}
      >

        { label && <Listbox.Label className='text-xs font-semibold block mb-2 text-left'>{label}{required ? ' *' : ''}</Listbox.Label> }

        <Listbox.Button className={`
          bg-white
          py-2 px-3
          rounded-xl
          flex justify-between items-center gap-2.5
          text-sm whitespace-nowrap
          min-w-[8rem]
          ${addClass || ''}
        `}>
          {selectedOption.name}
          <ChevronUpChevronDown className='h-3 overflow-visible fill-primary-900' />
        </Listbox.Button>

        <Transition
          enter='transition-[transform_opacity] duration-200 ease-out'
          enterFrom='scale-y-50 opacity-0'
          enterTo='scale-100 opacity-100'
          leave='transition-[transform_opacity] duration-75 ease-in'
          leaveFrom='scale-100 opacity-100'
          leaveTo='scale-y-50 opacity-0'
          className='z-20'
        >

          <Listbox.Options className={`
            absolute
            mt-2
            bg-white
            py-2 px-3
            rounded-xl
            text-sm whitespace-nowrap
            min-w-[8rem] max-h-32 overflow-y-scroll
            shadow-2xl
            ${addClass || ''}
          `}>
            {
              options.map(option => (
                <Listbox.Option as={Fragment} key={option.id} value={option}>
                  {({ active, selected }) => {

                    if (selectedOption.id === option.id && !selected) selected = true
                    
                    return (
                      <li className={`
                        ${active ? 'text-primary-400 bg-gray-200' : ''}
                        ${selected ? 'text-primary-400 bg-gray-100' : ''}
                        px-1.5 py-1 rounded-lg
                        flex justify-start items-center gap-2
                        transition-[color_background-colr] duration-50
                        cursor-pointer min-w-full w-fit
                        mb-0
                      `}>

                        {selected ? <Checkmark className='h-3 fill-primary-500' /> : <span aria-hidden='true' className='w-3'>&nbsp;</span>}
                        {option.name}

                      </li>
                    )
                  }}
                </Listbox.Option>
              ))
            }
          </Listbox.Options>

        </Transition>

      </Listbox>

    </div>
  )
}

export default Select