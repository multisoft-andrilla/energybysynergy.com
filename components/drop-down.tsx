// * Types
import type { FunctionComponent } from 'react'
import type { DropDown as DropDownProps } from '@/typings/components'

// * Hooks
import { useState } from 'react'

// * Components
import { Menu, Transition } from '@headlessui/react'
import ChevronDown from '@/svgs/icons/chevron-down.svg'
import { Fragment } from 'react'
import Link, { fillLiftClasses } from './link'

const DropDown: FunctionComponent<DropDownProps> = ({
  type = 'click',
  button,
  trigger,
  items,
  className,
  addClass,
  onClick
}) => {

  const typeIsHover = (type === 'hover'),
        [isOpen, setIsOpen] = useState<boolean>(false),
        openMenu = () => setIsOpen(true),
        closeMenu = () => setIsOpen(false)

  return typeIsHover
  ?
  <Menu as='li' className={`${className || ''} ${addClass || ''}`}>

    {
      isOpen
      ?
      <div aria-hidden='true' className='fixed inset-0' onMouseOver={closeMenu}>&nbsp;</div>
      :
      null
    }

    <Menu.Button
      onMouseOver={openMenu}
      className={`
        ${button?.className || `${isOpen ? 'pb-4 translate-y-2' : ''}`}
        ${button?.addClass || ''}
      `}
    >

      {trigger}

    </Menu.Button>

    <Transition
      show={isOpen}
      as={Fragment}
      enter='transition-[transform,opacity] duration-300 ease-out'
      enterFrom='scale-95 opacity-0'
      enterTo='scale-100 opacity-100'
      leave='transition-[transform,opacity] duration-200 ease-out'
      leaveFrom='scale-100 opacity-100'
      leaveTo='scale-95 opacity-0'
    >

      <Menu.Items static className={`
        ${className || `
          absolute w-96 top-10 z-10
          max-w-sm
          grid grid-flow-row gap-2
          p-4 rounded-2xl
          text-lg
          bg-gray-100 dark:bg-gray-600
          shadow-xl
          outline-none
        `}
        ${addClass || ''}
      `}>
        {
          items.map((item, index) => {

            if (item === 'divider') {
              return (
                <hr key={`menu-divider-${index}`} className='w-full border border-gray-300 dark:border-gray-500 rounded-full my-2' />
              )
            }
            
            const { label, href } = item

            return (
              <Menu.Item key={label}>
                {({ active }) => (
                  <Link
                    type='fill-lift'
                    href={href}
                    className={item.className}
                    addClass={`
                      ${item.addClass || ''}
                      ${active ? fillLiftClasses : ''}
                    `}
                  >{label}</Link>
                )}
              </Menu.Item>
            )
          })
        }
      </Menu.Items>

    </Transition>

  </Menu>
  :
  <Menu as='li'>
    {({ open }) => (
      <div className={`${open ? 'absolute z-10 pr-6 sm:relative sm:p-0' : ''}`}>

        <Menu.Button className={`
          flex items-center gap-2
          ${open ? `
            bg-gray-200 bg-opacity-90 dark:bg-gray-600 dark:bg-opacity-90
            transparency-reduce:bg-opacity-100
            backdrop-blur-lg transparency-reduce:backdrop-filter-none
            w-full
            px-2 pt-1.5 rounded-2xl
            mb-2 sm:mb-0
            sm:bg-transparent sm:backdrop-blur-none sm:p-0 sm:rounded-none
          ` : ''}
        `}>

          {trigger}

          <ChevronDown className={`${open ? 'rotate-180' : ''} -top-1 w-6 fill-current transition-transform duration-300`} />

        </Menu.Button>

        <Transition
          as={Fragment}
          enter='transition-[transform,opacity] duration-300 ease-out'
          enterFrom='scale-95 opacity-0'
          enterTo='scale-100 opacity-100'
          leave='transition-[transform,opacity] duration-200 ease-out'
          leaveFrom='scale-100 opacity-100'
          leaveTo='scale-95 opacity-0'
        >

          <Menu.Items className={`
            ${className || `
              ${open ? '' : 'absolute'}
              sm:absolute sm:w-96 sm:translate-y-2 z-10
              max-w-sm
              grid grid-flow-row gap-2
              p-4 rounded-2xl
              text-lg
              bg-gray-200 bg-opacity-90 dark:bg-gray-600 dark:bg-opacity-90
              transparency-reduce:bg-opacity-100
              backdrop-blur-lg transparency-reduce:backdrop-filter-none
              outline-none
            `}
            ${addClass || ''}
          `}>
            {
              items.map((item, index) => {

                if (item === 'divider') {
                  return (
                    <hr key={`menu-divider-${index}`} className='w-full border border-gray-300 dark:border-gray-500 rounded-full my-2' />
                  )
                }
                
                const { label, href } = item

                return (
                  <Menu.Item key={label}>
                    {({ active }) => (
                      <Link
                        onClick={onClick}
                        type='fill-lift'
                        href={href}
                        className={item.className}
                        addClass={`
                          ${item.addClass || ''}
                          ${active ? fillLiftClasses : ''}
                        `}
                      >{label}</Link>
                    )}
                  </Menu.Item>
                )
              })
            }
          </Menu.Items>

        </Transition>

      </div>
    )}
  </Menu>
}

export default DropDown