// * Types
import type { FunctionComponent } from 'react'
import type { Button as ButtonProps } from '@/typings/components'

// * Modules
import React from 'react'

// * Components
import Link from 'next/link'

/**
 * # Button Component
 * 
 * - A component for rendering links with various styles and options.
 * - Utilizes the Next.js `Link` component and provides additional functionality.
 * 
 * ## Styles
 * 
 * This component includes various classes to style the button. The styles are defined by the following props:
 * 
 * - padding
 * - rounded
 * - color
 * - inverse
 * - opacity
 * 
 * See **Props** section below for more info.
 * 
 * ## Props
 * 
 * - `padding` [padding='base'] - The size of the element based on padding.
 * - `rounded` [rounded='sm'] - The size of the border radius.
 * - `color` [color='primary'] - Sets the color classes to predefined values.
 * - `inverse` [inverse=false] - Inverts the normal and hover color classes of the predefined values.
 * - `opacity` [opacity='opaque'] - Whether the button should be transparent or opaque.
 * - `href` - The destination URL for the link.
 * - `target` - The target attribute for the link (local link default: `'_self'`; external link default: `'_blank'`).
 * - `rel` - The rel attribute for the link (local link default: `'follow'`; external link default: `'nofollow'`).
 * - `prefetch` - Whether the content of the destination URL should be prefetched (local link default: `true`; external link default: `false`).
 * - `download` - Redefines the name of the file when downloaded.
 * - `type` - Defines the type attribute of the button element (default: `'button'`).
 * - `onClick` - A function for handling onClick events.
 * - `className` - Replaces all default classes to apply to the button. (Recommended with styles from Link.)
 * - `addClass` - Additional classes to apply to the button.
 * - `icon` - Any `ReactElement`, but intended to be an imported SVG. Using this provides predefined classes; otherwise, use your element/SVG in the children
 * - `iconPosition` [iconPosition='right'] - Whether the icon should appear on the right or the left of the text
 * - `title` - The title attribute for the button.
 * - `children` - The content to display inside the button.
 * 
 * ## Examples
 * 
 * @example
 *   <Button href='/about' icon={ArrowRight} title='About Us'>Meet Our Team</Button>
 * 
 * @example
 *   <Button padding='sm' rounded='full' inverse={true} opacity='transparent'>Schedule a Call</Button>
 */
const Button: FunctionComponent<ButtonProps> = ({
  padding = 'base',
  rounded = 'lg',
  color = 'primary',
  inverse = false,
  opacity = 'opaque',
  href,
  target,
  rel,
  download,
  type,
  onClick,
  className,
  addClass,
  icon,
  iconPosition = 'right',
  title,
  children
}) => {
  
  let paddingClasses: string | undefined,
      backgroundClasses: string | undefined,
      textColorClasses: string | undefined,
      borderClasses: string | undefined,
      roundedClasses: string | undefined,
      iconColorClasses: string | undefined,
      shadowClasses: string | undefined,
      transitionClasses: string | undefined

  if (padding === 'xs') paddingClasses = 'px-2 py-0.5'
  if (padding === 'sm') paddingClasses = 'px-4 py-1'
  if (padding === 'base') paddingClasses = 'px-4 py-1 sm:px-12 sm:py-1.5'
  if (padding === 'md') paddingClasses = 'px-8 py-2'
  if (padding === 'lg') paddingClasses = 'px-12 py-3'

  if (rounded === 'none') roundedClasses = 'rounded-none'
  if (rounded === 'sm') roundedClasses = 'rounded-md'
  if (rounded === 'base') roundedClasses = 'rounded-lg'
  if (rounded === 'lg') roundedClasses = 'rounded-xl'
  if (rounded === 'xl') roundedClasses = 'rounded-3xl'
  if (rounded === 'full') roundedClasses = 'rounded-full'

  if (color === 'primary') {

    borderClasses = 'border-primary-400 contrast-more:border-primary-700'
    shadowClasses = 'shadow-glow-xl text-shadow-glow-xl'

    if (!inverse) {

      textColorClasses = 'text-white contrast-more:text-primary-900 dark:text-gray-900 hover:text-primary-400 contrast-more:hover:text-gray-900 contrast-more:dark:hover:text-primary-400'
      iconColorClasses = `
        fill-white contrast-more:fill-gray-900 dark:fill-gray-900 hover:fill-primary-400 contrast-more:hover:fill-primary-700
        stroke-white contrast-more:stroke-gray-900 dark:stroke-gray-900 hover:stroke-primary-400 contrast-more:hover:stroke-primary-700
      `
  
      if (opacity === 'opaque') backgroundClasses = 'bg-primary-400 hover:bg-white dark:hover:bg-gray-900'
      if (opacity === 'transparent') backgroundClasses = 'bg-primary-400 hover:bg-transparent'
    }
  
    if (inverse) {
  
      textColorClasses = 'text-primary-400 contrast-more:text-primary-700 hover:text-white contrast-more:text-gray-900 dark:hover:text-gray-900'
      iconColorClasses = `
        fill-primary-400 contrast-more:fill-primary-600 hover:fill-white dark:hover:fill-gray-900
        stroke-primary-400 contrast-more:stroke-primary-600 hover:stroke-white dark:hover:stroke-gray-900
      `
  
      if (opacity === 'opaque') backgroundClasses = 'bg-white dark:bg-gray-900 hover:bg-primary-400'
      if (opacity === 'transparent') backgroundClasses = 'bg-transparent hover:bg-primary-400'
    }
  }

  if (color === 'tune') {

    borderClasses = 'border-none'

    textColorClasses = 'text-white'

    backgroundClasses = 'bg-gradient-to-br from-tune-300 via-tune-600 to-tune-800 [background-size:200%] [background-position:left] hover:[background-position:right]'

    transitionClasses = 'transition-[background-position] duration-500'

  }

  if (color === 'white') {

    borderClasses = 'border-white'

    if (!inverse) {

      textColorClasses = 'text-black hover:text-white'
      iconColorClasses = `fill-black hover:fill-white stroke-black hover:stroke-black`
  
      if (opacity === 'opaque') backgroundClasses = 'bg-white hover:bg-black'
      if (opacity === 'transparent') backgroundClasses = 'bg-white hover:bg-transparent'
    }
  
    if (inverse) {
  
      textColorClasses = 'text-white hover:text-black'
      iconColorClasses = 'fill-white hover:fill-black stroke-white hover:stroke-black'
  
      if (opacity === 'opaque') backgroundClasses = 'bg-black hover:bg-white'
      if (opacity === 'transparent') backgroundClasses = 'bg-transparent hover:bg-white'
    }
  }

  if (color === 'black') {

    borderClasses = 'border-black'

    if (!inverse) {

      textColorClasses = 'text-white hover:text-black'
      iconColorClasses = 'fill-white hover:fill-black stroke-white hover:stroke-black'
  
      if (opacity === 'opaque') backgroundClasses = 'bg-black hover:bg-white'
      if (opacity === 'transparent') backgroundClasses = 'bg-black hover:bg-transparent'
    }
  
    if (inverse) {
  
      textColorClasses = 'text-white hover:text-black'
      iconColorClasses = 'fill-white hover:fill-black stroke-white hover:stroke-black'
  
      if (opacity === 'opaque') backgroundClasses = 'bg-black hover:bg-black-400'
      if (opacity === 'transparent') backgroundClasses = 'bg-transparent hover:bg-black-400'
    }
  }

  const buttonClasses = `
    ${paddingClasses || ''}
    ${backgroundClasses}
    ${textColorClasses}
    ${borderClasses} border-2
    ${roundedClasses}
    ${shadowClasses || ''}
    ${icon ? `${iconColorClasses} flex items-center gap-2` : ''}
    ${transitionClasses || 'transition-[background-color,color] duration-300'}
    font-bold whitespace-nowrap
  `

  const iconOnRight = iconPosition === 'right'

  const Icon = icon

  const isExternal = href?.startsWith('http')

  return href
  ?
  <Link
    href={href}
    download={download}
    target={target || isExternal ? '_blank' : '_self'}
    rel={
      rel !== undefined
      ? (
        rel === 'nofollow'
        ? `${rel} noreferrer noopener`
        : `${rel} prefetch`
      ) : (
        isExternal
        ? 'nofollow noreferrer noopener'
        : 'prefetch'
      )
    }
    className={`${className || buttonClasses} ${addClass || ''} cursor-pointer`}
    title={title}
    onClick={onClick}
  >
    { (Icon && !iconOnRight) ? <Icon className='w-4 h-4 stroke-[2]' /> : null }

    {children}

    { (Icon && iconOnRight) ? <Icon className='w-4 h-4 stroke-[2]' /> : null }
  </Link>
  :
  <button
    type={type || 'button'}
    onClick={onClick}
    className={`${className || buttonClasses} ${addClass || ''} cursor-pointer`}
    title={title}
  >
    { (Icon && !iconOnRight) ? <Icon className='w-4 h-4 stroke-[2]' /> : null }

    {children}

    { (Icon && iconOnRight) ? <Icon className='w-4 h-4 stroke-[2]' /> : null }
  </button>

}

export default Button