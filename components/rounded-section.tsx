// * Types
import type { FunctionComponent, RefObject } from 'react'
import type { RoundedSection as RoundedSectionProps } from '@/typings/templates'

// * Hooks
import { forwardRef } from 'react'

const RoundedSection: FunctionComponent<RoundedSectionProps> = forwardRef(({ exterior, interior, inView, children }, ref) => {
  return (
    <section
      ref={ref as RefObject<HTMLElement>}
      className={`
        ${exterior?.className || `
          my-12 ${inView ? '' : 'motion-safe:px-6'} motion-reduce:px-6
          transition-[padding-left,padding-right] duration-500
          max-w-[120rem] mx-auto
        `}
        ${exterior?.addClass || ''}
      `}
    >

      <div className={`
        ${interior?.className || `
          w-full min-h-[36rem] md:max-h-[36rem]
          flex flex-col md:flex-row items-center gap-12
          p-6 sm:p-12
          ${inView ? '' : 'motion-safe:rounded-5xl'} motion-reduce:rounded-5xl
          transition-[border-radius] duration-500
          overflow-hidden isolate
        `}
        ${interior?.addClass || ''}
      `}>

        { children }

      </div>

    </section>
  )
})

RoundedSection.displayName = 'RoundedSection'

export default RoundedSection