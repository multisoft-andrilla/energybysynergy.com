// * Types
import type { FunctionComponent } from 'react'
import type { Link as LinkProps } from '@/typings/components'

// * Components
import A from 'next/link'

// * Styles
const baseClasses = `
  after:content-[''] isolate
  after:absolute after:left-1/2 after:-z-10
  after:-translate-x-1/2
  after:duration-300
`

const lineClasses = `
  ${baseClasses}
  after:-bottom-0.5
  after:w-[calc(100%+0.15rem)]
  after:border after:border-current after:rounded-full
  after:transition-transform after:ease-in hover:after:ease-out
`

const scaleXClasses = `after:scale-x-0 hover:after:scale-x-100`
const scaleYClasses = `after:scale-y-0 hover:after:scale-y-100`

export const lineNoramlClasses = `
  ${lineClasses}
  ${scaleYClasses}
  after:translate-y-0.5 hover:after:translate-y-0 after:origin-bottom
`

export const lineStaticClasses = `${lineClasses}`
export const lineLtrClasses = `${lineClasses} ${scaleXClasses} after:origin-left`
export const lineRtlClasses = `${lineClasses} ${scaleXClasses} after:origin-right`
export const lineCenterClasses = `${lineClasses} ${scaleXClasses}`

export const lineLiftClasses = `
  ${lineClasses}
  ${scaleYClasses}
  after:origin-bottom after:translate-y-1 after:scale-x-75 hover:after:translate-y-0 hover:after:scale-x-100
`

const fillClasses = `
  ${baseClasses}
  after:top-1/2
  after:w-[calc(100%+0.25rem)] after:h-[calc(100%+0.05rem)]
  after:-translate-y-1/2
  after:rounded
  hover:text-white
  after:ease-in-out
  transition-[color] duration-300 ease-in-out
`

const fillColorTransitionClasses = `${fillClasses} after:transition-[transform_color] after:bg-primary-400`

export const fillCenterClasses = `
  ${fillClasses}
  after:scale-x-50 after:scale-y-[0.25] hover:after:scale-x-100 hover:after:scale-y-100
  hover:after:bg-primary-400
  after:transition-[transform_background-color]
`

export const fillLtrClasses = `${fillColorTransitionClasses} ${scaleXClasses} after:origin-left`
export const fillRtlClasses = `${fillColorTransitionClasses} ${scaleXClasses} after:origin-right`
export const fillLiftClasses = `${fillColorTransitionClasses} ${scaleYClasses} after:origin-bottom`

/**
 * # Link Component
 * 
 * - A component for rendering links with various styles and options.
 * - Utilizes the Next.js `Link` component and provides additional functionality.
 * 
 * ## Styles
 * 
 * This component includes various classes to style the link. The styles are divided into two types:
 * 
 * - Line styles: These styles add a line underneath the link, and include variations for different positions and orientations.
 * - Fill styles: These styles add a background color behind the link, and include variations for different positions and orientations.
 * 
 * ## Props
 * 
 * - `title` - The title attribute for the link.
 * - `type` - The style and animation variant for the link.
 * - `href` - The destination URL for the link.
 * - `target` - The target attribute for the link (local link default: `'_self'`; external link default: `'_blank'`).
 * - `rel` - The rel attribute for the link (local link default: `'follow'`; external link default: `'nofollow'`).
 * - `download` - Redefines the name of the file when downloaded
 * - `className` - Replaces all default classes to apply to the link.
 * - `addClass` - Additional classes to apply to the link.
 * - `children` - The content to display inside the link.
 * 
 * ## Examples
 * 
 * @example
 *   <Link href='/about' type='ltr' title='About Us'>Learn more about our company</Link>
 * 
 * @example
 *   <Link href='/about' type='fill-ltr' title='About Us'>Learn more about our company</Link>
 */
const Link: FunctionComponent<LinkProps> = ({
  title,
  type,
  href,
  target,
  rel,
  download,
  className,
  addClass,
  onClick,
  children
}) => {

  let linkClasses: string = lineNoramlClasses

  const isExternal = href.startsWith('http')

  if (type === 'static') linkClasses = lineStaticClasses
  if (type === 'ltr') linkClasses = lineLtrClasses
  if (type === 'rtl') linkClasses = lineRtlClasses
  if (type === 'center') linkClasses = lineCenterClasses
  if (type === 'lift') linkClasses = lineLiftClasses
  if (type === 'fill') linkClasses = fillCenterClasses
  if (type === 'fill-ltr') linkClasses = fillLtrClasses
  if (type === 'fill-rtl') linkClasses = fillRtlClasses
  if (type === 'fill-lift') linkClasses = fillLiftClasses

  return (
    <A
      title={title}
      href={href}
      target={target || (isExternal ? '_blank' : '_self')}
      download={download}
      rel={
        rel !== undefined
        ? (
          rel === 'nofollow'
          ? `${rel} noreferrer noopener`
          : `${rel} prefetch`
        ) : (
          isExternal
          ? 'nofollow noreferrer noopener'
          : 'prefetch'
        )
      }
      className={`${className || linkClasses} ${addClass || ''}`}
      onClick={onClick}
    >
      {children}
    </A>
  )
}

export default Link