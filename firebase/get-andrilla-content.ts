// * Types
import type { AndrillaContent } from '@/typings/firestore'

// Firebase
import { collection, doc, getDoc } from 'firebase/firestore'
import { siteID, firestore } from '@/lib/firebase'

/**
 * ### Get Andrilla Content From ID
 * 
 * Gets a `websites/{siteID}/andrillaContent/{uid}` document from the specified user ID.
 * 
 * @param {string} uid
 * @returns {Promise<AndrillaContent|undefined>} If successful returns `andrillaContent`, otherwise returns `undefined`
 */
export const getAndrillaContentFromId = async (uid: string): Promise<AndrillaContent | undefined> => {

  const siteAndrillaContentRef = collection(firestore, 'websites', siteID, 'andrillaContent'),
        query = doc(siteAndrillaContentRef, uid),
        andrillaContentDoc = await getDoc(query)
  
  if (andrillaContentDoc.exists()) {

    const data = andrillaContentDoc.data(),
          andrillaContentAsString = JSON.stringify(data),
          andrillaContent: AndrillaContent = JSON.parse(andrillaContentAsString)

    return andrillaContent

  } else {
    console.error('Failed to get doc', `websites/${siteID}/andrillaContent/${uid}`)
    return undefined
  }
}