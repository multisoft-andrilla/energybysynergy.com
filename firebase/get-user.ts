// * Types
import type { UserInfo } from '@/typings/firestore'

// Firebase
import { collection, doc, getDoc } from 'firebase/firestore'
import { firestore } from '@/lib/firebase'

/**
 * ### Get User From ID
 * 
 * Gets a `websites/{siteID}/andrillaContent/{uid}` document from the specific user ID.
 * 
 * @param {string} uid
 * @returns {Promise<UserInfo|undefined>} If successful returns `userInfo`, otherwise returns `undefined`
 */
 export const getUserFromId = async (uid: string): Promise<UserInfo | undefined> => {

  const siteAndrillaContentRef = collection(firestore, 'users'),
        query = doc(siteAndrillaContentRef, uid),
        userDoc = await getDoc(query)
  
  if (userDoc.exists()) {

    const data = userDoc.data(),
          userInfoAsString = JSON.stringify(data),
          userInfo: UserInfo = JSON.parse(userInfoAsString)

    return userInfo

  } else {
    console.error('Failed to get doc', `users/${uid}`)
    return undefined
  }
}