// * Types
import type { WebsiteInfo } from '@/typings/firestore'

// Firebase
import { collection, doc, getDoc } from 'firebase/firestore'
import { siteID, firestore } from '@/lib/firebase'

/**
 * ### Get Website From ID
 * 
 * Gets a `websites/{siteID}/forms/{formID}` document from the site's ID (specified in `@/lib/firebase`).
 * 
 * @returns {Promise<WebsiteInfo|undefined>} If successful returns `websiteInfo`, otherwise returns `undefined`
 */
 export const getWebsiteFromId = async (): Promise<WebsiteInfo | undefined> => {

  const siteRef = collection(firestore, 'websites'),
        query = doc(siteRef, siteID),
        siteDoc = await getDoc(query)
  
  if (siteDoc.exists()) {

    const data = siteDoc.data(),
          websiteInfoAsString = JSON.stringify(data),
          websiteInfo: WebsiteInfo = JSON.parse(websiteInfoAsString)

    return websiteInfo

  } else {
    console.error('Failed to get doc', `websites/${siteID}`)
    return undefined
  }
}