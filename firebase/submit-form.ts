// * Types
import type { AndrillaContent } from '@/typings/firestore'
import type { DocumentData } from 'firebase/firestore'

// Firebase
import { collection, addDoc, Timestamp } from 'firebase/firestore'
import { siteID, firestore } from '@/lib/firebase'
import { getFormFromId } from './get-form'
import { getUserFromId } from './get-user'
import { getWebsiteFromId } from './get-website'

// * SendGrid
import { andrillaTeamNotificationTemplateID } from '@/lib/sendgrid'

// * Functions
import { isDefined } from '@/utils/functions'
import { sendEmail } from '@/utils/send-email'

export const submitForm = async (
  andrillaContent: AndrillaContent,
  ownerID: string,
  formFields: {
    name: string,
    id: string,
    value: string
  }[],
  formID: string
) => {

  const submissionsCollection = collection(firestore, 'websites', siteID, 'submissions')

  const userDoc: DocumentData | undefined = await getUserFromId(ownerID),
        formDoc: DocumentData | undefined = await getFormFromId(formID),
        websiteDoc: DocumentData | undefined = await getWebsiteFromId()

  const ownerName: string = `${userDoc?.firstName} ${userDoc?.lastName}`,
        ownerEmail: string = andrillaContent.contactInfo.email,
        formName: string = formDoc?.name,
        url: string = websiteDoc?.url

  let emailData: any = {
    ownerName,
    ownerEmail,
    formName: formName,
    url: url,
    fields: formFields
  }

  let submissionData: any = {
    ownerID,
    formID,
    createdAt: Timestamp.fromDate(new Date())
  }

  formFields.forEach(field => {
    const { id, value } = field
    submissionData = { ...submissionData, [id]: value }
  })

  // Submit to Firestore
  try {
    addDoc(submissionsCollection, submissionData)
  } catch (error: any) {
    console.error('Error adding doc.', error)
    return Promise.reject()
  }

  // Send Notification to Owner
  sendEmail(emailData)
  
  // Andrilla Team Notificaiton
  sendEmail(emailData, andrillaTeamNotificationTemplateID)

  return Promise.resolve()
}