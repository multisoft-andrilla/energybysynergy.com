// * Types
import type { FormInfo } from '@/typings/firestore'

// Firebase
import { collection, doc, getDoc } from 'firebase/firestore'
import { siteID, firestore } from '@/lib/firebase'

/**
 * ### Get Form From ID
 * 
 * Gets a `websites/{siteID}/forms/{formID}` document from the specified form ID.
 * 
 * @param {string} formID
 * @returns {Promise<FormInfo|undefined>} If successful returns `formInfo`, otherwise returns `undefined`
 */
 export const getFormFromId = async (formID: string): Promise<FormInfo | undefined> => {

  const siteFormsRef = collection(firestore, 'websites', siteID, 'forms'),
        query = doc(siteFormsRef, formID),
        formDoc = await getDoc(query)
  
  if (formDoc.exists()) {

    const data = formDoc.data(),
          formInfoAsString = JSON.stringify(data),
          formInfo: FormInfo = JSON.parse(formInfoAsString)

    return formInfo

  } else {
    console.error('Failed to get doc', `websites/${siteID}/forms/${formID}`)
    return undefined
  }
}