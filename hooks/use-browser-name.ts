// * Types
import { BrowserNames } from '@/typings/hooks'

// * Hooks
import { useState, useEffect } from 'react'

/**
 * ### Use Browser Name
 * 
 * Returns the name of the browser
 * 
 * @returns {BrowserNames | unknown} browser name
 */
export const useBrowserName = (): BrowserNames | 'unknown' => {

  const [ browserName, setBrowserName ] = useState<BrowserNames|'unknown'>('unknown')

  useEffect(() => {

    const userAgent = window.navigator.userAgent

    let name: BrowserNames | 'unknown' = browserName

    if (userAgent.indexOf('Chrome') !== -1) {

      name = 'Chrome'

    } else if (userAgent.indexOf('Safari') !== -1) {

      name = 'Safari'

    } else if (userAgent.indexOf('Firefox') !== -1) {

      name = 'Firefox'

    } else if (userAgent.indexOf('MSIE') !== -1) {

      name = 'Internet Explorer'

    } else if (userAgent.indexOf('Edge') !== -1) {

      name = 'Microsoft Edge'

    } else if (userAgent.indexOf('Opera') !== -1 || userAgent.indexOf('OPR') !== -1) {

      name = 'Opera'

    }

    setBrowserName(name)

  }, [browserName, setBrowserName])

  return browserName
}
