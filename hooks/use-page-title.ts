// * Hooks
import { useEffect, useState } from 'react'

// * Functions
import { isDefined } from '@/utils/functions'

/**
 * ### Use Page Title
 * 
 * A custom hook that updates the page title, taking into account the business name and whether the page is being rendered for a crawler. This hook sets the title in the `useEffect` hook based on the provided `initialTitle` prop.
 * 
 * @param {string|undefined} initialTitle The initial title for the page (use meta.title for simplicity. default: business name).
 * @param {string|undefined} updatedTitle The title to update for non-crawlers.
 * @returns {string|undefined} A tuple containing the current page title and a function to update the page title
 */
export const usePageTitle = (
  initialTitle: string | undefined,
  updatedTitle: string,
  businessName: string
): string | undefined => {

  const [title, setTitle] = useState(initialTitle || businessName)

  useEffect(() => {
    const fetchCrawlerStatus = async () => {
      try {
        
        const res = await fetch('/api/is-crawler'),
              data = await res.json(),
              isCrawler = data 

        if (!isCrawler) setTitle(isDefined(updatedTitle) ? `${updatedTitle} | ${businessName}` : businessName)
        
      } catch (error) {
        console.error(error)
      }
    } 

    fetchCrawlerStatus() 
  }, [initialTitle, setTitle, updatedTitle, businessName])

  return title
}