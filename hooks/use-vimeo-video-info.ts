// * Types
import type { Dispatch, SetStateAction } from 'react'
import type { VimeoVideoInfo } from '@/typings/components'

// * Hooks
import { useState, useEffect } from 'react'

/**
 * ### Use Vimeo Video Info
 * 
 * A custom hook that fetches and returns Vimeo video information.
 * 
 * @param {string} src The source URL of the Vimeo video.
 * @param {string|undefined} id The unique identifier of the <iframe> for autoplay.
 * @param {boolean|undefined} autoplay Whether the video should autoplay.
 * @param {Dispatch<SetStateAction<boolean>>} setIsFacade A setter function for a boolean state that controls whether a facade is displayed.
 * @param {(id: string, setIsFacade: Dispatch<SetStateAction<boolean>>) => Promise<void>} playVideo A function that plays the Vimeo video.
 * @returns {VimeoVideoInfo} The video information, including title and thumbnail.
 */
export const useVimeoVideoInfo = (
  src: string,
  id: string | undefined,
  autoplay: boolean | undefined,
  facadeState: [boolean, Dispatch<SetStateAction<boolean>>],
  playVideo: (id: string, facadeState: [boolean, Dispatch<SetStateAction<boolean>>], loop?: boolean) => void,
  loop: boolean | undefined
): VimeoVideoInfo => {

  const isVimeoVideo = src.includes('vimeo')

  const [videoInfo, setVideoInfo] = useState<VimeoVideoInfo>({
          title: undefined,
          thumbnail: undefined
        })

  useEffect(() => {
    if (isVimeoVideo) fetch(`https://vimeo.com/api/oembed.json?url=${src}`, { mode: 'cors' })
      .then((res) => res.json())
      .then((data) => {
        setVideoInfo(() => {
          return {
            title: data.title,
            thumbnail: data.thumbnail_url ? data.thumbnail_url.split('_')[0] + '_1920x1080' : undefined,
          }
        })
        if (autoplay && id) playVideo(id, facadeState, loop || false)
      })
      .catch((error) => {
        setVideoInfo(() => {
          return {
            title: 'Error',
            thumbnail: undefined
          }
        })
      })
  }, [src, id, isVimeoVideo, autoplay, facadeState, loop, playVideo])

  return videoInfo
}