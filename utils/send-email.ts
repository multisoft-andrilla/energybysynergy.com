// * Variables
import { formSubmissionNotificationID, andrillaTeamNotificationTemplateID } from '../lib/sendgrid'

/**
 * ### Send Email
 * - Prepares emails for sending, and send via SendGrid api
 * @param {any} data Any object
 * @param {string} templateID If left undefined, a basic templateID is applied
 */
export const sendEmail = async (data: any, templateID: string = formSubmissionNotificationID) => {
  
  // Default to Send to Owner
  let body = {
    data,
    templateID: templateID,
    to: {
      email: data.ownerEmail,
      name: data.ownerName
    }
  }

  // Email Data for Submitter
  if (!templateID) {
    data.fields.forEach((field: {name: string, value: string}) => {
      if (field.name === 'name') body.to.name = field.value
      if (field.name === 'firstName') body.to.name = field.value
      if (field.name === 'lastName') body.to.name = `${body.to.name} ${field.value}`
      if (field.name === 'email') body.to.email = field.value
    })
  }

  // Email Data for Andrilla Team
  if (templateID === andrillaTeamNotificationTemplateID) {
    body.to.name = 'Andrilla Team'
    body.to.email = 'info@andrilla.net'
  }

  // Send Email via Sendgrid
  await fetch('../api/sendgrid', {
    method: 'POST',
    body: JSON.stringify(body)
  }).catch((error: any) => console.error('Failed to send email', error))
}