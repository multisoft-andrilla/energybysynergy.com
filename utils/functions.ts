// * Types
import type { RefObject } from 'react'
import type { Rgb as RgbString } from '@/typings/main'

// * Modules
import { jsPDF } from 'jspdf'

// * Regex
import { rgbRegex } from '../utils/regex'

/**
 * ### Has Class
 * - Checks whether a reference has a class
 * @param {MutableRefObject<HTMLElement>} ref 
 * @param {string} className 
 * @returns {boolean} boolean
 */
export const hasClass = (ref: RefObject<HTMLElement>, className: string): boolean | void => {

  const element = ref.current,
        elementsType: string = Object.prototype.toString.call(element).replace(']', '').split(' ')[1],
        classType: string = Object.prototype.toString.call(className).replace(']', '').split(' ')[1]

  // Element must be defined
  if (element) {

    // Error: Element is not an HTML Element
    if (elementsType.startsWith('HTML') !== true) {
      console.error(`hasClass(): Failed to get element`, `${elementsType}s are not accepted. Only one element is allowed.`)
      return false
    }

    // Error: className is not a string
    if (classType !== 'String') {
      console.error(`hasClass(): Failed to get classes`, `${classType}s are not accepted. Only on class at a time.`)
      return false
    }

    return element.classList.contains(className)
  }

  // Element is not defined
  return false
}

/**
 * ### Is Defined
 * - Checks whether a value is defined
 * - Tests against 'undefined' && '' && null && undefined
 * @param {any} value 
 * @returns {boolean} boolean
 */
export const isDefined = (value: any) => {
  // If is defined
  if (value !== 'undefined' && value !== '' && value !== null && value !== undefined) return true
  // Not defined
  return false
}

/**
 * ### To Camel Case
 * - Converts and string to camel case based on spaces
 * - Eliminates no letter-number characters
 * @param {string} string
 * @returns {string} string
 */
export const toCamelCase = (string: string) => {
  return string
    .replace(/[^a-z0-9]/gi, ' ')
    .toLowerCase()
    .split(' ')
    .map((str: string, index: number) => index === 0 ? str : str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase())
    .join('')
}

/**
 * ### RGB to HEX
 * - Converts rgb(x, y, z) to #XXYYZZ
 * @param {RgbString} rgb
 * @returns {string} #XXYYZZ
 */
export const rgbToHex = (rgb: RgbString): string => {

  const match = rgb.match(rgbRegex)

  if (match) {

    const [, r, g, b] = match.map(Number)

    return '#' + [r, g, b].map(value => {
      const hex = value.toString(16)
      return hex.length === 1 ? '0' + hex : hex
    }).join('')

  }

  return 'undefined'
}

/**
 * ### Generate PDF
 * - This function generates a PDF document from an HTML element.
 * @param {HTMLElement} element - The HTML element to be converted to a PDF document.
 * @param {string} name - The name to be used for the generated PDF document.
 */
export const generatePdf = (element: HTMLElement, name: string, url: string) => {

  const links = element.querySelectorAll('a')

  links.forEach((link: HTMLAnchorElement) => {
    if (link.href.startsWith('/')) link.href = `https://${url}${link.href}`
  })

  const doc = new jsPDF()

  doc.html(element, {
    callback: (doc) => {
      doc.save(`${name}.pdf`)
    },
    x: 0,
    y: 0,
    autoPaging: 'text',
    html2canvas: {
      scale: 0.2
    },
    margin: 12
  })

}

/**
 * ### Get Random Number
 * - Returns a random number between the specified values or 1 and 100
 * @param {number} min The minimum value
 * @param {number} max The maximum value
 * @returns {number} A random number between the specified values or 1 and 100
 */
export const getRandomNumber = (min: number = 0, max: number = 100): number => {
  return Math.floor(Math.random() * (max - min + 1) + min)
}