// * Types
import { Email, Tel, Rgb, Min50Max160, Max50 } from '@/typings/main'

export const rgbRegex = /rgb\((\d+), (\d+), (\d+)\)/

/**
 * ### Is RGB
 * - Checks whether the specified string is in RGB format
 * @param {string} rgb
 * @returns {boolean} boolean
 */
export const isRgb = (rgb: string): rgb is Rgb => {
  return rgbRegex.test(rgb)
}

export const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

/**
 * ### Is Email
 * - Checks whether the specified string is in email format
 * @param {string} email
 * @returns {boolean} boolean
 */
export const isEmail = (email: string): email is Email => {
  return emailRegex.test(email)
}

export const telRegex = /(?:\+1\s|1\s|)?\d{3}\.\d{3}\.\d{4}|(?:\+1\s|1\s|)?\d{3}-\d{3}-\d{4}|(?:\+1\s|1\s|)?\(\d{3}\) \d{3}-\d{4}|(?:\+1\s|1\s|\+1|1|)?\d{10}/

/**
 * ### Is Phone Number
 * - Checks whether the specified string is in phone number format
 * @param {string} tel
 * @returns {boolean} boolean
 */
export const isPhoneNumber = (tel: string): tel is Tel => {
  return telRegex.test(tel)
}

export const min50Max160Regex = /(.{50,160})/

/**
 * ### Min 50 Max 160
 * - Checks whether the specified string is minimum 50 characters and maximum 160 characters
 * @param {string} str
 * @return {boolean} boolean
 */
export const isMin50Max160 = (str: string): str is Min50Max160 => {
  return min50Max160Regex.test(str)
}

export const max50Regex = /(.{0,50})/

/**
 * ### Max 50
 * - Checks whether the specified string is maximum 50 characters
 * @param {string} str
 * @return {boolean} boolean
 */
export const isMax50 = (str: string): str is Max50 => {
  return max50Regex.test(str)
}