import { isDefined } from './functions'

/**
 * ### Has Class
 * - Returns a boolean based on whether the specified element has the specified class
 * @param {HTMLElement} element Any HTML Element
 * @param {string} className A string of any class to check for
 * @returns {boolean} true if the specified element has the specified class, else false
 */
export const hasClass = (element: HTMLElement, className: string): boolean => {
  return element.classList.contains(className)
}

/**
 * ### Add Class
 * - Adds the specified classes to the specified elements
 * @param {Element|HTMLElement|HTMLElement[]|NodeList|string|undefined} elements An HTML Element, an array of HTML Elements, a Node List, a string (as a selector for a querySelector)
 * @param {string|string[]} classes A string or an array of classes to add to each element
 */
export const addClass = (elements: Element | HTMLElement | HTMLElement[] | NodeList | string, classes: string | string[]) => {

  const elementsType = elements.constructor.name,
        classesType = classes.constructor.name

  let elementList: HTMLElement[] | undefined,
      classesList: string[] | undefined

  // * Convert elements to array
  // @ts-ignore elementsType varfies type
  if (elementsType === 'String') elementList = Array.from(document.querySelectorAll(elements)) // Selector
  // @ts-ignore elementsType varfies type
  if (elementsType.startsWith('HTML')) elementList = [elements] // One HTML Element
  // @ts-ignore elementsType varfies type
  if (elementsType === 'NodeList') elementList = Array.from(elements) // Multiple HTML Elements
  // @ts-ignore elementsType varfies type
  if (elementsType === 'Array') elementList = elements // Array of Elements

  // * Convert classes to array
  // @ts-ignore classesType varfies type
  if (classesType === 'String' && classes.split(' ')) classesList = classes.split(' ')
  // @ts-ignore classesType varfies type
  if (classesType === 'Array') classesList = classes

  if (elementList && classesList) elementList.forEach((element: HTMLElement) =>
    classesList!.forEach((classItem: string) => {
      if (hasClass(element, classItem)) return
      element.classList.add(classItem)
    })
  )
}

/**
 * ### Remove Class
 * - Removes the specified classes from the specified elements
 * @param {HTMLElement|HTMLElement[]|NodeList|string|undefined} elements An HTML Element, an array of HTML Elements, a Node List, a string (as a selector for a querySelector)
 * @param {string|string[]} classes A string or an array of classes to remove from each element
 */
export const removeClass = (elements: HTMLElement | HTMLElement[] | NodeList | string, classes: string | string[]) => {

  const elementsType = elements.constructor.name,
        classesType = classes.constructor.name

  let elementList: HTMLElement[] | undefined,
      classesList: string[]

  // * Convert elements to array
  // @ts-ignore elementsType varfies type
  if (elementsType === 'String') elementList = Array.from(document.querySelectorAll(elements)) // Selector
  // @ts-ignore elementsType varfies type
  if (elementsType.startsWith('HTML')) elementList = [elements] // One HTML Element
  // @ts-ignore elementsType varfies type
  if (elementsType === 'NodeList') elementList = Array.from(elements) // Multiple HTML Elements
  // @ts-ignore elementsType varfies type
  if (elementsType === 'Array') elementList = elements // Array of Elements

  // * Convert classes to array
  // @ts-ignore classesType varfies type
  if (classesType === 'String' && classes.split(' ')) classesList = classes.split(' ')
  // @ts-ignore classesType varfies type
  if (classesType === 'Array') classesList = classes

  if (elementList) elementList.forEach((element: HTMLElement) => 
    classesList.forEach((classItem: string) => {
      if (!hasClass(element, classItem)) return
      element.classList.remove(classItem)
    })
  )
}

/**
 * ### Toggle Class
 * - Toggles the specified classes on the specified elements
 * @param {HTMLElement|HTMLElement[]|NodeList|string|undefined} elements An HTML Element, an array of HTML Elements, a Node List, a string (as a selector for a querySelector)
 * @param {string|string[]} classes A string or an array of classes to toggle on each element
 */
export const toggleClass = (elements: HTMLElement | HTMLElement[] | NodeList | string, classes: string | string[]) => {

  const elementsType = elements.constructor.name,
        classesType = classes.constructor.name

  let elementList: HTMLElement[] | undefined,
      classesList: string[]

  // * Convert elements to array
  // @ts-ignore elementsType varfies type
  if (elementsType === 'String') elementList = Array.from(document.querySelectorAll(elements)) // Selector
  // @ts-ignore elementsType varfies type
  if (elementsType.startsWith('HTML')) elementList = [elements] // One HTML Element
  // @ts-ignore elementsType varfies type
  if (elementsType === 'NodeList') elementList = Array.from(elements) // Multiple HTML Elements
  // @ts-ignore elementsType varfies type
  if (elementsType === 'Array') elementList = elements // Array of Elements

  // * Convert classes to array
  // @ts-ignore classesType varfies type
  if (classesType === 'String' && classes.split(' ')) classesList = classes.split(' ')
  // @ts-ignore classesType varfies type
  if (classesType === 'Array') classesList = classes

  if (elementList) elementList.forEach((element: HTMLElement) => 
    classesList.forEach((classItem: string) => {
      element.classList.toggle(classItem)
    })
  )
}

/**
 * ### Get Pseudo Class List
 * - Returns the class list of a pseudo element for the specified element
 * @param {HTMLElement} element Any HTML Element
 * @param {'before'|'after'} pseudo The pseudo element to target (default: 'before')
 * @returns {string} The class list of the specified psuedo element
 */
export const getPseudoClassList = (element: HTMLElement, pseudo: 'before' | 'after' = 'before'): any => {

  const computedStyle = getComputedStyle(element, `::${pseudo}`),
        content = computedStyle.content

  if (!isDefined(content)) return ''

  const classList = content.replace(/["']/g, '')

  return computedStyle
}