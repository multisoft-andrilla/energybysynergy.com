/** The current date as a Date object */
const d = new Date()

/** The current millisecond of the current second */
export const milliseconds = d.getMilliseconds()

/** The current second of the current minute */
export const seconds = d.getSeconds()

/** The current minute of the current hour */
export const minutes = d.getMinutes()

/** The current hour of the current day */
export const hours = d.getHours()

/** The current day of the week as a number */
export const weekday = d.getDay()

/** The current day of the current month */
export const day = d.getDate()

/** The current month as a number */
export const month = d.getMonth()

/** The current year */
export const year = d.getFullYear()

/** An array of the names of month in order */
export const monthNamesList = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]

/**
 * ### Get Month Name
 * - Returns the name of the specified month
 * @param {Date} date A Date object representing the month to get the name of the month from. (Preset to the current date)
 * @returns {string} The name of the specified month
 */
export const getMonthName = (date: Date = d): string => {
  // Return the name of the month
  return monthNamesList[date.getMonth()]
}

/** The name of the current month */
export const currentMonthName = getMonthName()

/** An array of the names of the days of the week in order */
export const weekdayNamesList = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ]

/**
 * ### Get Weekday Name
 * - Returns the weekday name of the specified day
 * @param {Date} date A Date object representing the day to get the weekday name from. (Preset to the current date)
 * @returns {string} The name of the specified weekday
 */
export const getWeekdayName = (date: Date = d): string => {
  // Return the name of the day of the week
  return weekdayNamesList[date.getDay()]
}

/** The name of the current day of the week */
export const currentWeekdayName = getWeekdayName()

/**
 * ### Days In Month
 * - Returns the number of days in the specified month.
 * @param {Date} date A Date object representing the month to get the number of days for. (Preset to the current date)
 * @returns {number} The number of days in the specified month.
 */
export const daysInMonth = (date: Date = d): number => {
  const selectedYear = date.getFullYear(),
        selectedMonth = date.getMonth() + 1
  return new Date(selectedYear, selectedMonth, 0).getDate()
}

/**
 * ### First of Month
 * - Returns the first day of the specified month.
 * @param {Date} date A Date object representing the month to get the first day for. (Preset to current date)
 * @returns {Date} A Date object of the given month, with the first day.
 */
export const firstOfMonth = (date: Date = d): Date => {
  // Return a new Date object with the first of the month selected
  return new Date(date.getFullYear(), date.getMonth(), 1)
}

/**
 * ### Get Previous Month
 * - Returns the number of the previous month from the specified month
 * @param {Date} date The Date object representing the month to get the previous month from (Preset to current date)
 * @returns {number} The indexed month of the previous month
 */
export const getPreviousMonth = (date: Date = d): number => {
  const givenMonth = date.getMonth() // Get the month from date
  // Return the indexed month. min 0, max 11
  return givenMonth === 0 ? 11 : givenMonth - 1
}

/**
 * ### Get Next Month
 * - Returns the number of the following month from the specified month
 * @param {Date} date The Date object representing the month to get the following month from (Preset to current date)
 * @returns {number} The indexed month of the following month
 */
export const getNextMonth = (date: Date = d): number => {
  const givenMonth = date.getMonth() // Get the month from date
  // Return the indexed month. min 0, max 11
  return givenMonth === 11 ? 0 : givenMonth + 1
}

/**
 * ### Get Hours in 12
 * - Returns the hour based on the specified 24 hour value in a 12 hour format
 * @param {number} hour The hour to be converted to 12 hour format
 * @returns {number} The hour in a 12 hour format
 */
export const getHoursIn12 = (hour: number): number => {
  if (hour > 12) return hour - 12
  return hour
}

/**
 * ### Get Meridian from Hour
 * - Returns either 'pm' or 'am' based on the specified 24 hour value
 * @param {number} hour The hour to get the meridian from
 * @returns {'am'|'pm'} The meridian for the given hour
 */
export const getMeridianFromHour = (hour: number): 'am' | 'pm' => {
  if (hour >= 12) return 'pm'
  return 'am'
}