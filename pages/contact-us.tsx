// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'
import type { FunctionComponent } from 'react'

type ContactItemProps = {
  Icon: any
  type: 'email' | 'tel' | 'address'
  label: string | string[]
}

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import Link from 'next/link'
import Envelope from '@/svgs/icons/envelope.svg'
import Phone from '@/svgs/icons/phone.svg'
import MapPinEllipse from '@/svgs/icons/map-pin-ellipse.svg'
import Details from '@/components/details'

// * Partials
import EmailForm from '@/partials/forms/email-form'

// * Templates
import ContactFooter from '@/templates/contact-footer'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Styles
import { lineLtrClasses } from '@/components/link'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'Contact Us',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Contact and FAQ',
  description: 'Have general questions about our energy-saving products and services? Get in touch with Energy by Synergy for an energy audit of your home or business, financing options, and sustainable energy solutions. Check our FAQs first for answers to common questions.',
  thumbnail: {
    src: 'contact-us-thumbnail.webp',
    alt: 'Preview of Contact Us page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const ContactItem: FunctionComponent<ContactItemProps> = ({ Icon, type, label }) => {

  const StyledIcon = () => <Icon className='
    bg-gray-700
    w-10 h-10
    rounded-xl
    fill-white
    px-2
    flex-shrink-0
  '/>

  const containerClasses = `
    w-72
    flex items-center gap-3
    text-sm
  `

  if (type === 'email' && typeof(label) === 'string') {
    return (
      <Link href={`mailto:${label}`} className={containerClasses}>
        <StyledIcon />
        <span className={lineLtrClasses}>{label}</span>
      </Link>
    )
  }

  if (type === 'tel' && typeof(label) === 'string') {
    return (
      <Link href={`tel:1${label.replace(/\D/g, '')}`} className={containerClasses}>
        <StyledIcon />
        <span className={lineLtrClasses}>{label}</span>
      </Link>
    )
  }

  if (type === 'address') {
    return (
      <div className={containerClasses}>
        <StyledIcon />
        <address>{
          (typeof(label) === 'string')
          ?
          label
          :
          label.map((line, index) => {

            const isLastLine = label.length === (index + 1)

            return isLastLine
            ?
            line
            :
            <>
              {line}
              <br />
            </>
          })
        }</address>
      </div>
    )
  }

  return null
}

const ContactUs: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <main>

        <section className='px-6 pt-12 sm:pt-36 sm:pb-24 max-w-7xl mx-auto grid grid-flow-row place-items-center md:grid-cols-2 gap-12'>

          <div>

            <h2 className='font-display text-3xl sm:text-4xl text-gray-700 dark:text-gray-200 font-bold'>Contact Us</h2>
            <br />

            <p className='text-gray-500 dark:text-gray-400 text-xl'>We're here to help you with any general questions you may have about our products or services. Be sure to check our FAQ below, first. Your question may have already been answered.</p>

            <div className='grid grid-flow-row xl:grid-cols-2 gap-6 mt-12'>

              <ContactItem
                Icon={Envelope}
                type='email'
                label='support@energybysynergy.com'
              />

              <ContactItem
                Icon={Phone}
                type='tel'
                label='(844) 527-7377'
              />

              <ContactItem
                Icon={MapPinEllipse}
                type='address'
                label={[
                  '3950 RCA Blvd. Suite 5003',
                  'Palm Beach Gardens, FL 33410'
                ]}
              />

            </div>

          </div>

          <EmailForm ownerID={ownerID} andrillaContent={andrillaContent} />

        </section>

        <section className='py-24 px-4 sm:px-6 max-w-6xl mx-auto'>

          <div  className='bg-gray-900 px-6 sm:px-8 py-8 rounded-4xl lg:flex gap-8'>

            <div className='mb-8 lg:mb-0 pb-8 lg:pb-0 lg:pr-8 border-b border-b-gray-400 lg:border-b-0 lg:border-r lg:border-r-gray-400'>

              <h2 className='text-gray-50 font-black text-3xl sm:text-4xl font-display lg:w-min'>Frequently <span className='lg:whitespace-nowrap'>Asked Questions</span></h2>

            </div>

            <div className='grid grid-flow-row gap-4'>

              <Details summary='Can Synergy help me reduce my energy bills?'>
                Absolutely. Our team of experts will conduct an energy audit of your home or business to identify areas where energy can be saved and develop a plan to reduce your energy bills.
              </Details>

              <Details summary='What is an energy audit?'>
                An energy audit is an assessment of your energy usage, identifying areas where energy can be saved and developing a customized plan to reduce your energy bills.
              </Details>

              <Details summary='Are there any tax benefits that your services help with?'>
                Yes, by installing a solar energy system, you may qualify for the Investment Tax Credit (ITC), which currently provides a 30% federal tax credit for eligible homeowners and businesses. This credit can significantly reduce the overall cost of your solar installation and is a great incentive to switch to sustainable energy.
              </Details>

              <Details summary='Does Synergy offer financing options?'>
                Yes, we offer financing options to help you implement energy-saving solutions that fit within your budget.
              </Details>

              <Details summary='How long does it take to implement energy-saving solutions?'>
                The timeline for implementing energy-saving solutions varies depending on the scope of the project, but we work quickly and efficiently to minimize disruption to your home or business.
              </Details>

              <Details summary='Can Synergy help me find sustainable energy solutions?'>
                Yes, we specialize in developing sustainable energy solutions that reduce your environmental impact while saving you  money on energy bills.
              </Details>
              
            </div>

          </div>

        </section>

        <ContactFooter
          intro='Ready to get started?'
          heading='Sign up for a Free Consultation'
          andrillaContent={andrillaContent}
        />

      </main>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default ContactUs
