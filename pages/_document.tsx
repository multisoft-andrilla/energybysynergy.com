import Document, { DocumentContext, DocumentInitialProps, Html, Head, Main, NextScript } from 'next/document'

class MainDocument extends Document {
  static async getInitialProps(
    context: DocumentContext
  ): Promise<DocumentInitialProps> {
    const initialProps = await Document.getInitialProps(context)

    return initialProps
  }

  render() {
    return (
      <Html lang='en-US' prefix='og: https://ogp.me/ns#'>
        <Head>
          {/* todo <meta name='google-site-verification' content='REPLACE_ME' /> */}
          <meta property='og:locale' content='en-US' />
          <meta property='og:site_name' content='Synergy Energy Solutions' />
          <meta name='copyright' content='Andrilla LLC' />
          <link rel='shortcut icon' href='/favicons/favicon.ico' />
          <link rel='icon' type='image/x-icon' sizes='16x16 32x32' href='/favicons/favicon.ico' />
          <link rel='apple-touch-icon' sizes='152x152' href='/favicons/favicon-152-precomposed.png' />
          <link rel='apple-touch-icon' sizes='144x144' href='/favicons/favicon-144-precomposed.png' />
          <link rel='apple-touch-icon' sizes='120x120' href='/favicons/favicon-120-precomposed.png' />
          <link rel='apple-touch-icon' sizes='114x114' href='/favicons/favicon-114-precomposed.png' />
          <link rel='apple-touch-icon' sizes='180x180' href='/favicons/favicon-180-precomposed.png' />
          <link rel='apple-touch-icon' sizes='72x72' href='/favicons/favicon-72-precomposed.png' />
          <link rel='apple-touch-icon' sizes='57x57' href='/favicons/favicon-57.png' />
          <link rel='icon' sizes='32x32' href='/favicons/favicon-32.png' />
          <meta name='msapplication-TileImage' content='/favicons/favicon-144.png' />
          <link rel='manifest' href='/favicons/manifest.json' />
          <link rel='icon' sizes='192x192' href='/favicons/favicon-192.png' />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MainDocument