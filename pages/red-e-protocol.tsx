// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import { InView } from 'react-intersection-observer'
import Card from '@/components/card'
import Img from 'next/image'
import Button from '@/components/button'
import Poweroutlet from '@/svgs/icons/poweroutlet.svg'
import BoltHorizontal from '@/svgs/icons/bolt-horizontal.svg'
import Sun from '@/svgs/icons/sun.svg'
import PersonFillQuestionmark from '@/svgs/icons/person-fill-questionmark.svg'

// * Templates
import RoundedSection from '@/components/rounded-section'
import ContactFooter from '@/templates/contact-footer'

// * Functions
import { isMin50Max160 } from '@/utils/regex'
import InfoGrid from '@/templates/info-grid'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'RED-E Protocol',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Sustainable Energy Solutions with RED-E',
  description: 'Our RED-E protocol provides sustainable energy solutions to our clients, reducing energy consumption and benefiting the environment. With RED-E, you can save money on energy costs while also reducing your reliance on traditional energy sources. Get a free assessment today.',
  thumbnail: {
    src: 'red-e-protocol-thumbnail.webp',
    alt: 'Preview of RED-E Protocol page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const RedEProtocol: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <ParallaxProvider>

        <main>

          <InView threshold={0.75} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection
                ref={ref}
                inView={inView}
                interior={{
                  className: `
                    w-full min-h-[36rem] md:max-h-[36rem]
                    grid grid-flow-row md:grid-cols-2 place-items-center gap-12
                    p-6 sm:p-12
                    ${inView ? '' : 'rounded-5xl'} motion-reduce:rounded-5xl
                    transition-[border-radius] duration-500
                    overflow-hidden isolate
                    bg-gradient-to-bl from-gray-50 to-gray-200 dark:from-gray-800 dark:to-gray-900
                  `
                }}>
    
                <Card
                  heading='Revolutionizing Energy Consumption'
                  body='We implement the RED-E protocol in all of our services, allowing us to provide sustainable energy solutions to our clients. RED-E is based on intelligent and responsible designs of electrical, mechanical, and plumbing systems, reducing energy consumption and benefiting the environment.'
                  addClass='h-min'
                />

                <Parallax speed={-2} className='contens md:motion-safe:block'>

                  <Img
                    src='/images/red-e.webp'
                    alt='RED-E'
                    width={1122}
                    height={508}
                    className='object-contain'
                    sizes='
                      100vw,
                      (min-width: 768px) 50vw
                    '
                    priority
                    loading='eager'
                  />

                </Parallax>
    
              </RoundedSection>
            )}
          </InView>

          <section className='px-6 py-8 pb-48 max-w-6xl mx-auto'>

            <div className='grid grid-flow-row md:grid-cols-2 place-items-center'>

              <div className='mb-12'>

                <h2 className='font-display'>
                  <span className='text-primary-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>Why use RED-E?</span>
                  <br />
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>A Win-Win Decision</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>We believe that using RED-E is not just about protecting the environment, but also about making smart business decisions. With RED-E, you can save money on energy costs while also reducing your reliance on traditional energy sources.</p>
                <br />

                <Button href='#contact' padding='lg'>Get My Assessment</Button>

              </div>

              <div className='flex items-end'>

                <Img
                  src='/images/beach.webp'
                  alt='Beautiful birds-eye view of a beach with blue water and a green forest'
                  width={1000}
                  height={749}
                  className='
                    w-full max-w-[calc(100%-4rem)] md:max-w-md rounded-t-[11.5rem] h-64 md:h-96
                    object-cover mx-auto
                  '
                />

              </div>

            </div>

            <InfoGrid
              items={[
                {
                  Icon: Poweroutlet,
                  heading: 'What is it?',
                  body: 'RED-E is a sustainable engineering application designed to reduce energy consumption and improve the efficiency of electrical, mechanical, and plumbing systems.'
                },
                {
                  Icon: BoltHorizontal,
                  heading: 'Why RED-E?',
                  body: 'We use RED-E because it leads to sustainable and cost-effective solutions for our clients.'
                },
                {
                  Icon: Sun,
                  heading: 'When is RED-E considered?',
                  body: 'We consider RED-E for anyone looking to improve energy efficiency and save money in the long run.'
                },
                {
                  Icon: PersonFillQuestionmark,
                  heading: 'Want to learn more about RED-E?',
                  button: {
                    href: '#contact',
                    children: 'Contact Us'
                  }
                }
              ]}
              options={{
                heading: {
                  addClass: 'max-w-xs'
                },
                body: {
                  addClass: 'max-w-sm'
                }
              }}
              className='
                p-4 lg:p-6 rounded-4xl
                bg-gray-800 dark:bg-gray-900
                shadow-[1.5rem_2rem_0] shadow-gray-200 dark:shadow-gray-700
                gap-8 sm:gap-10
              '
            />

          </section>

          <ContactFooter
            intro='Ready to meet the team?'
            heading='Sign up for a Free Consultation'
            andrillaContent={andrillaContent}
          />

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default RedEProtocol
