// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { FunctionComponent } from 'react'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { HasClassName, Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'
import { useBrowserName } from '@/hooks/use-browser-name'
import { useMemo, useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import Img from 'next/image'
import Divider from '@/components/divider'
import Button from '@/components/button'
import Network from '@/svgs/icons/network.svg'
import Sensor from '@/svgs/icons/sensor.svg'
import SolarPanel from '@/svgs/icons/solar-panel.svg'
import CircleFill from '@/svgs/icons/circle-fill.svg'

// * Partials
import ContactForm from '@/partials/forms/contact-form'

// * Templates
import ImageContentCarousel from '@/templates/image-content-carousel'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Modules
import anime from 'animejs'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = '',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Energy Solutions for Individuals and Businesses',
  description: 'Synergy provides innovative energy solutions including solar panel systems, harmonics reducers, and energy efficiency services for individuals and businesses to maximize energy efficiency, minimize costs, and reduce reliance on traditional energy sources. Get a free consultation today.',
  thumbnail: {
    src: 'young-couple-making-deal-thumbnail.webp',
    alt: 'a young couple making a deal with a businessman'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const CountItem: FunctionComponent<{
  item: {
    icon: JSX.Element,
    number: string,
    text: string
  },
  index: number
}> = ({ item, index }) => {

  const { icon, number, text } = item,
        Icon: FunctionComponent<HasClassName> = ({ className }) => <div className={className}>{icon}</div>,
        [ countString, setCountString ] = useState(number),
        { ref, inView } = useInView()

  const numberAmount = Number(number.replace(/\D/g, '')),
        numberSplit = number.split(`${numberAmount}`),
        numberBefore = numberSplit[0],
        numberAfter = numberSplit[1]

  let animeObject = useMemo(() => ({
    count: 0
  }), [])

  useEffect(() => {

    if (inView) anime({
      targets: animeObject,
      count: numberAmount,
      duration: 3000,
      delay: index * 100,
      round: 1,
      easing: 'easeOutQuint',
      update: () => {
        setCountString(`${numberBefore || ''}${animeObject.count}${numberAfter || ''}`)
      }
    })

  }, [inView, numberAmount, animeObject, numberAfter, numberBefore, index])

  return (
    <li className='flex items-center gap-4 group/item'>

      <div className='
        w-16 h-16
        bg-gray-700
        overflow-hidden
        rounded-2xl
        p-3
        transition-[fill,background-color] duration-300
      '>

        <Icon className='
          w-10 h-10
          fill-primary-500
        '/>

        <div className='
          w-16 h-16
          absolute top-1/2 left-1/2
          -translate-y-1/2 -translate-x-1/2
        '>

          <div className='
            w-20 h-20
            absolute top-1/2 left-1/2
            -translate-y-1/2 -translate-x-1/2 -rotate-225
          '>

            <div className='
              overflow-hidden
              bg-gray-600
              w-20 h-20 group-hover/item:w-0
              transition-[width] duration-500 ease-[cubic-bezier(0,.1,.1,.6)]
            '>

              <div className='w-20 h-20 absolute rotate-225'>

                <Icon className='
                  w-10 h-10
                  top-1/2 left-1/2
                  -translate-y-1/2 -translate-x-1/2
                  fill-gray-50
                '/>
                
              </div>
              
            </div>
            
          </div>
          
        </div>

      </div>

      <h3>
        <span ref={ref} className='text-gray-50 font-display font-black text-3xl'>{countString}</span>
        <br />
        <span className='text-xl text-gray-200'>{text}</span>
      </h3>

    </li>
  )
}

const Home: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  const isSafari = useBrowserName() === 'Safari'

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

        <script id='schema' type='application/ld+json' dangerouslySetInnerHTML={{ __html: `
          {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "name": "${meta.title} | ${businessName}",
            "description": "${meta.description}",
            "image": [
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/thumbnails/young-couple-making-deal-thumbnail.webp",
                "width": 1000,
                "height": 667
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/young-couple-making-deal.webp",
                "width": 2560,
                "height": 1707
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/solar-panels-cutout.webp",
                "width": 1000,
                "height": 449.635
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/svgs/logos/tune.svg",
                "width": 1024,
                "height": 750
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/svgs/logos/atalon-green.svg",
                "width": 1023.141,
                "height": 303.257
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/svgs/logos/kasper-electrical.svg",
                "width": 1016.411,
                "height": 259.294
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/mom-holding-son-playing-on-the-beach.webp",
                "width": 1000,
                "height": 667
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/man-working-on-electricity.webp",
                "width": 1000,
                "height": 668
              },
              {
                "@type": "ImageObject",
                "url": "${BASE_URL}/images/dad-holding-daughter-on-his-shoulders-and-son-on-his-side.webp",
                "width": 1000,
                "height": 667
              }
            ],
            "mainEntityOfPage": {
              "@type": "WebPage",
              "@id": "${BASE_URL}/"
            },
            "publisher": {
              "@type": "Organization",
              "name": "Synergy Energy Solutions",
              "logo": {
                "@type": "ImageObject",
                "url": "${BASE_URL}/svgs/logos/synergy-energy-solutions.webp"
              }
            },
            "offers": {
              "@type": "Offer",
              "url": "${BASE_URL}/#contact",
              "price": "0",
              "priceCurrency": "USD"
            },
            "breadcrumb": {
              "@type": "BreadcrumbList",
              "itemListElement": [
                {
                  "@type": "ListItem",
                  "position": 1,
                  "name": "Home",
                  "item": "${BASE_URL}/"
                }
              ]
            }
          }
        `}} />

      </Head>

      <ParallaxProvider>

        <main>

          <section className='
            pt-16 px-6
            z-10
            flex flex-col justify-center gap-12
            sm:aspect-[4/3] lg:aspect-auto
            lg:h-[45rem] xl:h-[47rem] 2xl:h-[60rem]
          '>

            <div className='
              max-w-[100rem] mx-auto
              absolute inset-0 -z-10
              overflow-y-hidden
              before:bg-gradient-to-b before:from-gray-700/75
              before:absolute before:inset-0 before:z-10
            '>

              <Parallax speed={-15} className='absolute inset-y-0 h-full motion-reduce:contents'>

                <Img
                  src='/images/young-couple-making-deal.webp'
                  alt='a young couple making a deal with a businessman'
                  width={2560}
                  height={1707}
                  sizes='100vw'
                  priority
                  loading='eager'
                  className='min-h-full object-cover'
                />

              </Parallax>

            </div>

            <article className='text-white sm:text-center'>

              <h1 className='text-2xl sm:text-3xl lg:text-4xl font-black font-display'>Maximize Energy Efficiency, Minimize Costs</h1>
              <br />

              <p className='text-xl sm:text-2xl lg:text-3xl max-w-xl lg:max-w-3xl mx-auto'>Switch to clean, energy-efficient systems that reduce the cost of electricity.</p>

            </article>

            <ContactForm andrillaContent={andrillaContent} ownerID={ownerID} addClass='-mb-64 lg:-mb-72' />
            
            <Divider addClass='fill-gray-50 dark:fill-gray-800 -z-10 top-auto' />

          </section>

          <section className='px-6 pt-96 sm:pt-72 md:pt-48 pb-12 grid grid-flow-row md:grid-cols-2 max-w-6xl mx-auto'>

            <div className='mb-12'>

              <h2 className='font-display'>
                <span className='text-primary-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>Why Synergy?</span>
                <br />
                <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>Our Approach to Energy</span>
              </h2>
              <br />

              <p className='text-gray-500 dark:text-gray-400 text-xl'>Innovation requires change, so we prioritize building relationships, educating clients, and creating positive experiences to revolutionize how we use energy.</p>
              <br />

              <Button href='#contact' padding='lg'>Use Synergy</Button>

            </div>

            <div className='flex items-end'>

              <Img
                src='/images/solar-panels-cutout.webp'
                alt='solar panels'
                width={1000}
                height={750}
                sizes='
                  (max-width: 600px) 100%,
                  576px
                '
                className='w-full max-w-xl aspect-video object-cover object-top pl-8 pr-8 md:pr-12'
              />

            </div>

            <ul className='
              p-4 rounded-4xl
              bg-gray-800 dark:bg-gray-900
              grid grid-flow-row gap-8 md:grid-cols-3
              md:col-span-2
              shadow-[1.5rem_2rem_0] shadow-gray-200 dark:shadow-gray-700
            '>

              {
                [
                  {
                    icon: <Network className='w-full h-full' />,
                    number: '26',
                    text: 'states serviced'
                  },
                  {
                    icon: <Sensor className='w-full h-full' />,
                    number: '129+',
                    text: 'buildings installed'
                  },
                  {
                    icon: <SolarPanel className='w-full h-full' />,
                    number: '$400+',
                    text: 'millions saved'
                  }
                ].map((item, index) => <CountItem key={`${item.number} ${item.text}`} item={item} index={index} />)
              }

            </ul>

          </section>

          <section className='pt-24 pb-24'>

            <article className='px-6 max-w-6xl mx-auto'>

              <h2 className='text-center mb-12'>
                <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold font-display'>Our Strategic Partners</span>
                <br />
                <span className='text-gray-500 dark:text-gray-400 text-xl'>make what we do possible</span>
              </h2>

              <div className='grid grid-rows-2 grid-cols-2 sm:flex flex-wrap justify-center gap-6 sm:gap-12 sm:max-w-3xl mx-auto'>

                {
                  [
                    {
                      name: 'Tune®',
                      logo: {
                        src: '/svgs/logos/tune.svg',
                        alt: 'Tune® – Simple Energy Savings Logo',
                        width: 1024,
                        height: 449.635
                      }
                    },
                    {
                      name: 'RED-E',
                      logo: {
                        src: '/svgs/logos/red-e.svg',
                        alt: 'RED-E Logo',
                        width: 1024,
                        height: 328.875
                      }
                    },
                    {
                      name: 'Kasper Electrical',
                      logo: {
                        src: '/svgs/logos/kasper-electrical.svg',
                        alt: 'Kasper Electrical logo',
                        width: 1016.411,
                        height: 259.294
                      }
                    },
                    {
                      name: 'Atalon Green',
                      logo: {
                        src: '/svgs/logos/atalon-green.svg',
                        alt: 'Atalon Green Logo',
                        width: 1023.141,
                        height: 303.257
                      }
                    },
                  ].map(partner => {

                    const { name, logo } = partner,
                          { src, alt, width, height } = logo

                    return (
                      <div
                        key={name}
                        title={name}
                        className='
                          max-w-full max-h-24
                          p-4 rounded-3xl
                          dark:bg-gray-200
                          border border-gray-200 dark:border-gray-700
                          shadow-[1rem_1.25rem_0] shadow-gray-200 dark:shadow-gray-700
                        '
                        style={{ aspectRatio: `${(width - (width * 0.15))} / ${height}` }}
                      >

                        <span className='sr-only'>{name}</span>

                        <Img
                          src={src}
                          alt={alt}
                          width={width}
                          height={height}
                          className='h-full object-contain dark:drop-shadow-[0_0.0625rem_0.25rem_hsl(0_0%_90%_/_0.5)] dark:brightness-75 dark:contrast-150 contrast-more:brightness-75 contrast-more:contrast-150'
                        />

                      </div>
                    )
                  })
                }

              </div>

              <div className='flex flex-col items-center mt-16'>

                <Parallax aria-hidden='true' speed={-3} className='absolute w-full motion-reduce:contents'>
                  <CircleFill className='motion-reduce:absolute top-10 left-1/2 -translate-x-1/2 fill-gray-400 w-2.5 h-2.5' />
                </Parallax>

                <hr className='h-24 border-l border-gray-400' />

                <div className='
                  p-4

                  before:absolute before:top-0 before:left-0 before:-z-10
                  before:w-56 before:h-20
                  before:rounded-3xl
                  before:border before:border-gray-400

                  after:absolute after:bottom-0 after:right-0 after:-z-10
                  after:w-56 after:h-20
                  after:rounded-3xl
                  after:border after:border-gray-400
                '>

                  <div className='bg-gray-50 dark:bg-gray-700 rounded-2xl max-w-xl py-12 sm:p-8'>

                    <p className='text-center text-xl text-gray-700 dark:text-gray-200 leading-[1.75]'>

                      We employ the <span className='whitespace-nowrap'>

                        <span title='RED-E' className='font-red-e top-1 text-shadow-[0_0.0625rem_0.4rem_hsl(0_0%_80%_/_0.25)]' style={isSafari ? { marginRight: '0.75rem' } : undefined}>RED-E</span> Protocol

                      </span> by using intelligent and responsible electrical systems that save our clients money and reduce their carbon footprint.

                    </p>

                  </div>

                </div>

              </div>
              
            </article>

          </section>

          <section className='pt-40 pb-12 grid grid-flow-row gap-24 bg-gray-900'>

            <Divider style='blocky-180' addClass='fill-gray-50 dark:fill-gray-800 bottom-auto -top-16' />

            <div className='px-6 grid grid-flow-row gap-8 md:grid-cols-2 max-w-6xl mx-auto'>

              <h2 className='text-3xl text-gray-50 font-bold font-display max-w-xs'>Energy Solutions Made Just for You</h2>

              <p className='text-xl text-gray-200 max-w-xl'>Innovation requires change, so we prioritize building relationships, educating clients, and creating positive experiences to revolutionize how we use energy.</p>

            </div>

            <ImageContentCarousel items={[
              {
                image: {
                  src: '/images/mom-holding-son-playing-on-the-beach.webp',
                  alt: 'mom holding son playing on the beach',
                  width: 1000,
                  height: 667
                },
                heading: 'Energy Efficiency Services',
                body: `Through our guidance and solutions, we help individuals and businesses reduce energy consumption and save money.`
              },
              {
                image: {
                  src: '/images/man-working-on-electricity.webp',
                  alt: 'man working on electricity',
                  width: 1000,
                  height: 668
                },
                heading: 'Harmonics Reducer',
                body: `Our system by Tune® reduces costs and waste by addressing heat and noise from essential devices in buildings and houses.`
              },
              {
                image: {
                  src: '/images/dad-holding-daughter-on-his-shoulders-and-son-on-his-side.webp',
                  alt: 'dad holding daughter on his shoulders and son on his side',
                  width: 1000,
                  height: 667
                },
                heading: 'Solar Panel Systems',
                body: `Renewable energy sources that can be installed on residential and commercial buildings to save money and reduce reliance on energy companies.`
              }
            ]}/>

          </section>

          <section className='px-6 pt-12 pb-24 bg-gray-900 grid grid-flow-row gap-12 md:grid-cols-2'>
            
            <div id='contact' className='absolute -top-24'>&nbsp;</div>

            <div className='md:col-start-2'>

              <h2 className='font-display'>
                <span className='text-primary-400 text-xl font-medium'>There's no need to wait.</span>
                <br />
                <span className='text-3xl text-gray-50 font-bold'>Get Started With a Free Consultation</span>
              </h2>
              <br />

              <p className='text-gray-200 text-xl'>Let us learn where your problem areas are, and we'll provide you with a solution that fits your needs.</p>

            </div>

            <ContactForm
              addId='1'
              andrillaContent={andrillaContent}
              ownerID={ownerID}
              addClass='md:col-start-1 md:row-start-1 dark:shadow-2xl dark:shadow-gray-800'
              button={{ label: 'Get a Quote' }}
            />

          </section>

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default Home
