// * Types
import type { NextApiRequest, NextApiResponse } from 'next'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {

  const userAgent = req.headers['user-agent'] as string

  if (
    userAgent.includes('Googlebot')
    ||
    userAgent.includes('Bingbot')
    ||
    userAgent.includes('Slurp')
  ) return res.json(true)

  return res.json(false)
}

export default handler