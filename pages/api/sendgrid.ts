const mail = require('@sendgrid/mail')
mail.setApiKey(process.env.SENDGRID_API_KEY)

// * Variables
import { formSubmissionNotificationID, andrillaTeamNotificationTemplateID } from '../../lib/sendgrid'

const sendGrid = async (req: any, res: any) => {

  const body = JSON.parse(req.body),
        { data, templateID, to } = body,
        isNotification: boolean = (templateID === formSubmissionNotificationID || templateID === andrillaTeamNotificationTemplateID)

  // Default Email to Owner
  let dynamicTemplateData: any = {
    formName: data.formName,
    url: data.url,
    fields: data.fields
  }

  // Email to Andrilla Team
  if (isNotification && templateID === andrillaTeamNotificationTemplateID) {
    dynamicTemplateData = {
      formName: data.formName,
      url: data.url,
      name: data.ownerName,
      email: data.ownerEmail
    }
  }

  // Email to Submitter
  if (!isNotification) {
    dynamicTemplateData = {
      name: to.name.split(' ')[0],
      url: data.url,
      logo: data.logo,
      financialHouseLink: data.financialHouseLink,
      user: {
        firstName: data.ownerName.split(' ')[0],
        email: data.ownerEmail
      },
      colors: data.colors
    }
  }

  const message = {
    to: [
      {
        email: to.email,
        name: to.name
      }
    ],
    from: {
      email: 'mail@andrilla.net',
      name: isNotification ? 'Andrilla Notification' : data.ownerName
    },
    replyTo: {
      email: isNotification ? 'info@andrilla.net' : data.ownerEmail,
      name: isNotification ? 'Andrilla Team' : data.ownerName
    },
    template_id: templateID,
    dynamic_template_data: dynamicTemplateData
  }

  await mail.send(message)
    .then((res: any) => {
      res[0].statusCode
      res[0].headers
      console.log(`Email successfully sent to ${message.to[0].email}`)
    })
    .catch((error: any) => {
      console.error(`Failed to send email to ${message.to[0].email}`, error)
    })
}

export default sendGrid