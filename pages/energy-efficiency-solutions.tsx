// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import { InView } from 'react-intersection-observer'
import RoundedSection from '@/components/rounded-section'
import Img from 'next/image'
import Card from '@/components/card'
import Button from '@/components/button'
import Banknote from '@/svgs/icons/banknote.svg'
import LightRibbon from '@/svgs/icons/light-ribbon.svg'
import BoltTrianglebadgeExclamationmark from '@/svgs/icons/bolt-trianglebadge-exclamationmark.svg'
import PersonFillQuestionmark from '@/svgs/icons/person-fill-questionmark.svg'
import Eye from '@/svgs/icons/eye.svg'
import DocTextMagnifyingglass from '@/svgs/icons/doc-text-magnifyingglass.svg'
import Sensor from '@/svgs/icons/sensor.svg'
import SolarPanel from '@/svgs/icons/solar-panel.svg'
import Buildings from '@/svgs/icons/buildings.svg'
import DialHigh from '@/svgs/icons/dial-high.svg'
import FigureWaterFitness from '@/svgs/icons/figure-water-fitness.svg'
import CheckmarkSeal from '@/svgs/icons/checkmark-seal.svg'
import MinusPlusBatteryblock from '@/svgs/icons/minus-plus-batteryblock.svg'
import BoltCar from '@/svgs/icons/bolt-car.svg'
import HouseDeskclock from '@/svgs/icons/house-deskclock.svg'
import IPhoneHouse from '@/svgs/icons/iphone-house.svg'

// * Templates
import InfoGrid from '@/templates/info-grid'
import ContactFooter from '@/templates/contact-footer'
import Testimonials from '@/templates/testimonials'
import GiantItemList from '@/templates/giant-item-list'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'Energy Efficiency Solutions',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Energy Efficiency Solutions for Home and Business',
  description: 'We offer consultation services that help you identify areas of improvement for your energy usage, reduce costs, and increase sustainability. Our three-step process is simple and effective. Schedule a consultation today and let us help you optimize your energy consumption.',
  thumbnail: {
    src: 'consultation-services-thumbnail.webp',
    alt: 'Preview of Energy Efficiency Solutions page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const EnergyEfficiencyServices: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <ParallaxProvider>

        <main>

          <InView threshold={0.5} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} interior={{ addClass: 'bg-gray-900 pb-96' }}>

                <div className={`absolute inset-0 min-h-[36rem] lg:max-h-[36rem] w-full overflow-hidden ${inView ? '' : 'rounded-5xl'} isolate transition-[border-radius] duration-500`}>
    
                  <Parallax
                    speed={-5}
                    className={`
                      motion-reduce:contents
                      absolute right-0 -bottom-10 md:bottom-auto top-auto md:-top-10 md:left-auto
                      h-1/2 w-full md:w-1/2 md:h-[calc(100%+5rem)]
                      after:content-[""]
                      after:absolute after:inset-0 after:z-10
                      md:after:w-1/4 after:h-1/4 md:after:h-full
                      after:bg-gradient-to-b md:after:bg-gradient-to-r after:from-gray-900
                    `}
                  >
    
                    <div className='
                      motion-safe:contents
                      absolute right-0 -bottom-10 md:bottom-auto top-auto md:-top-10 md:left-auto
                      h-1/2 w-full md:w-1/2 md:h-[calc(100%+5rem)]
                      after:content-[""]
                      after:absolute after:inset-0 after:z-10
                      md:after:w-1/4 after:h-1/4 md:after:h-full
                      after:bg-gradient-to-b md:after:bg-gradient-to-r after:from-gray-900
                    '>

                      <Img
                        src='/images/black-family-on-front-porch.webp'
                        alt='Black family on front porch'
                        width={2000}
                        height={1333}
                        className='absolute inset-0 w-full h-full object-cover'
                        sizes='
                          100vw,
                          (min-width: 768px) 50vw
                        '
                        priority
                        loading='eager'
                      />
                      
                    </div>
    
                  </Parallax>
    
                </div>
    
                <Card
                  heading='Energy Efficiency Designed for You'
                  body='Our consultation services help you or your business make informed decisions about your energy usage and reduce your energy bills. We evaluate your current energy consumption, identify areas for improvement, and provide practical and effective solutions that improve your bottom line.'
                  options={{
                    heading: {
                      as: 'h1',
                      addClass: 'text-gray-700 dark:text-gray-50 md:w-3/4'
                    }
                  }}
                  className='
                    bg-gray-50 dark:bg-gray-800
                    rounded-3xl
                    max-w-2xl
                    overflow-y-hidden
                    border-[1.5rem] sm:border-[3rem] border-gray-50 dark:border-gray-800
                    max-h-[31.5rem]
                  '
                />
    
              </RoundedSection>
            )}
          </InView>

          <section className='px-6 py-8 pb-12 max-w-6xl mx-auto'>

            <div className='grid grid-flow-row md:grid-cols-2 place-items-center'>

              <div className='mb-12'>

                <h2 className='font-display'>
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>Common Challenges</span>
                  <br />
                  <span className='text-primary-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>that we can solve.</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>Our team of experts can help you identify and solve common energy-related issues, so you can enjoy a more efficient and sustainable future.</p>
                <br />

                <Button href='#contact' padding='lg'>Get My Assessment</Button>

              </div>

              <div className='flex items-end'>

                <Img
                  src='/images/synergy-rep-cutout.webp'
                  alt='Smiling Synergy representative with thumbs up'
                  width={518}
                  height={1000}
                  className='
                    w-full max-w-xl max-h-[20rem] md:max-h-[25rem]
                    object-contain object-top pl-8 pr-8 md:pr-12'
                />

              </div>

            </div>

            <InfoGrid
              items={[
                {
                  Icon: Banknote,
                  heading: 'High energy bills',
                  body: 'Unnecessary expenses draining your budget.'
                },
                {
                  Icon: LightRibbon,
                  heading: 'Outdated infrastructure',
                  body: 'Limited capabilities and higher risk of failure.'
                },
                {
                  Icon: BoltTrianglebadgeExclamationmark,
                  heading: 'Equipment not working as it should',
                  body: 'Reduced efficiency and increased energy costs.'
                },
                {
                  Icon: PersonFillQuestionmark,
                  heading: 'Understanding your energy usage',
                  body: 'Lack of visibility into usage patterns and waste.'
                },
                {
                  Icon: Eye,
                  heading: 'Finding sustainable energy solutions',
                  body: 'Difficulty increasing efficiency and reducing impact.'
                },
                {
                  Icon: DocTextMagnifyingglass,
                  heading: 'Taking advantage of tax credits',
                  body: 'Spending more on taxes than necessary.'
                }
              ]}
            />

          </section>

          <section className='py-24'>

            <div className='absolute inset-0 overflow-x-hidden'>

              <Img
                src='/svgs/backgrounds/wave-rtl-primary.svg'
                alt='Wave form gradually growing from small to large, and rising up left to right as it grows'
                width={1440}
                height={761.6}
                className='
                  absolute top-1/2 left-1/2 -z-50
                  -translate-y-1/2 -translate-x-1/2
                  min-w-[100rem] w-full
                '
              />
              
            </div>

            <div className='px-6 max-w-6xl mx-auto'>

              <div className='max-w-xl'>

                <h2 className='font-display text-3xl text-gray-700 dark:text-gray-200 font-bold'>Our Process is Simple</h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>3-steps to keeps you informed and involved from the initial consultation to the final installation.</p>

              </div>

            </div>

            <div className='px-6 max-w-[90rem] mx-auto'>

              {/* Classes pre-loaded for Tailwind */}
              <span aria-hidden='true' className='border-primary-400'>&nbsp;</span>
              <span aria-hidden='true' className='border-primary-600'>&nbsp;</span>
              <span aria-hidden='true' className='border-primary-700'>&nbsp;</span>

              <GiantItemList
                items={[
                  {
                    heading: {
                      text: 'Schedule a consultation',
                      addClass: 'lg:max-h-28 lg:w-64'
                    },
                    body: 'Discuss your energy challenges and goals',
                    identifier: {
                      addClass: 'before:text-primary-700 md:before:-left-16 md:before:-top-20'
                    }
                  },
                  {
                    heading: {
                      text: 'Receive a personalized assessment',
                      addClass: 'lg:max-h-28 lg:w-[22rem]'
                    },
                    body: 'Learn our solutions through a proposal and timeline',
                    identifier: {
                      addClass: 'before:text-primary-600 md:before:-left-20 md:before:-top-20 lg:ml-8'
                    }
                  },
                  {
                    heading: {
                      text: 'Implement and enjoy the savings',
                      addClass: 'lg:max-h-28 lg:w-72'
                    },
                    body: {
                      text: `That's it!`,
                      addClass: 'text-2xl'
                    },
                    identifier: {
                      addClass: 'before:text-primary-400 md:before:-left-16 md:before:-top-20 lg:ml-16'
                    }
                  }
                ]}
              />

            </div>

          </section>

          <section className='px-6 py-24 max-w-6xl mx-auto'>

            <div className='grid grid-flow-row md:grid-cols-2 place-items-center'>

              <div className='mb-20'>

                <h2 className='font-display'>
                  <span className='text-primary-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>What all do we do?</span>
                  <br />
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>Our Services</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>We offer many services outside of our expert consultations, all with the excellence you'd expect.</p>
                <br />

                <Button href='#contact' padding='lg'>Use Synergy</Button>

              </div>

            </div>

            <InfoGrid
              items={[
                {
                  Icon: Sensor,
                  heading: 'Tune® Energy',
                  body: 'Saves money and extends the life of electronics.'
                },
                {
                  Icon: SolarPanel,
                  heading: 'Residential Solar',
                  body: 'Save on all of your energy uses.'
                },
                {
                  Icon: Buildings,
                  heading: 'Commercial Solar',
                  body: 'Savings and sustainability (coming soon).'
                },
                {
                  Icon: DialHigh,
                  heading: 'Solar Water Heating',
                  body: 'Heat your water without raising your bills.'
                },
                {
                  Icon: FigureWaterFitness,
                  heading: 'Solar Pool Heating',
                  body: 'Enjoy a warm pool without expensive bills.'
                },
                {
                  Icon: CheckmarkSeal,
                  heading: 'Solar and Tune® Energy Installation',
                  body: 'Professional installation to ensure security.'
                },
                {
                  Icon: MinusPlusBatteryblock,
                  heading: 'Battery Storage',
                  body: 'Keep your batteries safe and healthy.'
                },
                {
                  Icon: BoltCar,
                  heading: 'EV Chargers',
                  body: 'Charge your electric vehicles.'
                },
                {
                  Icon: HouseDeskclock,
                  heading: 'Automation',
                  body: 'Predict energy bills and enjoy life more.'
                },
                {
                  Icon: IPhoneHouse,
                  heading: 'Remote Monitoring',
                  body: `Know what's going on from anywhere (coming soon).`
                }
              ]}
              className='
                p-4 rounded-4xl
                bg-gray-800 dark:bg-gray-900
                shadow-[1.5rem_2rem_0] shadow-gray-200 dark:shadow-gray-700
                gap-8 sm:gap-10
              '
              options={{
                heading: {
                  className: 'text-gray-50 font-display font-black text-xl'
                }
              }}
            />

          </section>

          <Testimonials
            intro={`That's our process…`}
            heading='So what do people think of it?'
            testimonials={[
              {
                name: 'Sophia Rodriguez',
                body: `I recently had Synergy install energy-efficient lighting in my home, and I couldn't be happier with the results. Their team was professional, efficient, and the cost savings on my electricity bill have been significant.`,
                image: {
                  src: '/images/testimonials/sophia-rodriquez.webp',
                  alt: 'Sophia Rodriguez',
                  width: 150,
                  height: 225
                }
              },
              {
                name: 'Incredit',
                body: `Synergy helped us become more energy-efficient and reduce our operating costs. We can't thank them enough for how they helped our business reach our carbon footprint goals.`,
                image: {
                  src: '/images/testimonials/incredit.webp',
                  alt: 'Incredit Logo',
                  width: 150,
                  height: 150
                }
              },
              {
                name: 'Michael Chen',
                body: `Working with Synergy to upgrade my home with solar panels was a great decision. The team was knowledgeable, friendly, and the improvements have made a noticeable difference.`,
                image: {
                  src: '/images/testimonials/michael-chen.webp',
                  alt: 'Michael Chen',
                  width: 210,
                  height: 150
                }
              },
              {
                name: 'Get Served Servers',
                body: `Synergy's energy-efficient solutions have helped us reduce our operating costs and improve our sustainability. Their team is knowledgeable, reliable, and committed to providing exceptional service.`,
                image: {
                  src: '/images/testimonials/get-served-servers.webp',
                  alt: 'Get Served Servers Logo',
                  width: 150,
                  height: 150
                }
              },
              {
                name: 'Emily and David Johnson',
                body: `We had an excellent experience with Synergy. Their team installed the harmonics converter, and we've been thrilled with the results. We highly recommend Synergy to anyone looking to save money.`,
                image: {
                  src: '/images/testimonials/emily-and-david-johnson.webp',
                  alt: 'Emily and David Johnson',
                  width: 150,
                  height: 225
                }
              }
            ]}
          />

          <ContactFooter
            intro='Ready to meet the team?'
            heading='Sign up for a Free Consultation'
            andrillaContent={andrillaContent}
          />

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default EnergyEfficiencyServices
