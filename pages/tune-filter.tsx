// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Libraries
import { tuneDominosVideo } from '@/lib/video-sources'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import { InView } from 'react-intersection-observer'
import Card from '@/components/card'
import Img from 'next/image'
import Button from '@/components/button'
import BoltRingClosed from '@/svgs/icons/bolt-ring-closed.svg'
import NetworkShield from '@/svgs/icons/network-shield.svg'
import LightbulbLed from '@/svgs/icons/lightbulb-led.svg'
import Video from '@/components/video'

// * Templates
import RoundedSection from '@/components/rounded-section'
import ContactFooter from '@/templates/contact-footer'
import GiantItemList from '@/templates/giant-item-list'
import Testimonials from '@/templates/testimonials'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'Tune® Filter',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Save on Energy Bills and Extend Electronics Life',
  description: 'Tune® is a passive device that reduces heat and disruptive noise in your electrical system, saving you money on energy bills and extending the life of your electronics. Install Tune® in your breaker box, with included instructions or by a certified electrician, and enjoy the benefits of its 24/7 operation. Learn how Tune® helped businesses like RPM Pizza cut their energy costs by 10%* and sign up for a free consultation with our expert team to find out how Tune® can help you too.',
  thumbnail: {
    src: 'tune-filter-thumbnail.webp',
    alt: 'Preview of Tune® Filter page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const TuneFilter: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <ParallaxProvider>

        <main>

          <InView threshold={0.7} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} interior={{ addClass: 'bg-gradient-to-bl from-tune-300 to-tune-800' }}>
    
                <Card
                  heading='Savings You Can Feel'
                  body='Tune® is a passive device that reduces heat and disruptive noise in your electrical system, which saves you a lot of money on energy bills, and extends the life of your electronics and appliances.'
                />

                <Parallax speed={5} className='contens md:motion-safe:block'>

                  <Img
                    src='/images/tune-cutout.webp'
                    alt='Tune® Filter'
                    width={478}
                    height={615}
                    className='object-contain h-96 rotate-[7deg] drop-shadow-3xl'
                    sizes='
                      100vw,
                      (min-width: 768px) 50vw
                    '
                    priority
                    loading='eager'
                  />

                </Parallax>
    
              </RoundedSection>
            )}
          </InView>

          <section className='px-6 py-8 pb-12 max-w-6xl mx-auto'>

            <div className='grid grid-flow-row md:grid-cols-2 place-items-center'>

              <div className='mb-20'>

                <h2 className='font-display'>
                  <span className='text-tune-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>Why Tune® is the…</span>
                  <br />
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>The Smart Choice</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>Our team of experts can help you identify and solve common energy-related issues, so you can enjoy a more efficient and sustainable future.</p>
                <br />

                <Button href='#contact' padding='lg' color='tune'>Install Tune®</Button>

              </div>

            </div>

            <ul className={`
              grid grid-flow-row md:grid-cols-3
              p-4 rounded-4xl
              bg-gradient-to-br from-tune-500 to-tune-700
              shadow-[1.5rem_2rem_0] shadow-gray-200 dark:shadow-gray-700
              gap-8
            `}>

              {
                [
                  {
                    Icon: BoltRingClosed,
                    heading: 'Simple',
                    body: 'Just install it, and save on your electric bills.'
                  },
                  {
                    Icon: NetworkShield,
                    heading: 'Safe',
                    body: `Rigorously tested to perform its best. Plus, there's no maintenance.`
                  },
                  {
                    Icon: LightbulbLed,
                    heading: 'Smart',
                    body: 'Works 24/7 to save you money, and keep your electronics alive.'
                  }
                ].map(item => {

                  const { Icon, heading, body } = item

                  return (
                    <li key={heading} className='flex flex-col sm:flex-row gap-4 group/item'>

                      <Icon className='
                        w-16 h-16 md:h-fit md:aspect-square
                        bg-tune-100 bg-opacity-25
                        fill-white
                        rounded-2xl
                        p-3
                      '/>

                      <div className='flex flex-col gap-2'>

                        <h3 className='text-gray-50 font-display font-black text-3xl'>{heading}</h3>

                        <p className='text-gray-200 max-w-xs'>{body}</p>

                      </div>

                    </li>
                  )
                })
              }

            </ul>

          </section>

          <InView threshold={0.75} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection
                ref={ref}
                inView={inView}
                interior={{
                  className: `
                    bg-gradient-to-bl from-tune-800 to-tune-900
                    w-full min-h-[36rem]
                    flex flex-col items-center gap-12
                    p-6 sm:p-12
                    ${inView ? '' : 'rounded-5xl'} motion-reduce:rounded-5xl
                    transition-[border-radius] duration-500
                    overflow-hidden isolate
                  `
                }}
              >
    
                <div className='text-center mx-auto max-w-3xl'>

                  <h2 className='font-display font-black text-3xl text-gray-50'>Learn How Tune® Helped RPM Pizza</h2>
                  <br />

                  <p className='text-gray-300 text-lg'>RPM Pizza, LLC is the largest franchise owner of Domino's® Pizza in the United States, employing over 3,500 team members. Tune® cut their energy costs by 10%*.</p>

                </div>

                <div className='w-full'>

                  <Video
                    src={tuneDominosVideo}
                    thumbnail={{
                      src: '/images/tune-dominos-video-thumbnail.webp',
                      alt: 'Person making pizza, with Dominos Pizza logo',
                      width: 2560,
                      height: 1424,
                      className: 'absolute inset-0 object-cover rounded-lg bg-black object-bottom w-full h-full'
                    }}
                    title='Tune® x RPM Dominos®'
                    className='
                      w-full rounded-lg aspect-[12/5] overflow-hidden'
                  />

                  <span className='block mt-4 text-xs text-gray-400 text-center'>*Savings data based on analysis of existing installations.</span>
                  
                </div>
  
              </RoundedSection>
            )}
          </InView>

          <section className='py-24'>

            <div className='absolute inset-0 overflow-x-hidden'>

              <Img
                src='/svgs/backgrounds/wave-ltr-tune.svg'
                alt='Wave form gradually shrinking from large to small, and lowering down left to right as it shrinks'
                width={1440}
                height={761.6}
                className='
                  absolute top-1/2 left-1/2 -z-50
                  -translate-y-1/2 -translate-x-1/2
                  min-w-[100rem] w-full
                '
              />
              
            </div>

            <div className='px-6 max-w-6xl mx-auto'>

              <div className='md:ml-auto max-w-xl md:text-right'>

                <h2 className='font-display'>
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>What You Should Know</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>These are the most common questions people want to know, before using Tune®.</p>

              </div>

            </div>

            <div className='px-6 max-w-[90rem] mx-auto'>

              {/* Classes pre-loaded for Tailwind */}
              <span aria-hidden='true' className='border-tune-400'>&nbsp;</span>
              <span aria-hidden='true' className='border-tune-500'>&nbsp;</span>
              <span aria-hidden='true' className='border-tune-600'>&nbsp;</span>

              <GiantItemList
                customIdentifier='+'
                items={[
                  {
                    heading: {
                      text: 'How Does Tune® Work?',
                      addClass: 'lg:max-h-28 md:w-64 lg:w-64'
                    },
                    body: 'Installed in the breaker box, Tune® reduces current harmonics, from your system.',
                    identifier: {
                      addClass: 'before:text-tune-600 md:col-start-3 md:col-end-4'
                    }
                  },
                  {
                    heading: {
                      text: ' Where does the disruptive noise and heat come from?',
                      addClass: 'lg:max-h-36 md:w-64 lg:w-[22rem]'
                    },
                    body: 'Any device that converts AC power to DC (high frequency) power creates harmonics. The culprits are all around you.',
                    identifier: {
                      addClass: 'before:text-tune-500 md:col-start-2 md:col-end-3'
                    }
                  },
                  {
                    heading: {
                      text: 'Can you tell me about installation?',
                      addClass: 'lg:max-h-28 md:w-64 lg:w-72'
                    },
                    body: {
                      text: `Tune® is simple to install in your breaker box, with included instructions, however, installation by a certified electrician, is recommended.`,
                    },
                    identifier: {
                      addClass: 'before:text-tune-400'
                    }
                  }
                ]}
              />

            </div>

          </section>

          <Testimonials
            intro='Many businesses use Tune®'
            heading='And They LOVE It'
            testimonials={[
              {
                name: 'Informa Markets',
                body: `By reducing heat in our system, saving energy, and saving equipment, Tune® is helping us champion sustainability in our business… the people at Tune® go out of their way to help us.`,
                image: {
                  src: '/images/testimonials/informa-markets.webp',
                  alt: 'Informa Markets Logo',
                  width: 242,
                  height: 242
                }
              },
              {
                name: 'RPM Pizza',
                body: `We don't want Team Members servicing electronics, we want them focused on providing exceptional service to our customers. Tune® did that for us.`,
                image: {
                  src: '/images/testimonials/rpm-pizza.webp',
                  alt: 'RPM Pizza Logo',
                  width: 306,
                  height: 190,
                  className: 'object-contain aspect-square w-20 rounded-xl bg-[#32648E]'
                }
              },
              {
                name: 'Rainsville, Alabama',
                body: `We are not only saving money, we are already seeing savings in maintenance. We don't get 5 calls a day about the issues at the new pumping station since we installed the filters.`,
                image: {
                  src: '/images/testimonials/rainsville-alabama.webp',
                  alt: 'Rainsville, Alabama Logo',
                  width: 442,
                  height: 302,
                  className: 'object-contain aspect-square w-20 rounded-xl bg-white'
                }
              },
              {
                name: 'Starnes Quarter Horses',
                body: `Our typical power bill has always been over $1000 a month, sometimes over $1400. For the first time EVER, it is down to under $850!`,
                image: {
                  src: '/images/testimonials/starnes-quarter-horses.webp',
                  alt: 'Starnes Quarter Horses Owners',
                  width: 250,
                  height: 252
                }
              },
              {
                name: 'Henkel Corporation',
                body: `A good example is our water softener, which is equipped with plastic relays that protect the system. In the past, this was a problem because we were blowing these relays quite regularly. `,
                image: {
                  src: '/images/testimonials/henkel-corporation.svg',
                  alt: 'Henkel Corporation Logo',
                  width: 76.266,
                  height: 56.69,
                  className: 'object-contain aspect-square w-20 rounded-xl bg-white px-1'
                }
              }
            ]}
          />

          <ContactFooter
            intro='Ready to meet the team?'
            heading='Sign up for a Free Consultation'
            andrillaContent={andrillaContent}
          />

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default TuneFilter
