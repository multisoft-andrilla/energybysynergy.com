// * Types
import type { AppProps } from 'next/app'

// * Components
import { Toaster } from 'react-hot-toast'

// * Partials
import Header from '@/partials/_header'
import Footer from '@/partials/_footer'

// * Styles
import '@/styles/globals.css'

// * Typefaces
import localFont from 'next/font/local'

const geom = localFont({
  src: [
    {
      path: '../typefaces/geom/geom.ttf'
    }
  ],
  variable: '--typeface-geom'
})

const montserratAlt1 = localFont({
  src: [
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-thin.ttf',
      weight: '100',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-extralight.ttf',
      weight: '200',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-light.ttf',
      weight: '300',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-regular.ttf',
      weight: '400',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-medium.ttf',
      weight: '500',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-semibold.ttf',
      weight: '600',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-bold.ttf',
      weight: '700',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-extrabold.ttf',
      weight: '800',
      style: 'normal'
    },
    {
      path: '../typefaces/montserrat-alt1/montserrat-alt1-black.ttf',
      weight: '900',
      style: 'normal'
    }
  ],
  variable: '--typeface-montserrat-alt1'
})

const redE = localFont({
  src: [
    {
      path: '../typefaces/red-e/red-e.otf',
      weight: '400',
      style: 'normal'
    }
  ],
  variable: '--typeface-red-e'
})

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <div className={`${geom.variable} ${montserratAlt1.variable} ${redE.variable} font-sans`}>
        <Header />
        <Component {...pageProps} />
        <Footer />
      </div>
      <Toaster />
    </>
  )
}

export default App
