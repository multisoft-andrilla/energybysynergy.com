// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'
import { InView } from 'react-intersection-observer'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import Img from 'next/image'
import Card from '@/components/card'
import Atropos from 'atropos/react'

// * Templates
import RoundedSection from '@/components/rounded-section'
import Testimonials from '@/templates/testimonials'
import ContactFooter from '@/templates/contact-footer'

// * Styles
import 'atropos/css'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'About Us',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: 'Innovative and Reliable Energy Solutions',
  description: 'Synergy is committed to delivering innovative and reliable energy solutions that support sustainable living to help our customers achieve their goals. Our team of power experts is dedicated to providing exceptional service and empowering you to make informed choices that optimize your energy consumption while improving your overall quality of life. Contact us today for a free consultation.',
  thumbnail: {
    src: 'about-us-thumbnail.webp',
    alt: 'Preview of About Us page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const AboutUs: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <ParallaxProvider>

        <main>

          <InView threshold={0.75} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} exterior={{ addClass: 'overflow-visible' }} interior={{ addClass: 'overflow-visible' }}>

                <div className={`absolute inset-0 min-h-[36rem] lg:max-h-[36rem] w-full overflow-hidden ${inView ? '' : 'rounded-5xl'} isolate transition-[border-radius] duration-500`}>
    
                  <Parallax
                    speed={-5}
                    className={`
                      motion-reduce:contents
                      absolute inset-0 -top-10 w-full h-[calc(100%+5rem)]
                      after:content-[""]
                      after:absolute after:inset-0 after:z-10
                      after:w-full after:h-full
                      after:bg-gradient-to-b after:from-gray-50
                      after:opacity-75
                    `}
                  >
    
                    <Img
                      src='/images/clear-skies-plant-hills.webp'
                      alt='Clear blue skies over mountains hills filled with various plants'
                      width={2560}
                      height={3836}
                      className='absolute inset-0 w-full h-full object-cover'
                      sizes='100vw'
                      priority
                      loading='eager'
                    />
    
                  </Parallax>
    
                </div>

                <Img
                  src='/images/kid-swinging.webp'
                  alt='Young boy swinging'
                  width={742}
                  height={473}
                  className='hidden invisible lg:block lg:visible absolute top-1/2 left-[34rem] -translate-y-12 -rotate-3'
                />
    
                <Card
                  heading='Our Purpose'
                  body='We believe in the power of smart energy solutions that assist our clients in reducing their energy costs while enhancing their everyday lives. We are committed to delivering innovative and reliable energy solutions that support sustainable living to help our customers achieve their goals.'
                  options={{
                    heading: {
                      as: 'h1'
                    }
                  }}
                />
    
              </RoundedSection>
            )}
          </InView>

          <InView threshold={0.75} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} interior={{
                className: `w-full min-h-[37rem] md:max-h-[36rem] md:flex items-center ${inView ? '' : 'rounded-5xl'} motion-reduce:rounded-5xl transition-[border-radius] duration-500 isolate overflow-hidden`
              }}>
    
                <Img
                  src='/images/happy-working-father-using-laptop-with-his-family-home.webp'
                  alt='happy working father using laptop with his family home'
                  width={800}
                  height={533}
                  className={`object-cover ${inView ? '' : 'rounded-t-5xl'} transition-[border-radius] duration-500 md:hidden md:invisible`}
                />

                <div className={`
                  md:h-full md:top-10 overflow-hidden
                  hidden invisible md:block md:visible
                  ${inView ? '' : 'rounded-t-5xl'}
                  transition-[border-radius] duration-500
                  -mr-48 lg:-mr-8
                  after:content-[""]
                  after:absolute after:inset-0
                  after:bg-gradient-to-l after:from-gray-50 after:via-gray-50/75
                  lg:after:hidden
                `}>

                  <Img
                    src='/images/happy-working-father-using-laptop-with-his-family-home-cutout.webp'
                    alt='happy working father using laptop with his family home'
                    width={1000}
                    height={701}
                    className={`
                      hidden invisible md:block md:visible
                      object-cover
                    `}
                  />

                </div>
    
                <Card
                  heading='Our Commitment to Energy Efficiency'
                  body='Energy efficiency is important in reducing costs and promoting sustainable living. We believe in empowering you to make informed choices that optimize your energy consumption while improving your overall quality of life.'
                  className='max-w-2xl overflow-y-hidden p-6 sm:p-12'
                />
    
              </RoundedSection>
            )}
          </InView>

          <InView threshold={0.45} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} interior={{ addClass: 'bg-gray-800 dark:bg-gray-900' }}>
    
                <Card
                  heading='Our Promise of Integrity'
                  body='We believe that honesty, transparency, and ethical business practices are fundamental to our success. Our commitment to integrity is reflected in everything we do, from our transparent pricing and open communication to our sustainable and efficient energy solutions.'
                  className='max-w-2xl overflow-y-hidden'
                  options={{
                    heading: { addClass: 'text-gray-50' },
                    body: { addClass: 'text-gray-300' }
                  }}
                />

                <Parallax speed={-5} className='contents md:motion-safe:block'>

                  <div className='grid grid-flow-row md:grid-rows-3 md:grid-cols-2 place-items-center gap-8'>

                    {
                      [
                        {
                          name: 'Tune®',
                          logo: {
                            src: '/svgs/logos/tune.svg',
                            alt: 'Tune® – Simple Energy Savings Logo',
                            width: 1024,
                            height: 449.635
                          }
                        },
                        {
                          name: 'RED-E',
                          logo: {
                            src: '/svgs/logos/red-e.svg',
                            alt: 'RED-E Logo',
                            width: 1024,
                            height: 328.875
                          },
                          addClass: 'md:-top-8'
                        },
                        {
                          name: 'Synergy Energy Solutions',
                          logo: {
                            src: '/svgs/logos/synergy-energy-solutions.svg',
                            alt: 'Synergy Energy Solutions Logo',
                            width: 1024,
                            height: 369.7
                          },
                          className: `
                            w-fit max-h-28
                            py-4 px-8 rounded-3xl
                            bg-gray-50
                            md:col-span-2
                          `
                        },
                        {
                          name: 'Kasper Electrical',
                          logo: {
                            src: '/svgs/logos/kasper-electrical.svg',
                            alt: 'Kasper Electrical Logo',
                            width: 1016.411,
                            height: 259.294
                          },
                          addClass: 'md:-bottom-8'
                        },
                        {
                          name: 'Atalon Green',
                          logo: {
                            src: '/svgs/logos/atalon-green.svg',
                            alt: 'Atalon Green Logo',
                            width: 1023.141,
                            height: 303.257
                          }
                        },
                      ].map(partner => {

                        const { name, logo, className, addClass } = partner,
                              { src, alt, width, height } = logo

                        return (
                          <div
                            key={name}
                            title={name}
                            className={`
                              ${className || `
                                w-fit max-h-24
                                py-4 px-8 rounded-3xl
                                bg-gray-50
                              `}
                              ${addClass || ''}
                            `}
                            style={{ aspectRatio: `${(width - (width * 0.15))} / ${height}` }}
                          >

                            <span className='sr-only'>{name}</span>

                            <Img
                              src={src}
                              alt={alt}
                              width={width}
                              height={height}
                              className='h-full object-contain dark:drop-shadow-[0_0.0625rem_0.5rem_hsl(0_0%_80%_/_0.5)]'
                            />

                          </div>
                        )
                      })
                    }

                  </div>

                </Parallax>
    
              </RoundedSection>
            )}
          </InView>

          <section className='pt-24 pb-24 px-6 max-w-6xl mx-auto'>

            <h2 className='text-center mb-12'>
              <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold font-display'>Meet the Power Team</span>
              <br />
              <span className='text-gray-500 dark:text-gray-400 text-xl'>The people that bring the energy to Synergy.</span>
            </h2>

            <div className='
              mx-auto
              grid grid-flow-row sm:grid-cols-2 lg:grid-cols-4 gap-x-16 gap-y-8 sm:gap-y-12 lg:gap-y-24
              w-fit
            '>

              <style jsx-global='true'>{`
                .atropos-inner {
                  border-radius: 1.5rem;
                }
              `}</style>

              {
                [
                  {
                    name: 'Blaze Voltage',
                    title: 'Power Surge Technician',
                    image: {
                      src: '/images/team-members/blaze-voltage.webp',
                      alt: 'Blaze Voltage',
                      width: 600,
                      height: 400
                    }
                  },
                  {
                    name: 'Spark Thunderbolt',
                    title: 'Power Surge Technician',
                    image: {
                      src: '/images/team-members/spark-thunderbolt.webp',
                      alt: 'Spark Thunderbolt',
                      width: 400,
                      height: 598
                    }
                  },
                  {
                    name: 'Surge Dynamo',
                    title: 'High Voltage Engineer',
                    image: {
                      src: '/images/team-members/surge-dynamo.webp',
                      alt: 'Surge Dynamo',
                      width: 400,
                      height: 600
                    }
                  },
                  {
                    name: 'Arc Electra',
                    title: 'High Voltage Engineer',
                    image: {
                      src: '/images/team-members/arc-electra.webp',
                      alt: 'Arc Electra',
                      width: 400,
                      height: 598
                    }
                  },
                  {
                    name: 'Flash Watts',
                    title: 'Energy Transformer Specialist',
                    image: {
                      src: '/images/team-members/flash-watts.webp',
                      alt: 'Flash Watts',
                      width: 400,
                      height: 600
                    }
                  },
                  {
                    name: 'Bolt Kilowatt',
                    title: 'Energy Transformer Specialist',
                    image: {
                      src: '/images/team-members/bolt-kilowatt.webp',
                      alt: 'Bolt Kilowatt',
                      width: 400,
                      height: 500
                    }
                  },
                  {
                    name: 'Zapper Joule',
                    title: 'Lightning Protection Consultant',
                    image: {
                      src: '/images/team-members/zapper-joule.webp',
                      alt: 'Zapper Joule',
                      width: 600,
                      height: 400
                    }
                  },
                  {
                    name: 'Blitz Ampere',
                    title: 'Lightning Protection Consultant',
                    image: {
                      src: '/images/team-members/blitz-ampere.webp',
                      alt: 'Blitz Ampere',
                      width: 599,
                      height: 400
                    }
                  }
                ].map((member, index) => {

                  const { name, title, image } = member,
                        { src, alt, width, height } = image,
                        isEven = `${index / 2}`.split('.').length === 1

                  return (
                    <div key={`${name} ${title}`} className='flex flex-col justify-center gap-6 w-52 lg:even:top-16 lg:motion-safe:odd:scale-95'>

                      <Parallax
                        speed={isEven ? 2 : -2}
                        className='contents lg:motion-safe:flex flex-col justify-center gap-6'
                      >

                        <Atropos
                          rotateYMax={7}
                          rotateXMax={7}
                          shadowScale={0.8}
                        >

                          <Img
                            src={src}
                            alt={alt}
                            width={width}
                            height={height}
                            className='object-cover rounded-3xl w-full aspect-[3/4]'
                          />

                        </Atropos>

                        <div className='text-center'>

                          <h3 className='font-display text-lg font-semibold text-gray-700 dark:text-gray-50'>{name}</h3>

                          <p className='text-sm text-gray-500 dark:text-gray-300'>{title}</p>

                        </div>

                      </Parallax>

                    </div>
                  )
                })
              }

            </div>

          </section>

          <Testimonials
            intro='You know what we say about us…'
            heading='See what our clients have been saying.'
            testimonials={[
              {
                name: 'Sophia Rodriguez',
                body: `I recently had Synergy install energy-efficient lighting in my home, and I couldn't be happier with the results. Their team was professional, efficient, and the cost savings on my electricity bill have been significant.`,
                image: {
                  src: '/images/testimonials/sophia-rodriquez.webp',
                  alt: 'Sophia Rodriguez',
                  width: 150,
                  height: 225
                }
              },
              {
                name: 'Incredit',
                body: `Synergy helped us become more energy-efficient and reduce our operating costs. We can't thank them enough for how they helped our business reach our carbon footprint goals.`,
                image: {
                  src: '/images/testimonials/incredit.webp',
                  alt: 'Incredit Logo',
                  width: 150,
                  height: 150
                }
              },
              {
                name: 'Michael Chen',
                body: `Working with Synergy to upgrade my home with solar panels was a great decision. The team was knowledgeable, friendly, and the improvements have made a noticeable difference.`,
                image: {
                  src: '/images/testimonials/michael-chen.webp',
                  alt: 'Michael Chen',
                  width: 210,
                  height: 150
                }
              },
              {
                name: 'Get Served Servers',
                body: `Synergy's energy-efficient solutions have helped us reduce our operating costs and improve our sustainability. Their team is knowledgeable, reliable, and committed to providing exceptional service.`,
                image: {
                  src: '/images/testimonials/get-served-servers.webp',
                  alt: 'Get Served Servers Logo',
                  width: 150,
                  height: 150
                }
              },
              {
                name: 'Emily and David Johnson',
                body: `We had an excellent experience with Synergy. Their team installed the harmonics converter, and we've been thrilled with the results. We highly recommend Synergy to anyone looking to save money.`,
                image: {
                  src: '/images/testimonials/emily-and-david-johnson.webp',
                  alt: 'Emily and David Johnson',
                  width: 150,
                  height: 225
                }
              }
            ]}
          />

          <ContactFooter
            intro='Ready to meet the team?'
            heading='Sign up for a Free Consultation'
            andrillaContent={andrillaContent}
          />

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default AboutUs
