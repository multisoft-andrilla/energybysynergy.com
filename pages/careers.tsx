// * Types
import type { NextPage, GetStaticProps } from 'next'
import type { AndrillaContent, HasAndrillaContent } from '@/typings/firestore'
import type { Meta } from '@/typings/main'

// * Hooks
import { useRouter } from 'next/router'
import { usePageTitle } from '@/hooks/use-page-title'

// Firebase
import { getAndrillaContentFromId } from '@/firebase/get-andrilla-content'
import { ownerID } from '@/lib/firebase'

// * Components
import Head from 'next/head'
import { ParallaxProvider, Parallax } from 'react-scroll-parallax'
import { InView } from 'react-intersection-observer'
import RoundedSection from '@/components/rounded-section'
import Img from 'next/image'
import Card from '@/components/card'
import Button from '@/components/button'
import PersonCropSquare from '@/svgs/icons/person-crop-square.svg'
import DollarSign from '@/svgs/icons/dollar-sign.svg'
import ChartBarDocHorizontal from '@/svgs/icons/chart-bar-doc-horizontal.svg'
import PhoneArrowUpRight from '@/svgs/icons/phone-arrow-up-right.svg'
import House from '@/svgs/icons/house.svg'
import Tree from '@/svgs/icons/tree.svg'
import ThreePeople from '@/svgs/icons/3-people.svg'
import ClockBadgeCheckmark from '@/svgs/icons/clock-badge-checkmark.svg'
import Details from '@/components/details'

// * Templates
import InfoGrid from '@/templates/info-grid'
import ContactFooter from '@/templates/contact-footer'
import Testimonials from '@/templates/testimonials'
import GiantItemList from '@/templates/giant-item-list'

// * Functions
import { isMin50Max160 } from '@/utils/regex'

// * Meta
/** What displays after page load (Business name is added automatically). */
const pageTitle = 'Energy Efficiency Solutions',
      BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
      
const meta: Meta = {
  title: '',
  description: '',
  thumbnail: {
    src: 'careers-thumbnail.webp',
    alt: 'Preview of Careers page'
  }
}

const descriptionIsValid = meta?.description ? isMin50Max160(meta.description) : false

const Careers: NextPage<HasAndrillaContent> = ({ andrillaContent }) => {

  const { businessName } = andrillaContent,
        title = usePageTitle(meta?.title, pageTitle, businessName),
        description = descriptionIsValid ? meta.description : undefined,
        router = useRouter()

  return (
    <>
    
      <Head>
        
        <title>{title}</title>
        <meta property='og:title' content={`${meta.title} | ${businessName}`} />
        { description ? <>
          <meta name='description' content={description} />
          <meta property='og:description' content={description} />
        </> : null }
        { meta?.thumbnail ? <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/${meta.thumbnail.src}`} />
          <meta property='og:image:alt' content={meta.thumbnail.alt} />
        </> : <>
          <meta name='og:image' content={`${BASE_URL}/images/thumbnails/main.webp`} />
          <meta property='og:image:alt' content={`${businessName} Logo`} />
        </> }
        <meta property='og:url' content={`${BASE_URL}${router.asPath}`} />
        <meta property='og:canonical' content={`${BASE_URL}${router.asPath}`} />
        <meta name='theme-color' content={meta?.themeColor || '#ffab1a'} />
        <meta name='robots' content={meta?.robots || 'all'} />

      </Head>

      <ParallaxProvider>

        <main>

        <InView threshold={0.5} triggerOnce={false}>
            {({ inView, ref }) => (
              <RoundedSection ref={ref} inView={inView} interior={{ addClass: 'bg-gray-900 pb-96' }}>

                <div className={`absolute inset-0 min-h-[36rem] lg:max-h-[36rem] w-full sm:mt-36 md:mt-auto overflow-hidden ${inView ? '' : 'rounded-5xl'} isolate transition-[border-radius] duration-500`}>
    
                  <Parallax
                    speed={-5}
                    className={`
                      motion-reduce:contents
                      absolute right-0 -bottom-10 md:bottom-auto top-auto md:-top-10 md:left-auto
                      h-1/2 w-full md:w-1/2 md:h-[calc(100%+5rem)]
                      after:content-[""]
                      after:absolute after:inset-0 after:z-10
                      md:after:w-1/4 after:h-1/4 md:after:h-full
                      after:bg-gradient-to-b md:after:bg-gradient-to-r after:from-gray-900
                    `}
                  >
    
                    <div className='
                      motion-safe:contents
                      absolute right-0 -bottom-10 md:bottom-auto top-auto md:-top-10 md:left-auto
                      h-1/2 w-full md:w-1/2 md:h-[calc(100%+5rem)]
                      after:content-[""]
                      after:absolute after:inset-0 after:z-10
                      md:after:w-1/4 after:h-1/4 md:after:h-full
                      after:bg-gradient-to-b md:after:bg-gradient-to-r after:from-gray-900
                    '>

                      <Img
                        src='/images/happy-diverse-team.webp'
                        alt='Happy diverse team holding laptops and notepads'
                        width={1600}
                        height={1080}
                        className='absolute inset-0 w-full h-full object-cover [object-position:calc(50%+5rem)_50%]'
                        sizes='
                          100vw,
                          (min-width: 768px) 50vw
                        '
                        priority
                        loading='eager'
                      />
                      
                    </div>
    
                  </Parallax>
    
                </div>
    
                <Card
                  heading='Join the Energy Revolution Your Way'
                  body='Energy is not a one-size-fits-all solution. With our training, you can specialize in conducting comprehensive energy evaluations to identify the best solutions for consumers and businesses, tailored to meet their unique needs.'
                  options={{
                    heading: {
                      as: 'h1',
                      addClass: 'text-gray-700 dark:text-gray-50 md:w-3/4'
                    }
                  }}
                  className='
                    bg-gray-50 dark:bg-gray-800
                    rounded-3xl
                    max-w-2xl
                    overflow-y-hidden
                    border-[1.5rem] sm:border-[3rem] border-gray-50 dark:border-gray-800
                    max-h-[31.5rem]
                  '
                />
    
              </RoundedSection>
            )}
          </InView>

          <section className='px-6 py-8 pb-12 max-w-6xl mx-auto'>

            <div className='grid grid-flow-row md:grid-cols-2 place-items-center'>

              <div className='mb-12'>

                <h2 className='font-display'>
                  <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold'>A Different Kind of Team</span>
                  <br />
                  <span className='text-primary-400 text-xl font-bold contrast-more:text-primary-700 contrast-more:dark:text-primary-400'>Moving for the same goal.</span>
                </h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>With our one-stop-shop approach and comprehensive training, you can participate in the energy revolution to help make the world a better place for future generations.</p>
                <br />

                <Button href='#contact' padding='lg'>Become a Partner</Button>

              </div>

            </div>

            <InfoGrid
              items={[
                {
                  Icon: PersonCropSquare,
                  heading: 'Unparalleled leadership and training',
                  body: '75-years of combined sales experience.'
                },
                {
                  Icon: DollarSign,
                  heading: 'Transparent compensation structure',
                  body: `Easily predict when and how much you'll be paid.`
                },
                {
                  Icon: ChartBarDocHorizontal,
                  heading: 'No quotas',
                  body: 'Simply work as much as you want.'
                },
                {
                  Icon: PhoneArrowUpRight,
                  heading: 'No cold-calling',
                  body: 'Our networking strategies work for both you and the client.'
                },
                {
                  Icon: House,
                  heading: 'A place for everyone that wants to succeed',
                  body: 'Apply your drive to find success.'
                },
                {
                  Icon: Tree,
                  heading: 'Make a tangible difference',
                  body: `The impact you make will be obvious.`
                },
                {
                  Icon: ThreePeople,
                  heading: 'Work with a diverse team',
                  body: 'A positive environment for everyday people from all backgrounds.'
                },
                {
                  Icon: ClockBadgeCheckmark,
                  heading: 'Part-time with no pressure',
                  body: 'Your own hours. Your own place. Our strategies and material.'
                }
              ]}
            />

          </section>

          <section className='py-24'>

            <div className='absolute inset-0 overflow-x-hidden'>

              <Img
                src='/svgs/backgrounds/wave-rtl-primary.svg'
                alt='Wave form gradually growing from small to large, and rising up left to right as it grows'
                width={1440}
                height={761.6}
                className='
                  absolute top-1/2 left-1/2 -z-50
                  -translate-y-1/2 -translate-x-1/2
                  min-w-[100rem] w-full
                '
              />
              
            </div>

            <div className='px-6 max-w-6xl mx-auto'>

              <div className='max-w-xl'>

                <h2 className='font-display text-3xl text-gray-700 dark:text-gray-200 font-bold max-w-sm'>Is This the Right Opportunity for You?</h2>
                <br />

                <p className='text-gray-500 dark:text-gray-400 text-xl'>Ask yourself the following questions to learn more about whether you'll fit into our business model.</p>

              </div>

            </div>

            <div className='px-6 max-w-[90rem] mx-auto'>

              {/* Classes pre-loaded for Tailwind */}
              <span aria-hidden='true' className='border-primary-400'>&nbsp;</span>
              <span aria-hidden='true' className='border-primary-600'>&nbsp;</span>
              <span aria-hidden='true' className='border-primary-700'>&nbsp;</span>

              <GiantItemList
                className='
                  grid grid-flow-row gap-y-24 lg:grid-flow-col lg:grid-rows-6 place-items-center
                  pt-24
                '
                items={[
                  {
                    heading: {
                      text: 'Do you love to serve others?',
                      addClass: 'lg:max-h-28 lg:w-64'
                    },
                    body: `Whether you're a busy parent, a recent graduate, or simply looking for some extra money, our business is designed for you.`,
                    identifier: {
                      className: `
                        text-center
                        max-w-[21rem] lg:w-min
                        mx-auto
                        lg:row-start-1 lg:row-end-5
                        before:content-[var(--content)]
                        before:text-[11.25rem] before:font-black before:font-display
                        before:absolute before:-top-28 before:-left-12 before:-z-10
                      `,
                      addClass: 'before:text-primary-700 md:before:-left-16 md:before:-top-20'
                    }
                  },
                  {
                    heading: {
                      text: 'Do you love working with a team?',
                      addClass: 'lg:max-h-28 lg:w-[22rem]'
                    },
                    body: `You don't need any previous experience. If working with passionate people is your thing, you're in the right place.`,
                    identifier: {
                      className: `
                        text-center
                        max-w-[21rem] lg:w-min
                        mx-auto
                        lg:row-start-2 lg:row-end-6 lg:mt-12
                        before:content-[var(--content)]
                        before:text-[11.25rem] before:font-black before:font-display
                        before:absolute before:-top-28 before:-left-12 before:-z-10
                      `,
                      addClass: 'before:text-primary-600 md:before:-left-20 md:before:-top-20 lg:ml-8'
                    }
                  },
                  {
                    heading: {
                      text: 'Do you want to improve quality of life?',
                      addClass: 'lg:w-80 xl:w-96'
                    },
                    body: {
                      text: `We want to to make the world a better place for future generations, and we want people like you to partner with us, to make it happen.`,
                      addClass: 'xl:left-1/2 xl:-translate-x-1/2'
                    },
                    identifier: {
                      className: `
                        text-center
                        max-w-[21rem] lg:w-min
                        mx-auto
                        lg:row-start-3 lg:row-end-7 lg:mt-24
                        before:content-[var(--content)]
                        before:text-[11.25rem] before:font-black before:font-display
                        before:absolute before:-top-28 before:-left-12 before:-z-10
                      `,
                      addClass: 'before:text-primary-400 md:before:-left-16 md:before:-top-20 lg:ml-16'
                    }
                  }
                ]}
              />

            </div>

          </section>

          <section className='pt-24 pb-12 px-4 sm:px-6 max-w-6xl mx-auto'>

            <div  className='bg-gray-900 px-6 sm:px-8 py-8 rounded-4xl lg:flex gap-8'>

              <div className='mb-8 lg:mb-0 pb-8 lg:pb-0 lg:pr-8 border-b border-b-gray-400 lg:border-b-0 lg:border-r lg:border-r-gray-400'>

                <h2 className='text-gray-50 font-black text-3xl sm:text-4xl font-display lg:w-min'>What&nbsp;you&nbsp;need to&nbsp;know.</h2>

              </div>

              <div className='grid grid-flow-row gap-4 w-full'>

                <Details summary='Do I have to have an office?'>
                  No, you don't need to have an office. Our business model is designed for you to work from home, or wherever you choose.
                </Details>

                <Details summary='Do you have marketing materials?'>
                  Yes, we have marketing materials available to all of our partners, including brochures, videos, and social media content.
                </Details>

                <Details summary='Do I have to be a sales person?'>
                  No, you don't have to be a sales person to join our team. We provide the necessary training to help you succeed.
                </Details>

                <Details summary='Do I need to pass a background check?'>
                  Yes, a background check is required to join our team. This is to ensure the safety and security of our partners and customers.
                </Details>
                
              </div>

            </div>

          </section>

          <Testimonials
            intro={`We love our team…`}
            heading={`And we think you'll love them too.`}
            testimonials={[
              {
                name: 'Blaze Voltage',
                body: `Synergy allowed me to add the cash flow I needed to reach my goals, and—since I could work on my own time—it hardly felt demanding.`,
                image: {
                  src: '/images/team-members/blaze-voltage.webp',
                  alt: 'Blaze Voltage',
                  width: 600,
                  height: 400
                }
              },
              {
                name: 'Spark Thunderbolt',
                body: `Joining Synergy was one of the best decisions I've ever made. Their training was so helpful for me to get started, and—after awhile—I loved it so much that I decided to go to full-time.`,
                image: {
                  src: '/images/team-members/spark-thunderbolt.webp',
                  alt: 'Spark Thunderbolt',
                  width: 400,
                  height: 598
                }
              },
              {
                name: 'Surge Dynamo',
                body: 'The team-oriented environment of Synergy is what really made an impact on me. Being able to be my own boss, while still having teammates has been very rewarding.',
                image: {
                  src: '/images/team-members/surge-dynamo.webp',
                  alt: 'Surge Dynamo',
                  width: 400,
                  height: 600
                }
              },
              {
                name: 'Arc Electra',
                body: 'I have always been passionate about helping people in any way I can. Synergy made it possible for me to reach more people and help them lower their financial and environmental cost. The impact cannot be understated.',
                image: {
                  src: '/images/team-members/arc-electra.webp',
                  alt: 'Arc Electra',
                  width: 400,
                  height: 598
                }
              },
              {
                name: 'Flash Watts',
                body: `Since I already had a successful job that I loved, I wasn't so sure about Synergy at first. When my friend invited me to learn more, I obliged him, not knowing it would change my life. Adding "Energy Effeciency Advisor" to my skillset has allowed me to help so many businesses.`,
                image: {
                  src: '/images/team-members/flash-watts.webp',
                  alt: 'Flash Watts',
                  width: 400,
                  height: 600
                }
              },
              {
                name: 'Bolt Kilowatt',
                body: `There's nothing I wouldn't do for family, so when I was offered the opportunity to help families all across my state, I was ecstatic. The people I've reached have been so grateful for all of the ways Synergy has helped them save money.`,
                image: {
                  src: '/images/team-members/bolt-kilowatt.webp',
                  alt: 'Bolt Kilowatt',
                  width: 400,
                  height: 500
                }
              },
              {
                name: 'Zapper Joule',
                body: `Sales has never been my thing, so joining what seemed like a "salesforce" didn't get me excited. As I learned how Synergy is service-over-sales I became more interesting. Offering people services, rather than a product, is easy because I know I can help.`,
                image: {
                  src: '/images/team-members/zapper-joule.webp',
                  alt: 'Zapper Joule',
                  width: 600,
                  height: 400
                }
              },
              {
                name: 'Blitz Ampere',
                body: 'Before Synergy, I was working an unfulfilling 9-5. Although it paid the bills, there was no real passion behind my work. Synergy gave me a job that I love. I feel more free than ever before, and I can confidently say I am passionate about our serivces.',
                image: {
                  src: '/images/team-members/blitz-ampere.webp',
                  alt: 'Blitz Ampere',
                  width: 599,
                  height: 400
                }
              }
            ]}
          />

          <ContactFooter
            intro='Ready to meet the team?'
            heading='Sign up for a Free Consultation'
            andrillaContent={andrillaContent}
          />

        </main>

      </ParallaxProvider>
    
    </>
  )
}

export const getStaticProps: GetStaticProps<HasAndrillaContent> = async () => {

  const andrillaContent: AndrillaContent | undefined = await getAndrillaContentFromId(ownerID)

  if (andrillaContent) {
    return {
      props: {
        andrillaContent
      }
    }
  }

  return {
    notFound: true
  }
}

export default Careers
