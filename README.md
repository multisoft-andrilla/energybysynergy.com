# Synergy Solar Group

This is the front end repo for synergysolargroup.com.

## Get Started

Create a fork and run `npm i`. This project utilizes Next.js, Tailwind CSS, and Sanity.io CMS.