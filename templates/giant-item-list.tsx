// * Types
import type { FunctionComponent, Dispatch, SetStateAction } from 'react'
import type { GiantItemList as GiantItemListProps, GiantListItemProps } from '@/typings/templates'

// * Hooks
import { useState, useMemo } from 'react'
import { HasClassName } from '@/typings/main'

// * Components
import { Parallax } from 'react-scroll-parallax'

const getIdentiferColor = (
  identifier: HasClassName | undefined,
  setHasCustomIdentifierColor: Dispatch<SetStateAction<boolean>>
): string => {

  if (identifier?.addClass?.includes('before:text-')) {

    const identifierClasses = identifier.addClass.split(' ')

    return identifierClasses.flatMap(className => {

      if (
        !className.includes('before:text-') ||
        className.includes('[') ||
        className.includes('center') ||
        className.includes('left') ||
        className.includes('right')
      ) return

      if (className.includes('before:text-') && !className.includes('before:text-[')) {
        setHasCustomIdentifierColor(true)
        return className
      }

      return
    })[0]!
  }

  if (identifier?.addClass?.includes('before:text-')) {

    const identifierAddedClasses = identifier.addClass.split(' ')

    return identifierAddedClasses.flatMap(className => {

      if (
        !className.includes('before:text-') ||
        className.includes('[') ||
        className.includes('center') ||
        className.includes('left') ||
        className.includes('right')
      ) return

      if (className.includes('before:text-')) {
        setHasCustomIdentifierColor(true)
        return className
      }

      return
    })[0]!
  }

  return 'before:text-gray-500'
}

const ListItem: FunctionComponent<GiantListItemProps> = ({
  heading,
  body,
  identifier,
  index,
  listItemStyle,
  customIdentifier
}) => {

  const itemNumber = index + 1,
        [hasCustomIdentifierColor, setHasCustomIdentifierColor] = useState(false)

  const headingIsString = typeof(heading) === 'string',
        bodyIsString = typeof(body) === 'string',
        Heading = headingIsString ? 'h3' : (heading.as || 'h3')

  const identifierColor = useMemo(() => getIdentiferColor(identifier, setHasCustomIdentifierColor), [identifier]),
        identifierColorClass = identifierColor.replace('before:text-', ''),
        borderColor = `border-${identifierColorClass}`
  
  const defaultHeadingClasses = `
    px-2.5 py-4
    bg-gray-50 dark:bg-gray-700
    border-[0.625rem] ${borderColor}
    rounded-3xl
    text-2xl font-display font-bold text-gray-700 dark:text-gray-50
  `

  const defaultBodyClasses = `
    px-2.5 py-4
    bg-gray-50 dark:bg-gray-700
    border-b-[0.375rem] border-x-[0.375rem] border-gray-500
    rounded-b-3xl
    text-gray-500 dark:text-gray-200
    w-[calc(100%-3rem)]
    mx-auto
  `

  return (
    <li
      className={`
        ${identifier?.className || `
          text-center
          max-w-[21rem] md:w-min
          mx-auto
          md:first:row-start-1 md:first:row-end-5
          md:even:row-start-2 md:even:row-end-6 md:even:mt-12
          md:last:row-start-3 md:last:row-end-7 md:last:mt-24
          before:content-[var(--content)]
          before:text-[11.25rem] before:font-black before:font-display
          before:absolute before:-top-28 before:-left-12 before:-z-10
        `}
        ${identifier?.addClass || ''}
        ${hasCustomIdentifierColor ? '' : identifierColor}
      `}
      // @ts-ignore: CSS variables do not exist on type CSSProperties
      style={{ '--content': `'${customIdentifier || itemNumber}'` }}
    >

      <Parallax speed={1.5 * index} className='contents md:motion-safe:block'>

        <Heading className={`
          ${headingIsString
            ? defaultHeadingClasses
            : `${heading.className || defaultHeadingClasses}`
          }
          ${headingIsString ? '' : `${heading.addClass || ''}`}
        `}>{headingIsString ? heading : heading.text}</Heading>
        
        <p className={`
          ${bodyIsString
            ? defaultBodyClasses
            : `${body.className || defaultBodyClasses}`
          }
          ${bodyIsString ? '' : `${body.addClass || ''}`}
        `}>{bodyIsString ? body : body.text}</p>
        
      </Parallax>

    </li>
  )

}

const GiantItemList: FunctionComponent<GiantItemListProps> = ({
  listItemStyle = 'latin',
  customIdentifier,
  items,
  className,
  addClass
}) => {
  return (
    <ul className={`
      ${className || `
        grid grid-flow-row gap-y-24 md:grid-flow-col md:grid-rows-6 place-items-center
        pt-24
      `}
      ${addClass || ''}
    `}>
      {
        items.map((item, index) => {

          const { heading, body, identifier } = item

          return (
            <ListItem
              key={typeof(heading) === 'string' ? heading : heading.text}
              heading={heading}
              body={body}
              identifier={identifier}
              index={index}
              listItemStyle={listItemStyle}
              customIdentifier={customIdentifier}
            />
          )
        })
      }
    </ul>
  )
}

export default GiantItemList