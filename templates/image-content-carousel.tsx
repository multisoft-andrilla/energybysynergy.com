// * Types
import type { FunctionComponent } from 'react'
import type { ImageContentCarousel as ImageContentCarouselProps } from '@/typings/templates'

// * Components
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperObject, { Pagination } from 'swiper'
import Img from 'next/image'

// * Styles
import 'swiper/css'
import 'swiper/css/pagination'

const ImageContentCarousel: FunctionComponent<ImageContentCarouselProps> = ({ items }) => {

  const onSlideChange = (e: SwiperObject) => {
    setTimeout(() => {
      let updateInterval = setInterval(() => {
        e.update()
      }, 0.5)
      setTimeout(() => {
        clearInterval(updateInterval)
      }, 210)
    }, 90)
  }

  return (
    <div className='w-full overflow-x-hidden'>
    
      <style jsx-global='true'>{`
        .swiper-slide {
          width: auto;
        }
        .swiper-pagination-bullet {
          background-color: rgb(229 231 235);
        }
        .swiper-slide-active .swiper-image-container {
          height: 20rem;
          transform: translateY(-2rem);
        }
        @media (min-width: 640px) {
          .swiper-slide-active .swiper-content {
            width: 30rem;
          }
          .swiper-slide-active .swiper-text-container {
            transform: translateY(50%);
          }
          .swiper-slide-active .swiper-body {
            height: 3rem;
          }
        }
      `}</style>
    
      <Swiper
        modules={[ Pagination ]}
        spaceBetween={32}
        centeredSlides={true}
        initialSlide={1}
        pagination={{ clickable: true }}
        slidesPerView='auto'
        onSlideChange={onSlideChange}
        className='!pb-12 !pt-6 sm:!pb-20'
      >

        {
          items.map(item => {

            const { image, heading, body } = item,
                  { src, alt, width, height } = image

            return (
              <SwiperSlide key={heading}>

                <div className='
                  border-[0.85rem] border-gray-50 dark:border-gray-700
                  bg-gray-300 dark:bg-gray-800
                  rounded-4xl
                  w-64 h-88 lg:w-72
                  transition-[width] duration-300 ease-out
                  swiper-content
                '>

                  <div className='h-72 w-full mb-8'>

                    <div className='
                      absolute
                      w-full h-full
                      rounded-3xl
                      overflow-hidden
                      transition-[transform,height] duration-300 ease-out
                      swiper-image-container
                    '>

                      <Img
                        src={src}
                        alt={alt}
                        width={width}
                        height={height}
                        className='absolute inset-0 w-full h-full object-cover'
                      />
                      
                    </div>

                  </div>

                  <div className='absolute w-full bottom-0 px-3 pb-3'>

                    <div>

                      <div className='
                        absolute bottom-0
                        bg-gray-50 dark:bg-gray-700
                        p-4 rounded-2xl
                        text-gray-900 dark:text-gray-50
                        transition-[transform,height] duration-300 ease-out
                        swiper-text-container
                      '>

                        <h3 className='font-display font-semibold'>{heading}</h3>

                        <div className='flex items-end sm:h-0 overflow-hidden swiper-body'>
                          <p className='text-xs'>{body}</p>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </SwiperSlide>
            )
          })
        }

      </Swiper>
    
    </div>
  )
}

export default ImageContentCarousel