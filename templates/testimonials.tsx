// * Types
import type { FunctionComponent } from 'react'
import type { Testimonials as TestimonialsProps } from '@/typings/templates'

// * Components
import { Swiper, SwiperSlide } from 'swiper/react'
import { Keyboard, Navigation, Thumbs } from 'swiper'
import Img from 'next/image'
import ChevronLeft from '@/svgs/icons/chevron-left.svg'
import ChevronRight from '@/svgs/icons/chevron-right.svg'

// * Styles
import 'swiper/css'
import 'swiper/css/keyboard'
import 'swiper/css/grid'

const Testimonials: FunctionComponent<TestimonialsProps> = ({ intro, heading, testimonials }) => {
  return (
    <section className='pt-24 pb-24'>

      <h2 className='text-center mb-20 px-6'>
        <span className='text-gray-500 dark:text-gray-400 text-xl block'>{intro}</span>
        <br />
        <span className='text-3xl text-gray-700 dark:text-gray-200 font-bold font-display'>{heading}</span>
      </h2>

      <Swiper
        modules={[ Keyboard, Thumbs, Navigation ]}
        keyboard={{
          enabled: true,
          onlyInViewport: true
        }}
        navigation={{
          prevEl: '[data-swiper="previous"]',
          nextEl: '[data-swiper="next"]'
        }}
        grabCursor
        loop
        className='!pt-8'
        breakpoints={{
          300: {
            slidesPerView: 1
          },
          800: {
            slidesPerView: 2
          },
          1024: {
            slidesPerView: 3
          },
          1300: {
            slidesPerView: 4
          },
          1700: {
            slidesPerView: 5
          }
        }}
      >

        {
          testimonials.map((testimonial, index) => {

            const { name, body, image } = testimonial,
                  { src, alt, width, height } = image,
                  isEven = `${index / 2}`.split('.').length === 1

            return (
              <SwiperSlide key={name} className={`px-6 ${isEven ? '' : 'sm:-top-8'}`}>

                <div className={`
                  ${isEven ? 'bg-gray-900' : 'bg-gray-700'}
                  rounded-3xl p-6 h-96 sm:h-80 flex flex-col gap-4 mx-auto
                `}>

                  <div className='flex items-center gap-4'>

                    <Img
                      src={src}
                      alt={alt}
                      width={width}
                      height={height}
                      className={image.className || 'object-cover aspect-square w-20 rounded-xl'}
                    />

                    <h3 className='font-display font-bold text-lg text-gray-50'>{name}</h3>
                    
                  </div>

                  <p className='text-gray-200'>{body}</p>

                </div>

              </SwiperSlide>
            )
          })
        }

        {/* Swiper Navigation */}
        <div className='flex items-center gap-6 mt-8 mx-auto w-min'>

          <button data-swiper='previous' className='w-10 h-10 rounded-full bg-gray-400 hover:bg-primary-400 transition-[background-color] duration-200' title='Previous Slide'>

            <span className='sr-only'>Previous Slide</span>

            <ChevronLeft className='h-5 m-auto fill-white -translate-x-[1px]' />

          </button>

          <button data-swiper='next' className='swiper w-10 h-10 rounded-full bg-gray-400 hover:bg-primary-400 transition-[background-color] duration-200' title='Next Slide'>

            <span className='sr-only'>Next Slide</span>

            <ChevronRight className='h-5 m-auto fill-white translate-x-[1px]' />

          </button>

        </div>

      </Swiper>

    </section>
  )
}

export default Testimonials