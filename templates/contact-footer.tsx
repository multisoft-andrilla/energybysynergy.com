// * Types
import type { FunctionComponent } from 'react'
import type { ContactFooter as ContactFooterProps } from '@/typings/templates'

// Firebase
import { ownerID } from '@/lib/firebase'

// * Components
import Divider from '@/components/divider'

// * Partials
import ContactForm from '@/partials/forms/contact-form'

const ContactFooter: FunctionComponent<ContactFooterProps> = ({ andrillaContent, intro, heading }) => {
  return (
    <section className='px-6 pt-40 pb-32 bg-gray-900'>

      <Divider style='blocky-180' addClass='fill-gray-50 dark:fill-gray-800 bottom-auto -top-16' />
          
      <div id='contact' className='absolute -top-24'>&nbsp;</div>

      <div className='grid grid-flow-row gap-12 md:grid-cols-2 max-w-7xl mx-auto'>

        <div className='md:col-start-2'>

          <h2 className='font-display'>
            <span className='text-primary-400 text-xl font-medium'>{intro}</span>
            <br />
            <span className='text-3xl text-gray-50 font-bold'>{heading || 'Get Started With a Free Consultation'}</span>
          </h2>
          <br />

          <p className='text-gray-200 text-xl'>Let us learn where your problem areas are, and we'll provide you with a solution that fits your needs.</p>

        </div>

        <ContactForm
          addId='1'
          andrillaContent={andrillaContent}
          ownerID={ownerID}
          addClass='md:col-start-1 md:row-start-1 dark:shadow-2xl dark:shadow-gray-800'
          button={{ label: 'Get a Quote' }}
        />

      </div>

    </section>
  )
}

export default ContactFooter