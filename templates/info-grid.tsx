// * Types
import type { FunctionComponent } from 'react'
import type { InfoGrid as InfoGridProps } from '@/typings/templates'
import Button from '@/components/button'

const InfoGrid: FunctionComponent<InfoGridProps> = ({ rows, cols = 2, items, options, className, addClass }) => {

  const rowsClasses = rows ? `md:grid-rows-${rows}` : undefined,
        colsClasses = cols ? `md:grid-cols-${cols}` : undefined

  return (
    <ul className={`
      grid grid-flow-row ${rowsClasses || ''} ${colsClasses || ''}
      ${className || `
        p-4 rounded-4xl
        bg-gray-800 dark:bg-gray-900
        shadow-[1.5rem_2rem_0] shadow-gray-200 dark:shadow-gray-700
        gap-8 sm:gap-10 md:gap-16
      `}
      ${addClass || ''}
    `}>

      {
        items.map(item => {

          const { Icon, heading, body, button } = item

          return (
            <li key={heading} className='flex flex-col sm:flex-row gap-4 group/item'>

              <div className='
                w-16 h-16
                bg-gray-700
                overflow-hidden
                rounded-2xl
                p-3
                transition-[fill,background-color] duration-300
              '>

                <Icon className='
                  w-10 h-10
                  fill-primary-500
                '/>

                <div className='
                  w-16 h-16
                  absolute top-1/2 left-1/2
                  -translate-y-1/2 -translate-x-1/2
                '>

                  <div className='
                    w-20 h-20
                    absolute top-1/2 left-1/2
                    -translate-y-1/2 -translate-x-1/2 -rotate-225
                  '>

                    <div className='
                      overflow-hidden
                      bg-gray-600
                      w-20 h-20 group-hover/item:w-0
                      transition-[width] duration-500 ease-[cubic-bezier(0,.1,.1,.6)]
                    '>

                      <div className='w-20 h-20 absolute rotate-225'>

                        <Icon className='
                          w-10 h-10
                          top-1/2 left-1/2
                          -translate-y-1/2 -translate-x-1/2
                          fill-gray-50
                        '/>
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  
                </div>

              </div>

              <div className='flex flex-col gap-2'>

                <h3 className={`
                  ${options?.heading?.className || 'text-gray-50 font-display font-black text-2xl'}
                  ${options?.heading?.addClass || ''}
                `}>{heading}</h3>

                {
                  body ?
                  <p className={`
                    ${options?.body?.className || 'text-lg text-gray-200'}
                    ${options?.body?.addClass || ''}
                  `}>{body}</p>
                  : null
                }

                {
                  button ?
                  <Button href={button.href} target={button.target} rel={button.rel} className={button.className} addClass={button.addClass || 'mr-auto'}>{button.children}</Button>
                  : null
                }

              </div>

            </li>
          )
        })
      }

    </ul>
  )
}

export default InfoGrid