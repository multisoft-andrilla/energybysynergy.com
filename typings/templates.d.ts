// * Types
import { ImageProps } from 'next/image'
import type { Ref } from 'react'
import { Button } from './components'
import type { HasAndrillaContent } from './firestore'
import type { HasChildren, HasClassName } from './main'

export interface ImageContentCarousel {
  items: {
    image: ImageProps
    heading: string
    body: string
  }[]
}

export interface ContactFooter extends HasAndrillaContent {
  intro: string
  heading: string
}

export interface RoundedSection extends HasChildren {
  ref?: Ref
  inView: boolean
  exterior?: HasClassName
  interior?: HasClassName
}

export interface Testimonials extends HasClassName {
  intro: string
  heading: string
  testimonials: {
    name: string
    body: string
    image: ImageProps
  }[]
}

export interface InfoGrid extends HasClassName {
  rows?: number
  cols?: number
  items: {
    Icon: any
    heading: string
    body?: string
    button?: Button
  }[]
  options?: {
    icon?: HasClassName
    heading?: HasClassName & {
      as?: 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
    }
    body?: HasClassName
  }
}

type GiantListItem = {
  heading: string | HasClassName & {
    text: string
    as?: 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
  }
  body: string | HasClassName & {
    text: string
  }
  identifier?: HasClassName
}

export interface GiantItemList extends HasClassName {
  listItemStyle?: 'latin' | 'roman' | 'bullet' | 'custom'
  customIdentifier?: string
  items: [
    GiantListItem,
    GiantListItem,
    GiantListItem
  ]
}

export interface GiantListItemProps extends GiantListItem {
  index: number
  listItemStyle?: 'latin' | 'roman' | 'bullet' | 'custom'
  customIdentifier?: string
}