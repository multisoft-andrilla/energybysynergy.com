import { rgbRegex, emailRegex, telRegex, min50Max160Regex, max50Regex } from '@/utils/regex'

export type Rgb = string & { rgbRegex: true }

export type Email = string & { emailRegex: true }

export type Tel = string & { telRegex: true }

export type Min50Max160 = string & { min50Max160Regex: true }

export type Max50 = string & { max50Regex: true }

/**
 * ### Meta
 * 
 * Completely optional information used for `<meta>` tags in the `<head>`
 * 
 * #### Options
 * 
 * - `title` - The title displayed when sharing the page (Maximum 50).
 * - `description` - The description of the page, used for bots and SEO (Minimum 50, Maximum 160).
 * - `thumbnail` - An object of `src` and `alt` for the image displayed when sharing the page.
 * - `themeColor` - A color value for changing the toolbar in Safari. Leave undefined to let the browser decide.
 * - `robots` - Defines whether robots should be allowed to crawl the page.
 */
export interface Meta {
  /** The title displayed when sharing the link (Maximum 50; automatically adds business name). */
  title?: string | Max50
  /** The description of the page (Minimum 50, Maximum 160). */
  description?: string | Min50Max160
  /** The image displayed when sharing the page. */
  thumbnail?: {
    /** A link to the image source */
    src: string
    /** A description of the image */
    alt?: string
  }
  /** A color value for changing the toolbar in Safari. Leave undefined to let the browser decide. */
  themeColor?: string
  /**
   * - `'all'` - Allow crawlers (default).
   * - `'noindex, nofollow'` - Disallow crawlers (Good for private pages).
   */
  robots?: 'all' | 'noindex, nofollow'
}

export interface HasClassName {
  /** Replaces all default classes to apply to the component. */
  className?: string
  /** Additional classes to apply to the component. */
  addClass?: string
}

export interface HasChildren {
  /** The content to display inside the component. */
  children: any
}

export interface HasTitle {
  /** The title attribute for the element. */
  title?: string
}