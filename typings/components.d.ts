// * Types
import type { HasClassName, HasChildren, HasTitle } from './main'
import type { HTMLAttributeAnchorTarget } from 'react'
import { ImageProps } from 'next/image'

export interface Modal extends HasClassName, HasChildren {
  trigger: FunctionComponent<Button>
  place?: 'center' | 'bottom'
}

export interface ButtonContent extends LinkAttributes {
  /** The text displayed on the button. */
  label: string
  /** The destination URL for the button. */
  href?: string
  /** The content to display inside the modal */
  modal?: ReactElement
}

export interface Button extends HasClassName, HasChildren, LinkAttributes {
  /** The destination URL for the button. */
  href?: string
  /** The size of the element based on padding. */
  padding?: 'none' | 'xs' | 'sm' | 'base' | 'md' | 'lg'
  /** The size of the border radius. */
  rounded?: 'none' | 'sm' | 'base' | 'lg' | 'xl' | 'full'
  /** Sets the color classes to predefined values. */
  color?: 'primary' | 'tune' | 'white' | 'black'
  /** Inverts the normal and hover color classes of the predefined values. */
  inverse?: boolean
  /** Whether the button should be transparent or opaque. */
  opacity?: 'opaque' | 'transparent'
  /** Defines the type attribute of the button element (default: `'button'`). */
  type?: 'button' | 'submit' | 'reset'
  /** A function for handling onClick events. */
  onClick?: (e: any) => e.void
  /** Any `ReactElement`, but intended to be an imported SVG. Using this provides predefined classes; otherwise, use your element/SVG in the children */
  icon?: any
  /** Whether the icon should appear on the right or the left of the text */
  iconPosition?: 'left' | 'right'
}

export interface Form extends HasClassName, HasChildren {
  itemProp?: string
  itemScope?: boolean
  itemType?: string
  onSubmit?: (e: any) => e.void
  privacyMessage?: HasClassName
}

export interface Input extends HasClassName {
  type?: 'text' | 'textarea' | 'email' | 'tel' | 'checkbox' | 'radio' | 'time' | 'color' | 'date' | 'file' | 'image' | 'month' | 'number' | 'password' | 'range' | 'search' | 'url' | 'week'
  label?: string
  placeholder?: string | '*'
  required?: boolean
  name: string
  addId?: string
  min?: number,
  max?: number,
  rows?: number,
  cols?: number,
  resize?: boolean | 'x' | 'y',
  options?: string[] | { id: string, name: string }[]
  autocomplete?: string
}

export interface CheckboxRadioInput {
  type: 'checkbox' | 'radio'
  name: string
  option: string | { id: string, name: string }
}

export interface Select {
  name: string
  options: {
    id: string
    name: string
  }[]
  label?: string
  defaultValue?: number
  required?: boolean
  onChange?: (selected: { id: string, name: string }) => void
  addClass?: string
}

export interface Video extends HasClassName {
  src: string
  useFacade?: boolean
  thumbnail?: ImageProps
  autoplay?: boolean
  loop?: boolean
  title: string
}

export interface VimeoVideoInfo {
  title?: string
  thumbnail?: string
}

export interface LinkAttributes extends HasTitle {
  /** The target attribute for the link (local link default: `'_self'`; external link default: `'_blank'`). */
  target?: '_self' | '_blank' | '_parent' | '_top'
  /** The rel attribute for the link (local link default: `'follow'`; external link default: `'nofollow'`). */
  rel?: 'follow' | 'nofollow' | 'prefetch'
  /** Redefines the name of the file when downloaded. */
  download?: string
}

export interface Link extends HasChildren, HasClassName, LinkAttributes {
  /** The destination URL for the link. */
  href: string
  type?: 'normal' | 'ltr' | 'rtl' | 'lift' | 'center' | 'fill' | 'fill-lift' | 'fill-ltr' | 'fill-rtl' | 'static'
  onClick?: (e: any) => e.void
}

type IFrameAllowAttribute = 'accelerometer' | 'autoplay' | 'camera' | 'encrypted-media' | 'fullscreen' | 'gyroscope' | 'magnetometer' | 'microphone' | 'payment' | 'picture-in-picture' | 'publickey-credentials-get' | 'usb'

type IFrameReferrerPolicyAttribute = 'no-referrer' | 'no-referrer-when-downgrade' | 'origin' | 'origin-when-cross-origin' | 'same-origin' | 'strict-origin' | 'strict-origin-when-cross-origin' | 'unsafe-url'

type IFrameSandboxAttribute = 'allow-downloads' | 'allow-forms' | 'allow-modals' | 'allow-orientation-lock' | 'allow-pointer-lock' | 'allow-popups' | 'allow-popups-to-escape-sandbox' | 'allow-presentation' | 'allow-same-origin' | 'allow-scripts' | 'allow-top-navigation' | 'allow-top-navigation-by-user-activation' | 'allow-top-navigation-to-custom-protocols'

export interface IFrame extends HasClassName {
  src: string
  id?: string
  title: string
  allow?: IFrameAllowAttribute | IFrameAllowAttribute[] | 'allow-all'
  referrerPolicy?: IFrameReferrerPolicyAttribute
  sandbox?: IFrameSandboxAttribute | IFrameSandboxAttribute[] | 'allow-all'
  loading?: 'eager' | 'lazy'
}

export interface Divider extends HasClassName {
  /** Changes what the divider looks like (default: blocky). */
  style?: 'blocky' | 'blocky-180'
  /** Replace default classes. */
  className?: string
  /** Additional classes to apply to the component (use fill-* to change the color). */
  addClass?: string
}

export interface Card extends HasClassName {
  heading: string
  body: string
  options?: {
    heading?: HasClassName & {
      as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
    }
    body?: HasClassName
  }
}

export interface Details extends HasChildren, HasClassName {
  summary: string
}

export interface DropDown extends HasClassName {
  type?: 'click' | 'hover'
  trigger: FunctionComponent<HTMLDivElement>
  items: ((HasClassName & {
    label: string
    href: string
  }) | 'divider')[]
  button?: HasClassName
  onClick?: (e: any) => e.void
}