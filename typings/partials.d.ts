// * Types
import type { HasAndrillaContent } from './firestore'
import type { HasClassName } from './main'
import type { Dispatch, SetStateAction } from 'react'

export interface ContactForm extends HasClassName, HasAndrillaContent {
  ownerID: string
  button?: {
    label: string
  }
  /** Appends to the id for uniqueness */
  addId?: string
}

export interface SideNav {
  isOpen: boolean
  setIsOpen: Dispatch<SetStateAction<boolean>>
  toggleNavOpen: () => void
}

export interface NavItem extends HasClassName {
  as?: 'li' | 'div'
  href?: string
  label: string
  onClick?: (e: any) => e.void
}