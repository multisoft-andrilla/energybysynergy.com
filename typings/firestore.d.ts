// * Types
import type { Timestamp } from 'firebase/firestore'
import type { Email } from './components'

export interface AndrillaContent {
  address?: {
    line1?: string
    line2?: string
    city?: string
    state?: 'Alabama' | 'Alaska' | 'Arizona' | 'Arkansas' | 'California' | 'Colorado' | 'Connecticut' | 'Delaware' | 'District Of Columbia' | 'Florida' | 'Georgia' | 'Hawaii' | 'Idaho' | 'Illinois' | 'Indiana' | 'Iowa' | 'Kansas' | 'Kentucky' | 'Louisiana' | 'Maine' | 'Maryland' | 'Massachusetts' | 'Michigan' | 'Minnesota' | 'Mississippi' | 'Missouri' | 'Montana' | 'Nebraska' | 'Nevada' | 'New Hampshire' | 'New Jersey' | 'New Mexico' | 'New York' | 'North Carolina' | 'North Dakota' | 'Ohio' | 'Oklahoma' | 'Oregon' | 'Pennsylvania' | 'Rhode Island' | 'South Carolina' | 'South Dakota' | 'Tennessee' | 'Texas' | 'Utah' | 'Vermont' | 'Virginia' | 'Washington' | 'West Virginia' | 'Wisconsin' | 'Wyoming'
    stateCode?: 'AL' | 'AK' | 'AZ' | 'AR' | 'CA' | 'CO' | 'CT' | 'DE' | 'DC' | 'FM' | 'FL' | 'GA' | 'HI' | 'ID' | 'IL' | 'IN' | 'IA' | 'KS' | 'KY' | 'LA' | 'ME' | 'MD' | 'MA' | 'MI' | 'MN' | 'MS' | 'MO' | 'MT' | 'NE' | 'NV' | 'NH' | 'NJ' | 'NM' | 'NY' | 'NC' | 'ND' | 'OH' | 'OK' | 'OR' | 'PW' | 'PA' | 'PR' | 'RI' | 'SC' | 'SD' | 'TN' | 'TX' | 'UT' | 'VT' | 'VA' | 'WA' | 'WV' | 'WI' | 'WY'
    county?: string
    country?: 'United States of America' | 'Canada'
    countryCode?: 'USA' | 'CA'
    zipCode?: string
  }
  businessName: string
  contactInfo: {
    phone: string
    email: string
  }
  financialHouseLink?: string
  logo?: {
    src: string
    alt?: string
  }
  ownerID: string
  ownerName: string
  socialMedia?: {
    facebook?: string
    instagram?: string
    youtube?: string
    twitter?: string
    linkedin?: string
  }
  schedulingLink?: string
  palette?: {
    primaryColor: string
    secondaryColor: string
  }
}

export interface HasAndrillaContent {
  andrillaContent: AndrillaContent
}

export interface UserInfo {
  displayName: string
  email: string
  firstName: string
  lastName: string
  phone: string
  uid: string
}

export interface WebsiteInfo {
  ownerID: string
  memberList: string[]
  url: string
}

export interface UserInfo {
  createdAt: Timestamp
  updatedAt?: Timestamp
  displayName: string
  email: Email
  firstName: string
  lastName: string
  phone: Tel
}

export interface Form {
  createdAt?: Timestamp
  formID?: string
  fields?: {
    displayName: string
    inputName: string
    type: 'text' | 'email' | 'tel'
  }[]
  formName?: string
  ownerID: string
  siteID: string
  templateID?: string
  url?: string
  ownerEmail?: string
  [id?: string]: string
}

export interface FormInfo {
  createdAt?: Timestamp
  updatedAt?: Timestamp
  color: string
  formID: string
  ownerID: string
  templateID?: string
  fields: {
    displayName: string
    inputName: string
    type: 'text' | 'email' | 'tel' | 'csv'
  }[]
}

export interface Submission {
  formID: string
  ownerID: string
  submissionID: string
}